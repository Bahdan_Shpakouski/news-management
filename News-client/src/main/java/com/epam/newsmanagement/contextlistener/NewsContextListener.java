package com.epam.newsmanagement.contextlistener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

/**
 * Application Lifecycle Listener implementation class NewsContextListener
 *
 */
@WebListener
public class NewsContextListener extends Object implements ServletContextListener {
       
	static Logger logger = Logger.getLogger(NewsContextListener.class);
	
    /**
     * @see Object#Object()
     */
    public NewsContextListener() {
        super();
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent sce)  { 
    	logger.info("Stop");
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent sce)  {
    	logger.info("Start");
    }
	
}
