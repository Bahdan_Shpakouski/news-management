package com.epam.newsmanagement.controller;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagement.command.NewsCommand;
import com.epam.newsmanagement.command.NewsCommandFactory;

/**
 * Servlet implementation class NewsServlet
 */
@WebServlet("/NewsServlet")
public class NewsServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private ClassPathXmlApplicationContext context;
	
	private static final String ERROR = "WEB-INF/pages/Error.jsp";
	
	static Logger logger = Logger.getLogger(NewsServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewsServlet() {
        super();
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		context = new ClassPathXmlApplicationContext("spring-config\\Beans.xml");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		serve(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		serve(request, response);
	}

	
	private void serve(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException{
		NewsCommandFactory factory = (NewsCommandFactory) context.getBean("newCommandFactory");
		NewsCommand command;
		String page = null;
		try {
			command = factory.getCommand(request);
			page = command.process(request);
		} catch (Exception e) {
			logger.error(e.getMessage());
			page = ERROR;
		}
		request.getRequestDispatcher(page).forward(request, response);
	}
	
	public void destroy(){
		context.close();
	}
}
