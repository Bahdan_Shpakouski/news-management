package com.epam.newsmanagement.command;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.exception.CommandException;

public class NewsCommandFactory {

	private static final String COMMAND_PARAMETR = "command";
	
	private enum CommandNames{SHOW_NEWS, SHOW_MESSAGE, CREATE_COMMENT, FILTER_NEWS}
	
	@Autowired
	private ShowNewsCommand showNewsCommand;
	@Autowired
	private FilterNewsCommand filterNewsCommand;
	@Autowired
	private ShowMessageCommand showMessageCommand;
	@Autowired
	private CreateCommentCommand createCommentCommand;
	
	public NewsCommandFactory() {
		
	}
	
	public NewsCommand getCommand(HttpServletRequest request) 
			throws CommandException{
		NewsCommand command = null;
		if(request.getParameter(COMMAND_PARAMETR) == null){
			throw new CommandException("No command name");
		}
		CommandNames name = CommandNames.valueOf(request
				.getParameter(COMMAND_PARAMETR).toUpperCase());
		switch(name){
		case SHOW_NEWS:
			command = showNewsCommand;
			break;
		case SHOW_MESSAGE:
			command = showMessageCommand;
			break;
		case CREATE_COMMENT:
			command = createCommentCommand;
			break;
		case FILTER_NEWS:
			command = filterNewsCommand;
		}
		return command;
	}
}
