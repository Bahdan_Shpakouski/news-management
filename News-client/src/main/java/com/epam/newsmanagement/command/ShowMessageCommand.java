package com.epam.newsmanagement.command;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.entity.dataobject.NewsTO;
import com.epam.newsmanagement.entity.dataobject.SearchCriteriaObject;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.FacadeNewsServiceInterface;
import com.epam.newsmanagement.service.NewsServiceInterface;

public class ShowMessageCommand implements NewsCommand {

	private static final String PAGE = "WEB-INF/pages/ViewMessage.jsp";
	
	@Autowired
	private FacadeNewsServiceInterface facadeNewsService;
	@Autowired
	private NewsServiceInterface newsService;
	
	/**
	 * This method processes client request for viewing news message
	 *  and sends web page address string as response to it
	 * @param request HTTP request
	 * @return address string for web page ViewMessage
	 * @throws CommandException
	 */
	@Override
	public String process(HttpServletRequest request) throws CommandException {
		try {
			long id = Long.parseLong(request.getParameter("newsId"));
			NewsTO news = facadeNewsService.viewMessage(id);
			//Setting news message, author and comments attributes.
			request.setAttribute("msg", news);
			int pageNumber;
			List<Long> list;
			boolean filtered = false;
			SearchCriteriaObject sco = new SearchCriteriaObject();
			//if filter exists, then authorId!=null, even when using non-author search.
			if(request.getParameter("authorId")!=null){
				filtered = true;
				//Setting author in filter
				sco.setAuthorId(Long.valueOf(request.getParameter("authorId")));
				//Setting tags in filter
				if(!request.getParameter("tagList").equals("[]")){
					String tags = request.getParameter("tagList");
					tags = tags.substring(1, tags.length()-1);
					tags = tags.replace(" ", "");
					ArrayList<Long> ar = new ArrayList<Long>();
					String tagList[] = tags.split(",");
					for(String s : tagList){
						ar.add(Long.valueOf(s));
					}
					sco.setTags(ar);
				} else {
					sco.setTags(new ArrayList<Long>());
				}
				//Setting filter attribute.
				request.setAttribute("filter", sco);
			}
			//Calculating page number of the news and getting list of ids of the news
			//on that page
			if(filtered) {
				pageNumber = newsService.findSearchPageIndex(sco, id);
				list = newsService.viewSearchIndexes(sco, pageNumber);
			}else {
				pageNumber = newsService.findPageIndex(id);
				list = newsService.viewNewsIndexes(pageNumber);
			}
			int index = list.indexOf(id);
			//Throwing exception if news was not found in the list
			if(index == -1){
				throw new CommandException("News not found");
			}
			boolean first = false, last = false;
			//Checking if page is first, last or neither of that in the list of all news
			if(pageNumber==1 && index ==0){
				first = true;
			}
			if(filtered){
				int lastPage = (int) Math.ceil((double)newsService.countSearchResult(sco)/5);
				if(pageNumber== lastPage && index==list.size()-1){
					last = true;
				}
			}else{
				int lastPage = (int) Math.ceil((double)newsService.countNews()/5);
				if(pageNumber== lastPage && index==list.size()-1){
					last = true;
				}
			}
			//Setting previous news id attribute
			if(!first){
				long k = 0;
				if(index !=0) {
					k=list.get(index-1);
				}else {
					if(filtered) {
						k = newsService.viewSearchIndexes(sco, pageNumber-1).get(4);
					}else {
						k = newsService.viewNewsIndexes(pageNumber-1).get(4);
					}
				}
				request.setAttribute("previous", k);
			}
			//Setting next news id attribute
			if(!last){
				long k = 0;
				if(index !=4) {
					k=list.get(index+1);
				}else {
					if(filtered) {
						k = newsService.viewSearchIndexes(sco, pageNumber+1).get(0);
					}else {
						k = newsService.viewNewsIndexes(pageNumber+1).get(0);
					}
				}
				request.setAttribute("next", k);
			}
			request.setAttribute("page", pageNumber);
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
