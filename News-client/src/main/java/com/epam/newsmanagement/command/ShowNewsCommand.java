package com.epam.newsmanagement.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.entity.dataobject.SearchCriteriaObject;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorServiceInterface;
import com.epam.newsmanagement.service.FacadeNewsServiceInterface;
import com.epam.newsmanagement.service.NewsServiceInterface;
import com.epam.newsmanagement.service.TagServiceInterface;

public class ShowNewsCommand implements NewsCommand {

private static final String PAGE = "WEB-INF/pages/NewsList.jsp";
	
	@Autowired
	private FacadeNewsServiceInterface facadeNewsService;
	@Autowired
	private NewsServiceInterface newsService;
	@Autowired
	private AuthorServiceInterface authorService;
	@Autowired
	private TagServiceInterface tagService;
	
	@Override
	public String process(HttpServletRequest request) throws CommandException {
		try {
			SearchCriteriaObject sco = new SearchCriteriaObject();
			int pageNumber = Integer.parseInt(request.getParameter("page"));
			int lastPage = (int) Math.ceil((double)newsService.countNews()/5);
			ArrayList<Integer> pages = new ArrayList<Integer>();
			for(int i=1; i<=lastPage; i++){
				pages.add(i);
			}
			request.setAttribute("pages", pages);
			sco.setTags(new ArrayList<Long>());
			request.setAttribute("filter", sco);
			request.setAttribute("filtered", false);
			request.setAttribute("NewsList", facadeNewsService.viewSortedNews(pageNumber));
			request.setAttribute("AuthorList", authorService.viewAuthors());
			request.setAttribute("TagList", tagService.viewTags());
			request.setAttribute("page", pageNumber);
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
