package com.epam.newsmanagement.command;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagement.exception.CommandException;

public interface NewsCommand {

	String process(HttpServletRequest request) throws CommandException;
}
