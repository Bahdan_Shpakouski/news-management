<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<c:forEach items="${requestScope.authorList}" var="author">
	<form id="f${author.getAuthorId()}" method="post">
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/> 
		<p><b>Author:</b>
		<c:choose>
		<c:when test="${author.getExpired()==null }">
			<input type="text" name="authorName" value="${author.getAuthorName()}" size="35" maxlength="30">
			<input type="hidden" name="authorId" value="${author.getAuthorId()}">
			<a href="#" onClick="javascript:showButtons(${author.getAuthorId()});" 
			id="a${author.getAuthorId()}" style="display: inline">Edit</a>
			<a href="#" onClick="javascript:update(${author.getAuthorId()});" 
				id="b${author.getAuthorId()}" style="display: none">Update</a>
			<a href="#" onClick="javascript:expire(${author.getAuthorId()});" 
				id="c${author.getAuthorId()}" style="display: none">Expire</a>
			<a href="#" onClick="javascript:hideButtons(${author.getAuthorId()});" 
				id="d${author.getAuthorId()}" style="display: none">Cancel</a>
		</c:when>
		<c:otherwise>
			<input type="text" name="authorName" value="${author.getAuthorName()}" disabled><b>Author expired</b>
		</c:otherwise>
		</c:choose>
		</p>
	</form>
	</c:forEach>
	<br>
	<form action="addAuthor" id="addForm" method="post"><p>
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/> 
	<input type="text" name="authorName" size="35" maxlength="30" required>
	<a href="#" onClick="javascript:add();" >Add author</a>
	</p>
	</form>
<script>
function showButtons(id){
	document.getElementById("a"+id).style.display = "none";
	document.getElementById("b"+id).style.display = "inline";
	document.getElementById("c"+id).style.display = "inline";
	document.getElementById("d"+id).style.display = "inline";
}
function hideButtons(id){
	document.getElementById("a"+id).style.display = "inline";
	document.getElementById("b"+id).style.display = "none";
	document.getElementById("c"+id).style.display = "none";
	document.getElementById("d"+id).style.display = "none";
}
function update(id){
	document.getElementById("f"+id).action="updateAuthor";
	document.getElementById("f"+id).submit();
}
function expire(id){
	document.getElementById("f"+id).action="expireAuthor";
	document.getElementById("f"+id).submit();
}
function add(){
	document.getElementById("addForm").submit();
}
</script>
</body>
</html>