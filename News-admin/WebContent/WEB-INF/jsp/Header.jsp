<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<h1>News Portal</h1>
	<c:if test="${pageContext.request.userPrincipal.name != null}">
	<form method="post" action="logout">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/> 
	<p>Hello, Admin ${pageContext.request.userPrincipal.name}
	<button>Sign out</button>
	</p></form>
	</c:if>
</body>
</html>