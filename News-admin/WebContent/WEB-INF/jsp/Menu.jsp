<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Menu</title>
</head>
<body>
	<form action="controller" id="form1">
		<input type="hidden" name="page" value="1">
		<a href="#" onClick="javascript:submitMenu(1)"><b>News List</b></a><br>
		<a href="#" onClick="javascript:submitMenu(2)"><b>Add News</b></a><br>
		<a href="#" onClick="javascript:submitMenu(3)"><b>Add/Update Authors</b></a><br>
		<a href="#" onClick="javascript:submitMenu(4)"><b>Add/Update Tags</b></a>
	</form>
</body>
<script>
function submitMenu(mode) {
	switch(mode){
	case 1:
		document.getElementById('form1').action="showNews";
		break;
	case 2:
		document.getElementById('form1').action="addMessageForm";
		break;
	case 3:
		document.getElementById('form1').action="editAuthors";
		break;
	case 4:
		document.getElementById('form1').action="editTags";
		break;
	}
	document.getElementById('form1').submit();
}
</script>
</html>