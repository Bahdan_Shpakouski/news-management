<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<c:forEach items="${requestScope.tagList}" var="tag">
	<form id="f${tag.getTagId()}" method="post">
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/> 
		<p><b>Tag:</b>
		<input type="text" name="tagName" value="${tag.getTagName()}" size="35" maxlength="30">
		<input type="hidden" name="tagId" value="${tag.getTagId()}">
		<a href="#" onClick="javascript:showButtons(${tag.getTagId()});" 
		id="a${tag.getTagId()}" style="display: inline">Edit</a>
		<a href="#" onClick="javascript:update(${tag.getTagId()});" 
			id="b${tag.getTagId()}" style="display: none">Update</a>
		<a href="#" onClick="javascript:deleteTag(${tag.getTagId()});" 
			id="c${tag.getTagId()}" style="display: none">Delete</a>
		<a href="#" onClick="javascript:hideButtons(${tag.getTagId()});" 
			id="d${tag.getTagId()}" style="display: none">Cancel</a>
		</p>
	</form>
	</c:forEach>
	<br>
	<form action="addTag" id="addForm" method="post"><p>
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/> 
	<input type="text" name="tagName" size="35" maxlength="30" required>
	<a href="#" onClick="javascript:add();" >Add tag</a>
	</p>
	</form>
<script>
function showButtons(id){
	document.getElementById("a"+id).style.display = "none";
	document.getElementById("b"+id).style.display = "inline";
	document.getElementById("c"+id).style.display = "inline";
	document.getElementById("d"+id).style.display = "inline";
}
function hideButtons(id){
	document.getElementById("a"+id).style.display = "inline";
	document.getElementById("b"+id).style.display = "none";
	document.getElementById("c"+id).style.display = "none";
	document.getElementById("d"+id).style.display = "none";
}
function update(id){
	document.getElementById("f"+id).action="updateTag";
	document.getElementById("f"+id).submit();
}
function deleteTag(id){
	document.getElementById("f"+id).action="deleteTag";
	document.getElementById("f"+id).submit();
}
function add(){
	document.getElementById("addForm").submit();
}
</script>
</body>
</html>