<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>
<c:choose>
	<c:when test="${Message==null}">Add message</c:when>
	<c:otherwise>Edit message</c:otherwise>
</c:choose></title>
</head>
<body>
	<tiles:insertDefinition name="AddNews"></tiles:insertDefinition>
</body>
</html>