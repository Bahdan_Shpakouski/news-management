<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
	<form method="post">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/> 
	<input type="hidden" name="newsId" value="${Message.getNews().getNewsId()}">
	<table>
	<tr>
		<td>Title:</td><td><input type="text" name="title" size="35" maxlength="30" value="${Message.getNews().getTitle()}" required></td>
	</tr>
	<tr>
		<td>Date:</td><td><input type="text" id="date" disabled size="60" required></td>
	</tr>
	<tr>
		<td>Brief:</td><td><textarea  rows="3" name="shortText" cols="70" 
			maxlength="100" required><c:out value="${Message.getNews().getShortText()}"/></textarea></td>
	</tr>
	<tr>
		<td>Content:</td><td><textarea  rows="10" name="fullText" cols="70" maxlength="2000" required>
		<c:out value="${Message.getNews().getFullText()}"/></textarea></td>
	</tr>
	</table>
		<p align="center" id="tagOut"></p>
		<p align = "center">
		<select name="authorId">
			<c:forEach items="${requestScope.AuthorList}" var="author">
				<option value="${author.getAuthorId()}" 
				<c:if test="${author.getAuthorId().equals(Message.getAuthor().getAuthorId())}">selected</c:if>>	
				<c:out value="${author.getAuthorName()}"/></option>
			</c:forEach>
		</select>
		<select name="tag dropdown" onChange="javascript:addTag();" id="sl">
			<option value="0">...</option>
			<c:forEach items="${requestScope.TagList}" var="tag">
				<option value="${tag.getTagId()}" id="opt${tag.getTagId()}">
				<c:out value="${tag.getTagName()}"/></option>
			</c:forEach>
		</select>
		<c:choose>
			<c:when test="${Message!=null}">
				<input type="hidden" id="tagList" value="${fn:escapeXml(UsedTags)}" name="tagList">
			</c:when>
			<c:otherwise>
				<input type="hidden" id="tagList" value="[]" name="tagList">
			</c:otherwise>
		</c:choose>
		<button type="button" name="reset" onClick="javascript:resetTags();">Reset tags</button>
		<c:choose>
			<c:when test="${Message==null}">
				<button formaction="addNews">Save</button>
			</c:when>
			<c:otherwise>
				<button formaction="editNews">Save</button>
			</c:otherwise>
		</c:choose>
		</p>
	</form>
<script>
function pad(n){
	return n<10 ? '0'+n : n
}

var date = new Date();
document.getElementById('date').value=
	date.getFullYear()+"-"+pad(date.getMonth()+1)+"-"+pad(date.getDate());
Array.prototype.contains = function(elem){
	for (var i in this)			{
		if (this[i] == elem) return true;
	}
	return false;
}

var t = JSON.parse(document.getElementById("tagList").value);
var t2 = [];
document.getElementById("tagOut").innerHTML = "Tags: ";
for(i=0; i<t.length; i++){
	document.getElementById("tagOut").innerHTML += t[i].tagName + " ";
	t2.push(t[i].tagId);
}
document.getElementById("tagList").value = "["+t2+"]";
function resetTags(){
	document.getElementById("sl").value = 0;
	document.getElementById("tagOut").innerHTML = "Tags:";
	document.getElementById("tagList").value = "[]";
}
function addTag(){
	var tags= JSON.parse(document.getElementById("tagList").value);
	var sl = document.getElementById("sl").value;
	if(!tags.contains(sl)){
		tags.push(document.getElementById("sl").value);
		document.getElementById("tagOut").innerHTML = "Tags: ";
		for(i=0; i<tags.length; i++){
			document.getElementById("tagOut").innerHTML += 
				document.getElementById("opt"+tags[i]).text + " ";
		}
		document.getElementById("sl").value = 0;
		document.getElementById("tagList").value = '['+tags+']';
	}else{
		document.getElementById("sl").value = 0;
		alert("Tag already selected!");
	}
}
</script>
</body>
</html>