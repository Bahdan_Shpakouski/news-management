package com.epam.newsmanagement.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagServiceInterface;

@Controller
public class TagController {

	private static final String PAGE = "TagForm";
	private static final String ERROR ="Error";
	
	@Autowired
	private TagServiceInterface tagService;
	
	static Logger logger = Logger.getLogger(NewsController.class);
	
	@RequestMapping("/editTags")
	public String addMessageForm(ModelMap model){
		try {
			model.addAttribute("tagList", tagService.viewTags());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
			return ERROR;
		}
		return PAGE;
	}
	
	@RequestMapping("/updateTag")
	public String update(@RequestParam long tagId, @RequestParam String tagName, 
			ModelMap model){
		try {
			Tag tag = new Tag();
			tag.setTagId(tagId);
			tag.setTagName(tagName);
			tagService.updateTag(tag, tag.getTagId());
			model.addAttribute("tagList", tagService.viewTags());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
			return ERROR;
		}
		return PAGE;
	}
	
	@RequestMapping("/deleteTag")
	public String delete(@RequestParam long tagId,	ModelMap model){
		try {
			tagService.deleteTag(tagId);
			model.addAttribute("tagList", tagService.viewTags());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
			return ERROR;
		}
		return PAGE;
	}
	
	@RequestMapping("/addTag")
	public String add(@RequestParam String tagName, ModelMap model){
		try {
			Tag tag = new Tag();
			tag.setTagName(tagName);
			tagService.createTag(tag);
			model.addAttribute("tagList", tagService.viewTags());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
			return ERROR;
		}
		return PAGE;
	}
}
