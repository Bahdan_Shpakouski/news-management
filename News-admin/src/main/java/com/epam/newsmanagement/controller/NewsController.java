package com.epam.newsmanagement.controller;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.dataobject.SearchCriteriaObject;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorServiceInterface;
import com.epam.newsmanagement.service.FacadeNewsServiceInterface;
import com.epam.newsmanagement.service.NewsServiceInterface;
import com.epam.newsmanagement.service.TagServiceInterface;

@Controller
public class NewsController {

	private static final String LIST_PAGE = "NewsList";
	private static final String ERROR ="Error";
	
	@Autowired
	private FacadeNewsServiceInterface facadeNewsService;
	@Autowired
	private NewsServiceInterface newsService;
	@Autowired
	private AuthorServiceInterface authorService;
	@Autowired
	private TagServiceInterface tagService;
	
	static Logger logger = Logger.getLogger(NewsController.class);
	
	@RequestMapping("/showNews")
	public String showNews(@RequestParam("page") int pageNumber, ModelMap model){
		try {
			SearchCriteriaObject sco = new SearchCriteriaObject();
			int lastPage = (int) Math.ceil((double)newsService.countNews()/5);
			ArrayList<Integer> pages = new ArrayList<Integer>();
			for(int i=1; i<=lastPage; i++){
				pages.add(i);
			}
			model.addAttribute("pages", pages);
			sco.setTags(new ArrayList<Long>());
			model.addAttribute("filter", sco);
			model.addAttribute("filtered", false);
			model.addAttribute("NewsList", facadeNewsService.viewSortedNews(pageNumber));
			model.addAttribute("AuthorList", authorService.viewAuthors());
			model.addAttribute("TagList", tagService.viewTags());
			model.addAttribute("page", pageNumber);
		} catch (ServiceException e) {
			logger.error(e.getMessage());
			return ERROR;
		}
		return LIST_PAGE;
	}
	

	@RequestMapping("/filterNews")
	public String filterNews(@RequestParam("page") int pageNumber, @RequestParam long authorId,
			@RequestParam("tagList") String tags, ModelMap model){
		try {
			SearchCriteriaObject sco = new SearchCriteriaObject();
			sco.setAuthorId(authorId);
			if(!tags.equals("[]")){
				tags = tags.substring(1, tags.length()-1);
				tags = tags.replace(" ", "");
				ArrayList<Long> ar = new ArrayList<Long>();
				String tagList[] = tags.split(",");
				for(String s : tagList){
					ar.add(Long.valueOf(s));
				}
				sco.setTags(ar);
			} else {
				sco.setTags(new ArrayList<Long>());
			}
			int lastPage = (int) Math.ceil((double)newsService.countSearchResult(sco)/5);
			ArrayList<Integer> pages = new ArrayList<Integer>();
			for(int i=1; i<=lastPage; i++){
				pages.add(i);
			}
			model.addAttribute("pages", pages);
			model.addAttribute("filter", sco);
			model.addAttribute("filtered", true);
			model.addAttribute("NewsList", facadeNewsService.searchNews(sco, pageNumber));
			model.addAttribute("AuthorList", authorService.viewAuthors());
			model.addAttribute("TagList", tagService.viewTags());
			model.addAttribute("page", pageNumber);
		} catch (ServiceException e) {
			logger.error(e.getMessage());
			return ERROR;
		}
		return LIST_PAGE;
	}
	
	
}
