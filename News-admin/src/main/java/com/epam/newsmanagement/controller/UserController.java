package com.epam.newsmanagement.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UserController {

	private static final String LOGIN_PAGE = "Login";
	
	@RequestMapping("/loginPage")
	public String showLoginPage(@RequestParam(value="error", required=false) Integer error,
			ModelMap model){
		if(error!=null){
			if(error==1){
				model.addAttribute("error", "Bad authorization");
			}else{
				model.addAttribute("error", "Access denied");
			}
		}
		return LOGIN_PAGE;
	}
}
