package com.epam.newsmanagement.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.entity.dataobject.NewsTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorServiceInterface;
import com.epam.newsmanagement.service.FacadeNewsServiceInterface;
import com.epam.newsmanagement.service.NewsServiceInterface;
import com.epam.newsmanagement.service.TagServiceInterface;

@Controller
public class EditNewsController {

	private static final String FORM_PAGE = "AddMessage";
	private static final String REDIRECT_TO_MESSAGE = "redirect:viewMessage";
	private static final String ERROR ="Error";
	
	@Autowired
	private FacadeNewsServiceInterface facadeNewsService;
	@Autowired
	private NewsServiceInterface newsService;
	@Autowired
	private AuthorServiceInterface authorService;
	@Autowired
	private TagServiceInterface tagService;
	
	static Logger logger = Logger.getLogger(NewsController.class);
	
	@RequestMapping("/addMessageForm")
	public String addMessageForm(ModelMap model){
		try{
			model.addAttribute("AuthorList", authorService.viewNotExpiredAuthors());
			model.addAttribute("TagList", tagService.viewTags());
		}catch (ServiceException e){
			logger.error(e.getMessage());
			return ERROR;
		}
		return FORM_PAGE;
	}
	
	@RequestMapping("/editMessageForm")
	public String editMessageForm(@RequestParam long newsId, @RequestParam int page,
			ModelMap model){
		String result = FORM_PAGE;
		try{
			ObjectMapper mapper = new ObjectMapper();
			NewsTO msg = facadeNewsService.viewMessage(newsId);
			model.addAttribute("Message", msg);
			model.addAttribute("page", page);
			model.addAttribute("AuthorList", authorService.viewNotExpiredAuthors());
			model.addAttribute("TagList", tagService.viewTags());
			model.addAttribute("UsedTags", mapper.writeValueAsString(msg.getTags()));
		}catch (ServiceException e){
			logger.error(e.getMessage());
			result = ERROR;
		} catch (JsonGenerationException e) {
			logger.error(e.getMessage());
			result = ERROR;
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
			result = ERROR;
		} catch (IOException e) {
			logger.error(e.getMessage());
			result = ERROR;
		}
		return result;
	}
	
	@RequestMapping("/editNews")
	public String editNews(@RequestParam Map<String,String> params, ModelMap model){
		NewsTO newsTO = new NewsTO();
		News news = new News();
		news.setTitle(params.get("title"));
		news.setShortText(params.get("shortText"));
		news.setFullText(params.get("fullText"));
		news.setNewsId(Long.parseLong(params.get("newsId")));
		newsTO.setNews(news);
		long authorId = Long.parseLong(params.get("authorId"));
		try {
			newsTO.setAuthor(authorService.viewAuthor(authorId));
			if(!params.get("tagList").equals("[]")){
				String tags = params.get("tagList");
				tags = tags.substring(1, tags.length()-1);
				tags = tags.replace(" ", "");
				ArrayList<Tag> ar = new ArrayList<Tag>();
				String tagList[] = tags.split(",");
				if(!tags.equals("null")){
					for(String s : tagList){
						ar.add(tagService.viewTag(Long.valueOf(s)));
					}
				}
				newsTO.setTags(ar);
			} else {
				newsTO.setTags(new ArrayList<Tag>());
			}
			facadeNewsService.editMessage(newsTO, newsTO.getNews().getNewsId());
			model.addAttribute("newsId", newsTO.getNews().getNewsId());
			model.addAttribute("page", newsService.findPageIndex(newsTO
					.getNews().getNewsId()));
		} catch (ServiceException e) {
			logger.error(e.getMessage());
			return ERROR;
		}
		return REDIRECT_TO_MESSAGE;
	}
	
	@RequestMapping("/addNews")
	public String addNews(@RequestParam Map<String,String> params,
			ModelMap model){
		NewsTO newsTO = new NewsTO();
		News news = new News();
		news.setTitle(params.get("title"));
		news.setShortText(params.get("shortText"));
		news.setFullText(params.get("fullText"));
		newsTO.setNews(news);
		long authorId = Long.parseLong(params.get("authorId"));
		try{
			newsTO.setAuthor(authorService.viewAuthor(authorId));
			if(!params.get("tagList").equals("[]")){
				String tags = params.get("tagList");
				tags = tags.substring(1, tags.length()-1);
				tags = tags.replace(" ", "");
				ArrayList<Tag> ar = new ArrayList<Tag>();
				String tagList[] = tags.split(",");
				for(String s : tagList){
					ar.add(tagService.viewTag(Long.valueOf(s)));
				}
				newsTO.setTags(ar);
			} else {
				newsTO.setTags(new ArrayList<Tag>());
			}
			Long newsId = facadeNewsService.addNews(newsTO);
			model.addAttribute("newsId", newsId);
			model.addAttribute("page", newsService.findPageIndex(newsId));
		}catch (ServiceException e){
			logger.error(e.getMessage());
			return ERROR;
		}
		return REDIRECT_TO_MESSAGE;
	}
	
	@RequestMapping("/deleteMessage")
	public String deleteMessageForm(@RequestParam Map<String,String> params,
			ModelMap model){
		String result = "redirect:";
		try {
			long id = Integer.parseInt(params.get("newsId"));
			facadeNewsService.deleteNews(id);
			if(params.get("authorId")==null){
				result+="showNews";
			}else{
				result+="filterNews";
			}
			model.addAllAttributes(params);
			model.remove("newsId");
		} catch (ServiceException e) {
			logger.error(e.getMessage());
			return ERROR;
		}
		return result;
	}
}
