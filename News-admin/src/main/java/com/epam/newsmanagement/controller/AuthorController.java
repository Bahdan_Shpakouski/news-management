package com.epam.newsmanagement.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorServiceInterface;

@Controller
public class AuthorController {

	private static final String PAGE = "AuthorForm";
	private static final String ERROR ="Error";
	
	@Autowired
	private AuthorServiceInterface authorService;
	
	static Logger logger = Logger.getLogger(NewsController.class);
	
	@RequestMapping("/editAuthors")
	public String addMessageForm(ModelMap model){
		try {
			model.addAttribute("authorList", authorService.viewAuthors());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
			return ERROR;
		}
		return PAGE;
	}
	
	@RequestMapping("/updateAuthor")
	public String update(@RequestParam long authorId, @RequestParam String authorName, 
			ModelMap model){
		try {
			Author author = new Author();
			author.setAuthorId(authorId);
			author.setAuthorName(authorName);
			authorService.updateAuthor(author, author.getAuthorId());
			model.addAttribute("authorList", authorService.viewAuthors());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
			return ERROR;
		}
		return PAGE;
	}
	
	@RequestMapping("/expireAuthor")
	public String expire(@RequestParam("authorId") long id,
			ModelMap model){
		try {
			authorService.setExpired(id);
			model.addAttribute("authorList", authorService.viewAuthors());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
			return ERROR;
		}
		return PAGE;
	}
	
	@RequestMapping("/addAuthor")
	public String add(@RequestParam("authorName") String authorName,
			ModelMap model){
		try {
			Author author = new Author();
			author.setAuthorName(authorName);
			authorService.createAuthor(author);
			model.addAttribute("authorList", authorService.viewAuthors());
		} catch (ServiceException e) {
			logger.error(e.getMessage());
			return ERROR;
		}
		return PAGE;
	}
}
