package com.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.dataobject.NewsTO;
import com.epam.newsmanagement.entity.dataobject.SearchCriteriaObject;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentServiceInterface;
import com.epam.newsmanagement.service.FacadeNewsServiceInterface;
import com.epam.newsmanagement.service.NewsServiceInterface;

@Controller
public class MessageViewController {

	@Autowired
	private FacadeNewsServiceInterface facadeNewsService;
	@Autowired
	private NewsServiceInterface newsService;
	@Autowired
	private CommentServiceInterface commentService;
	
	private static final String PAGE = "ViewMessage";
	private static final String ERROR ="Error";
	
	static Logger logger = Logger.getLogger(NewsController.class);
	
	@RequestMapping("/viewMessage")
	public String showMessage(@RequestParam("newsId") long id, 
			@RequestParam(value="authorId", required=false) Long authorId, 
			@RequestParam(value = "tagList", required=false) String tags,
			 ModelMap model) {
		String result = null;
		try {
			result = prepareView(id, authorId, tags, model);
		} catch (ServiceException e) {
			logger.error(e);
			return ERROR;
		}
		return result;
	}
	
	@RequestMapping("/createComment")
	public String createComment(@RequestParam("newsId") long id, 
			@RequestParam(value="authorId", required=false) Long authorId, 
			@RequestParam(value = "tagList", required=false) String tags,
			@RequestParam("comment") String commentText, ModelMap model){
		String result = null;
		try {
			Comment comment = new Comment();
			comment.setCommentText(commentText);
			comment.setNewsId(id);
			commentService.createComment(comment);
			result = prepareView(id, authorId, tags, model);
		} catch (ServiceException e) {
			logger.error(e.getMessage());
			return ERROR;
		}
		return result;
	}
	
	/**
	 * This method processes client request and sends web page address string as response to it
	 * @param id id of the news
	 * @param authorId id of the author in the filter
	 * @param tags String containing array of tags in the filter
	 * @param model view model for setting attributes
	 * @return address string for web page ViewMessage
	 * @throws ServiceException
	 */
	private String prepareView(long id, Long authorId, String tags, 
			ModelMap model) throws ServiceException{
		NewsTO news = facadeNewsService.viewMessage(id);
		//Setting news message, author and comments attributes.
		model.addAttribute("msg", news);
		int pageNumber;
		List<Long> list;
		boolean filtered = false;
		SearchCriteriaObject sco = new SearchCriteriaObject();
		//if filter exists, then authorId!=null, even when using non-author search.
		if(authorId!=null){
			filtered = true;
			//Setting author in filter
			sco.setAuthorId(authorId);
			//Setting tags in filter
			if(!tags.equals("[]")){
				tags = tags.substring(1, tags.length()-1);
				tags = tags.replace(" ", "");
				ArrayList<Long> ar = new ArrayList<Long>();
				String tagList[] = tags.split(",");
				for(String s : tagList){
					ar.add(Long.valueOf(s));
				}
				sco.setTags(ar);
			} else {
				sco.setTags(new ArrayList<Long>());
			}
			//Setting filter attribute.
			model.addAttribute("filter", sco);
		}
		//Calculating page number of the news and getting list of ids of the news
		//on that page
		if(filtered) {
			pageNumber = newsService.findSearchPageIndex(sco, id);
			list = newsService.viewSearchIndexes(sco, pageNumber);
		}else {
			pageNumber = newsService.findPageIndex(id);
			list = newsService.viewNewsIndexes(pageNumber);
		}
		int index = list.indexOf(id);
		//Throwing exception if news was not found in the list
		if(index == -1){
			throw new ServiceException("News not found");
		}
		boolean first = false, last = false;
		//Checking if page is first, last or neither of that in the list of all news
		if(pageNumber==1 && index ==0){
			first = true;
		}
		if(filtered){
			int lastPage = (int) Math.ceil((double)newsService.countSearchResult(sco)/5);
			if(pageNumber== lastPage && index==list.size()-1){
				last = true;
			}
		}else{
			int lastPage = (int) Math.ceil((double)newsService.countNews()/5);
			if(pageNumber== lastPage && index==list.size()-1){
				last = true;
			}
		}
		//Setting previous news id attribute
		if(!first){
			long k = 0;
			if(index !=0) {
				k=list.get(index-1);
			}else {
				if(filtered) {
					k = newsService.viewSearchIndexes(sco, pageNumber-1).get(4);
				}else {
					k = newsService.viewNewsIndexes(pageNumber-1).get(4);
				}
			}
			model.addAttribute("previous", k);
		}
		//Setting next news id attribute
		if(!last){
			long k = 0;
			if(index !=4) {
				k=list.get(index+1);
			}else {
				if(filtered) {
					k = newsService.viewSearchIndexes(sco, pageNumber+1).get(0);
				}else {
					k = newsService.viewNewsIndexes(pageNumber+1).get(0);
				}
			}
			model.addAttribute("next", k);
		}
		model.addAttribute("page", pageNumber);
		return PAGE;
	}
}
