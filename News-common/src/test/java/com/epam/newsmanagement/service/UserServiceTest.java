package com.epam.newsmanagement.service;

import static org.junit.Assert.*;

import static org.mockito.Mockito.*;

import java.util.ArrayList;

import com.epam.newsmanagement.dao.jdbc.UserDAOImpl;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.UserService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

	@Mock
	private UserDAOImpl userDAO;
	
	@InjectMocks
	private UserService userService;
	
	@Test(expected = ServiceException.class)
	public void testCreateUserException() throws ServiceException{
		userService.createUser(null);
	}
	
	@Before
	public void setupMocks() throws ServiceException {
		try {
			when(userDAO.create(any(User.class))).thenReturn(10l);
			when(userDAO.login(any(User.class))).thenReturn(1l);
			when(userDAO.readAll()).thenReturn(new ArrayList<User>());
			when(userDAO.read(anyLong())).thenReturn(new User());
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}
	
	@Test
	public void testCreateUser() throws ServiceException {
		assertEquals(userService.createUser(new User()), (Long)10l);
	}
	
	@Test
	public void testCreateUserVerification() throws ServiceException{
		userService.createUser(new User());
		try {
			verify(userDAO, times(1)).create(new User());
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}
	
	@Test
	public void testLogin() throws ServiceException{
		assertEquals(userService.login(new User()), (Long)1l);
	}

	@Test
	public void testLoginVerification() throws ServiceException{
		userService.login(new User());
		try {
			verify(userDAO, times(1)).login(new User());
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}
	
	@Test(expected = ServiceException.class)
	public void testLoginException() throws ServiceException{
		userService.login(null);
	}

	@Test
	public void testUpdateUser() throws ServiceException {
		userService.updateUser(new User(), 1);
		try {
			verify(userDAO, times(1)).update(new User(), 1);
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}
	
	@Test(expected = ServiceException.class)
	public void testUpdateUserException() throws ServiceException {
		userService.updateUser(null, 1);
	}

	@Test
	public void testDeleteUser() throws ServiceException {
		userService.deleteUser(1);
		try {
			verify(userDAO, times(1)).delete(1);
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}

	@Test
	public void testViewUsers() throws ServiceException {
		assertEquals(userService.viewUsers(), new ArrayList<User>());
	}
	
	@Test
	public void testViewUsersVerification() throws ServiceException {
		userService.viewUsers();
		try {
			verify(userDAO, times(1)).readAll();
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}

	@Test
	public void testViewUser() throws ServiceException {
		assertEquals(userService.viewUser(1), new User());
	}

	@Test
	public void testViewUserVerification() throws ServiceException {
		userService.viewUser(1);
		try {
			verify(userDAO, times(1)).read(1);
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}

}
