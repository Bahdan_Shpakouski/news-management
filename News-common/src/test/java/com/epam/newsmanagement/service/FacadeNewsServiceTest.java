package com.epam.newsmanagement.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.entity.dataobject.NewsTO;
import com.epam.newsmanagement.entity.dataobject.SearchCriteriaObject;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.AuthorService;
import com.epam.newsmanagement.service.impl.CommentService;
import com.epam.newsmanagement.service.impl.FacadeNewsService;
import com.epam.newsmanagement.service.impl.NewsService;
import com.epam.newsmanagement.service.impl.TagService;

@RunWith(MockitoJUnitRunner.class)
public class FacadeNewsServiceTest {

	@Mock
	private NewsService newsService;
	@Mock
	private AuthorService authorService;
	@Mock
	private TagService tagService;
	@Mock
	private CommentService commentService;
	
	@InjectMocks
	private FacadeNewsService newsFacadeService;
	
	@Before
	public void setupMocks() throws ServiceException{
		ArrayList<News> newsList = new ArrayList<News>();
		newsList.add(new News());
		newsList.add(new News());
		when(newsService.addNews(new News())).thenReturn(10l);
		when(newsService.viewNews()).thenReturn(newsList);
		when(newsService.viewSortedNews()).thenReturn(newsList);
		when(newsService.viewSortedNews(anyInt())).thenReturn(newsList);
		when(newsService.viewMessage(anyLong())).thenReturn(new News());
		when(newsService.searchNews(any(SearchCriteriaObject.class)))
			.thenReturn(newsList);
		when(newsService.searchNews(any(SearchCriteriaObject.class), anyInt()))
			.thenReturn(newsList);
		ArrayList<Comment> commentList = new ArrayList<Comment>();
		commentList.add(new Comment());
		commentList.add(new Comment());
		when(commentService.viewComments(anyLong())).thenReturn(commentList);
		ArrayList<Tag> tagList = new ArrayList<Tag>();
		tagList.add(new Tag());
		tagList.add(new Tag());
		when(tagService.viewNewsTags(anyLong())).thenReturn(tagList);
		when(authorService.viewNewsAuthor(anyLong())).thenReturn(new Author());
	}
	
	@Test
	public void testAddNews() throws ServiceException {
		NewsTO newsTO = new NewsTO();
		newsTO.setAuthor(new Author());
		newsTO.setNews(new News());
		newsTO.setTags(new ArrayList<Tag>());
		newsTO.getTags().add(new Tag());
		newsTO.getTags().add(new Tag());
		Long id = newsFacadeService.addNews(newsTO);
		assertEquals((long)id, 10l);
	}
	
	@Test
	public void testAddNewsVerification() throws ServiceException {
		NewsTO newsTO = new NewsTO();
		newsTO.setAuthor(new Author());
		newsTO.setNews(new News());
		newsTO.setTags(new ArrayList<Tag>());
		newsTO.getTags().add(new Tag());
		newsTO.getTags().add(new Tag());
		newsFacadeService.addNews(newsTO);
		verify(newsService, times(1)).addNews(new News());
		verify(authorService, times(1)).viewAuthor(anyLong());
		verify(newsService, times(1)).addAuthor(anyLong(), anyLong());
		verify(newsService, times(2)).addTag(anyLong(), anyLong());
	}
	
	@Test(expected = ServiceException.class)
	public void testAddNewsException() throws ServiceException {
		newsFacadeService.addNews(null);
	}
	
	@Test
	public void testDeleteNews() throws ServiceException {
		newsFacadeService.deleteNews(1);
		verify(commentService, times(1)).deleteNewsComments(1);
		verify(newsService, times(1)).deleteNews(1);
	}

	@Test
	public void testViewNews() throws ServiceException {
		ArrayList<NewsTO> list = (ArrayList<NewsTO>) newsFacadeService.viewNews();
		assertTrue(list.size()==2);
		for(int i=0; i< list.size(); i++){
			assertEquals(list.get(i).getNews(), new News());
			assertEquals(list.get(i).getAuthor(), new Author());
			assertEquals(list.get(i).getComments().size(), 2);
			assertEquals(list.get(i).getTags().size(), 2);
		}
	}
	
	@Test
	public void testViewNewsVerification() throws ServiceException {
		newsFacadeService.viewNews();
		verify(newsService, times(1)).viewNews();
		verify(commentService, times(2)).viewComments(anyLong());
		verify(authorService, times(2)).viewNewsAuthor(anyLong());
		verify(tagService, times(2)).viewNewsTags(anyLong());
	}

	@Test
	public void testViewSortedNews() throws ServiceException {
		ArrayList<NewsTO> list = (ArrayList<NewsTO>) newsFacadeService.viewSortedNews();
		assertTrue(list.size()==2);
		for(int i=0; i< list.size(); i++){
			assertEquals(list.get(i).getNews(), new News());
			assertEquals(list.get(i).getAuthor(), new Author());
			assertEquals(list.get(i).getComments().size(), 2);
			assertEquals(list.get(i).getTags().size(), 2);
		}
	}

	@Test
	public void testViewSortedNewsVerification() throws ServiceException {
		newsFacadeService.viewSortedNews();
		verify(newsService, times(1)).viewSortedNews();
		verify(commentService, times(2)).viewComments(anyLong());
		verify(authorService, times(2)).viewNewsAuthor(anyLong());
		verify(tagService, times(2)).viewNewsTags(anyLong());
	}
	
	@Test
	public void testViewSortedNews2() throws ServiceException {
		ArrayList<NewsTO> list = (ArrayList<NewsTO>) newsFacadeService.viewSortedNews(1);
		assertTrue(list.size()==2);
		for(int i=0; i< list.size(); i++){
			assertEquals(list.get(i).getNews(), new News());
			assertEquals(list.get(i).getAuthor(), new Author());
			assertEquals(list.get(i).getComments().size(), 2);
			assertEquals(list.get(i).getTags().size(), 2);
		}
	}

	@Test
	public void testViewSortedNews2Verification() throws ServiceException {
		newsFacadeService.viewSortedNews(1);
		verify(newsService, times(1)).viewSortedNews(1);
		verify(commentService, times(2)).viewComments(anyLong());
		verify(authorService, times(2)).viewNewsAuthor(anyLong());
		verify(tagService, times(2)).viewNewsTags(anyLong());
	}
	
	@Test
	public void testViewMessage() throws ServiceException {
		NewsTO newsTO = newsFacadeService.viewMessage(10);
		assertEquals(newsTO.getNews(), new News());
		assertEquals(newsTO.getAuthor(), new Author());
		assertEquals(newsTO.getComments().size(), 2);
		assertEquals(newsTO.getTags().size(), 2);
	}
	
	@Test
	public void testViewMessageVerification() throws ServiceException {
		newsFacadeService.viewMessage(10);
		verify(newsService, times(1)).viewMessage(10);
		verify(commentService, times(1)).viewComments(10);
		verify(authorService, times(1)).viewNewsAuthor(10);
		verify(tagService, times(1)).viewNewsTags(10);
	}

	@Test
	public void testSearchNews() throws ServiceException {
		ArrayList<NewsTO> list = (ArrayList<NewsTO>) newsFacadeService
				.searchNews(new SearchCriteriaObject());
		assertTrue(list.size()==2);
		for(int i=0; i< list.size(); i++){
			assertEquals(list.get(i).getNews(), new News());
			assertEquals(list.get(i).getAuthor(), new Author());
			assertEquals(list.get(i).getComments().size(), 2);
			assertEquals(list.get(i).getTags().size(), 2);
		}
	}
	
	@Test
	public void testSearchNewsVerification() throws ServiceException {
		newsFacadeService.searchNews(new SearchCriteriaObject());
		verify(newsService, times(1)).searchNews(new SearchCriteriaObject());
		verify(commentService, times(2)).viewComments(anyLong());
		verify(authorService, times(2)).viewNewsAuthor(anyLong());
		verify(tagService, times(2)).viewNewsTags(anyLong());
	}
	
	@Test(expected = ServiceException.class)
	public void testSearchNewsException() throws ServiceException {
		newsFacadeService.searchNews(null);
	}
	
	@Test
	public void testSearchNews2() throws ServiceException {
		ArrayList<NewsTO> list = (ArrayList<NewsTO>) newsFacadeService
				.searchNews(new SearchCriteriaObject(), 1);
		assertTrue(list.size()==2);
		for(int i=0; i< list.size(); i++){
			assertEquals(list.get(i).getNews(), new News());
			assertEquals(list.get(i).getAuthor(), new Author());
			assertEquals(list.get(i).getComments().size(), 2);
			assertEquals(list.get(i).getTags().size(), 2);
		}
	}
	
	@Test
	public void testSearchNews2Verification() throws ServiceException {
		newsFacadeService.searchNews(new SearchCriteriaObject(), 1);
		verify(newsService, times(1)).searchNews(new SearchCriteriaObject(), 1);
		verify(commentService, times(2)).viewComments(anyLong());
		verify(authorService, times(2)).viewNewsAuthor(anyLong());
		verify(tagService, times(2)).viewNewsTags(anyLong());
	}
	
	@Test(expected = ServiceException.class)
	public void testSearchNews2Exception() throws ServiceException {
		newsFacadeService.searchNews(null, 1);
	}
	
	@Test
	public void testEditMessage() throws ServiceException{
		NewsTO newsTO = new NewsTO();
		newsTO.setAuthor(new Author());
		newsTO.getAuthor().setAuthorId(10l);
		newsTO.setNews(new News());
		newsTO.setTags(new ArrayList<Tag>());
		newsTO.getTags().add(new Tag());
		newsTO.getTags().add(new Tag());
		newsTO.getTags().add(new Tag());
		newsFacadeService.editMessage(newsTO, 1);
		verify(newsService, times(1)).editNews(new News(), 1);
		verify(authorService, times(1)).viewNewsAuthor(1);
		verify(tagService, times(1)).viewNewsTags(1);
		verify(newsService, times(1)).removeAuthor(0, 1);
		verify(newsService, times(1)).addAuthor(10, 1);
		verify(newsService, times(1)).removeTags(1);
		verify(newsService, times(3)).addTag(0, 1);
	}
	
	@Test(expected = ServiceException.class)
	public void testEditMessageException() throws ServiceException {
		newsFacadeService.editMessage(null, 1);
	}

}
