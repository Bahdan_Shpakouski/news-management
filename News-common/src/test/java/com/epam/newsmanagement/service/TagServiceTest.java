package com.epam.newsmanagement.service;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.jdbc.TagDAOImpl;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.TagService;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {

	@Mock
	private TagDAOImpl tagDAO;
	@InjectMocks
	private TagService tagService;
	
	@Before
	public void setupMocks() throws ServiceException {
		try {
			when(tagDAO.create(any(Tag.class))).thenReturn(64l);
			when(tagDAO.read(anyLong())).thenReturn(new Tag());
			when(tagDAO.readAll()).thenReturn(new ArrayList<Tag>());
			when(tagDAO.readNewsTags(anyLong()))
				.thenReturn(new ArrayList<Tag>());
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}
	
	@Test
	public void testCreateTag() throws ServiceException {
		assertEquals(tagService.createTag(new Tag()), (Long)64l);
	}
	
	@Test
	public void testCreateTagVerification() throws ServiceException {
		tagService.createTag(new Tag());
		try {
			verify(tagDAO, times(1)).create(new Tag());
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test(expected =  ServiceException.class)
	public void testCreateTagException() throws ServiceException {
		tagService.createTag(null);
	}

	@Test
	public void testViewTags() throws ServiceException{
		assertEquals(tagService.viewTags(), new ArrayList<Tag>());
	}
	
	@Test
	public void testViewTagsVerification() throws ServiceException{
		tagService.viewTags();
		try {
			verify(tagDAO, times(1)).readAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Test
	public void testViewTag() throws ServiceException{
		assertEquals(tagService.viewTag(1), new Tag());
	}

	@Test
	public void testViewTagVerification() throws ServiceException{
		tagService.viewTag(1);
		try {
			verify(tagDAO, times(1)).read(1);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test
	public void testUpdateTag() throws ServiceException{
		tagService.updateTag(new Tag(), 5);
		try {
			verify(tagDAO, times(1)).update(new Tag(), 5);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Test(expected =  ServiceException.class)
	public void testUpdateTagException() throws ServiceException {
		tagService.updateTag(null, 5);
	}
	
	@Test
	public void testViewNewsTags() throws ServiceException{
		assertEquals(tagService.viewNewsTags(1), new ArrayList<Tag>());
	}
	
	@Test
	public void testViewNewsTagsVerification() throws ServiceException{
		tagService.viewNewsTags(5);
		try {
			verify(tagDAO, times(1)).readNewsTags(5);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test
	public void testDeleteTag() throws ServiceException{
		tagService.deleteTag(1);
		try{
			verify(tagDAO, times(1)).removeTagFromAllNews(1);
			verify(tagDAO, times(1)).delete(1);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

}
