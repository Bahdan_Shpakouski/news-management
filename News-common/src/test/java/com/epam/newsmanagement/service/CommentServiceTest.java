package com.epam.newsmanagement.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.jdbc.CommentDAOImpl;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.CommentService;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {

	@Mock
	private CommentDAOImpl commentDAO;
	@InjectMocks
	private CommentService commentService;
	
	@Before
	public void setupMocks() throws ServiceException{
		try {
			when(commentDAO.readNewsComments(anyLong()))
				.thenReturn(new ArrayList<Comment>());
			when(commentDAO.read(anyLong())).thenReturn(new Comment());
			when(commentDAO.create(any(Comment.class))).thenReturn(20l);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test
	public void testViewComments() throws ServiceException {
		assertEquals(commentService.viewComments(1), new ArrayList<Comment>());
	}
	
	@Test
	public void testViewCommentsVerification() throws ServiceException {
		commentService.viewComments(1);
		try {
			verify(commentDAO, times(1)).readNewsComments(1);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Test
	public void testViewComment() throws ServiceException {
		assertEquals(commentService.viewComment(1), new Comment());
	}
	
	@Test
	public void testViewCommentVerification() throws ServiceException {
		commentService.viewComment(1);
		try {
			verify(commentDAO, times(1)).read(1);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Test
	public void testCreateComment() throws ServiceException {
		assertEquals(commentService.createComment(new Comment()), (Long)20l);
	}
	
	@Test
	public void testCreateCommentVerification() throws ServiceException {
		commentService.createComment(new Comment());
		try {
			verify(commentDAO, times(1)).create(new Comment());
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test(expected = ServiceException.class)
	public void testCreateCommentException() throws ServiceException {
		commentService.createComment(null);
	}

	@Test
	public void testUpdateComment() throws ServiceException {
		commentService.updateComment(new Comment(), 1);
		try {
			verify(commentDAO, times(1)).update(new Comment(), 1);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test(expected = ServiceException.class)
	public void testUpdateCommentException() throws ServiceException {
		commentService.updateComment(null, 50);
	}

	@Test
	public void testDeleteComment() throws ServiceException {
		commentService.deleteComment(1);
		try {
			verify(commentDAO, times(1)).delete(1);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test
	public void testDeleteNewsComments() throws ServiceException {
		commentService.deleteNewsComments(1);
		try {
			verify(commentDAO, times(1)).deleteNewsComments(1);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

}
