package com.epam.newsmanagement.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.jdbc.AuthorDAOImpl;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.AuthorService;


@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {

	@Mock
	private AuthorDAOImpl authorDAO;
	
	@InjectMocks
	private AuthorService authorService;
	
	@Before
	public void setupMocks() throws ServiceException{
		try {
			when(authorDAO.create(any(Author.class))).thenReturn(20l);
			when(authorDAO.read(anyLong())).thenReturn(new Author());
			when(authorDAO.readNewsAuthor(anyLong())).thenReturn(new Author());
			when(authorDAO.readAll()).thenReturn(new ArrayList<Author>());
			when(authorDAO.readNotExpired()).thenReturn(new ArrayList<Author>());
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test
	public void testCreateAuthor() throws ServiceException {
		assertEquals(authorService.createAuthor(new Author()), (Long)20l);
	}
	
	@Test
	public void testCreateAuthorVerification() throws ServiceException {
		authorService.createAuthor(new Author());
		try {
			verify(authorDAO, times(1)).create(new Author());
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test(expected = ServiceException.class)
	public void testCreateAuthorException() throws ServiceException {
		authorService.createAuthor(null);
	}

	@Test
	public void testSetExpired() throws ServiceException {
		try {
			authorService.setExpired(1);
			verify(authorDAO, times(1)).read(1);
			verify(authorDAO, times(1)).update(any(Author.class), anyLong());
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test(expected = ServiceException.class)
	public void testSetExpiredException() throws ServiceException {
		try {
			when(authorDAO.read(-1)).thenReturn(null);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		authorService.setExpired(-1);
	}

	@Test
	public void testUpdateAuthor() throws ServiceException {
		authorService.updateAuthor(new Author(), 2);
		try {
			verify(authorDAO, times(1)).update(new Author(), 2);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test(expected = ServiceException.class)
	public void testUpdateAuthorException() throws ServiceException {
		authorService.updateAuthor(null, 5);
	}

	@Test
	public void testViewAuthors() throws ServiceException {
		assertEquals(authorService.viewAuthors(), new ArrayList<Author>());
	}
	
	@Test
	public void testViewAuthorsVerification() throws ServiceException {
		authorService.viewAuthors();
		try {
			verify(authorDAO, times(1)).readAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Test
	public void testViewAuthor() throws ServiceException {
		assertEquals(authorService.viewAuthor(1), new Author());
	}
	
	@Test
	public void testViewAuthorVerification() throws ServiceException {
		authorService.viewAuthor(1);
		try {
			verify(authorDAO, times(1)).read(1);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Test
	public void testViewNewsAuthor() throws ServiceException {
		assertEquals(authorService.viewNewsAuthor(1), new Author());
	}
	
	@Test
	public void testViewNewsAuthorVerification() throws ServiceException {
		authorService.viewAuthor(1);
		try {
			verify(authorDAO, times(1)).read(1);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test
	public void testViewNotExpiredAuthors() throws ServiceException {
		assertEquals(authorService.viewNotExpiredAuthors(), new ArrayList<Author>());
	}
	
	@Test
	public void testViewNotExpiredAuthorsVerification() throws ServiceException {
		authorService.viewNotExpiredAuthors();
		try {
			verify(authorDAO, times(1)).readNotExpired();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
