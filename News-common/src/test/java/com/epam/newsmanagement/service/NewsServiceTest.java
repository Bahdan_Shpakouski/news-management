package com.epam.newsmanagement.service;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.jdbc.NewsDAOImpl;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.dataobject.SearchCriteriaObject;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsService;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {

	@Mock
	private NewsDAOImpl newsDAO;
	@InjectMocks
	private NewsService newsService;
	
	@Before
	public void setupMocks() throws ServiceException {
		try {
			when(newsDAO.create(any(News.class))).thenReturn(1l);
			when(newsDAO.readAll()).thenReturn(new ArrayList<News>());
			when(newsDAO.readSorted()).thenReturn(new ArrayList<News>());
			when(newsDAO.readSorted(anyInt())).thenReturn(new ArrayList<News>());
			when(newsDAO.readIndexesSorted(anyInt())).thenReturn(new ArrayList<Long>());
			when(newsDAO.readSearchIndexes(any(SearchCriteriaObject.class),
					anyInt())).thenReturn(new ArrayList<Long>());
			when(newsDAO.read(anyLong())).thenReturn(new News());
			when(newsDAO.countNews()).thenReturn(10l);
			when(newsDAO.searchNews(any(SearchCriteriaObject.class)))
				.thenReturn(new ArrayList<News>());
			when(newsDAO.searchNews(any(SearchCriteriaObject.class), anyInt()))
				.thenReturn(new ArrayList<News>());
			when(newsDAO.countSearchResult(any(SearchCriteriaObject.class))).thenReturn(1l);
			when(newsDAO.findPageIndex(anyLong())).thenReturn(1);
			when(newsDAO.findSearchPageIndex(any(SearchCriteriaObject.class), anyLong())).thenReturn(1);
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}
	
	@Test
	public void testAddNews() throws ServiceException {
		assertEquals(newsService.addNews(new News()), (Long)1l);
	}
	
	@Test
	public void testAddNewsVerification() throws ServiceException {
		newsService.addNews(new News());
		try {
			verify(newsDAO, times(1)).create(new News());
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test(expected = ServiceException.class)
	public void testAddNewsException() throws ServiceException {
		newsService.addNews(null);
	}

	@Test
	public void testEditNews() throws ServiceException {
		newsService.editNews(new News(), 1);
		try {
			verify(newsDAO, times(1)).update(new News(), 1);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test(expected = ServiceException.class)
	public void testEditNewsException() throws ServiceException {
		newsService.editNews(null, 8);
	}

	@Test
	public void testDeleteNews() throws ServiceException {
		newsService.deleteNews(1);
		try {
			verify(newsDAO, times(1)).delete(1);
			verify(newsDAO, times(1)).removeAuthors(1);
			verify(newsDAO, times(1)).removeTags(1);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Test
	public void testViewNews() throws ServiceException {
		assertEquals(newsService.viewNews(), new ArrayList<News>());
	}
	
	@Test
	public void testViewNewsVerification() throws ServiceException {
		newsService.viewNews();
		try {
			verify(newsDAO, times(1)).readAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Test
	public void testViewSortedNews() throws ServiceException {
		assertEquals(newsService.viewSortedNews(), new ArrayList<News>());
	}
	
	@Test
	public void testViewSortedNewsVerification() throws ServiceException {
		newsService.viewSortedNews();
		try {
			verify(newsDAO, times(1)).readSorted();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test
	public void testViewSortedNews2() throws ServiceException {
		assertEquals(newsService.viewSortedNews(1), new ArrayList<News>());
	}
	
	@Test
	public void testViewSortedNews2Verification() throws ServiceException {
		newsService.viewSortedNews(1);
		try {
			verify(newsDAO, times(1)).readSorted(1);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Test
	public void testViewMessage() throws ServiceException {
		assertEquals(newsService.viewMessage(1), new News());
	}
	
	@Test
	public void testViewMessageVerification() throws ServiceException {
		newsService.viewMessage(1);
		try {
			verify(newsDAO, times(1)).read(1);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Test
	public void testCountNews() throws ServiceException {
		assertEquals(newsService.countNews(), 10);
	}
	
	@Test
	public void testCountNewsVerification() throws ServiceException {
		newsService.countNews();
		try {
			verify(newsDAO, times(1)).countNews();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Test
	public void testSearchNews() throws ServiceException {
		assertEquals(newsService.searchNews(new SearchCriteriaObject()),
				new ArrayList<News>());
	}
	
	@Test
	public void testSearchNewsVerification() throws ServiceException {
		newsService.searchNews(new SearchCriteriaObject());
		try {
			verify(newsDAO, times(1)).searchNews(new SearchCriteriaObject());
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test
	public void testSearchNews2() throws ServiceException {
		assertEquals(newsService.searchNews(new SearchCriteriaObject(), 1),
				new ArrayList<News>());
	}
	
	@Test
	public void testSearchNews2Verification() throws ServiceException {
		newsService.searchNews(new SearchCriteriaObject(), 3);
		try {
			verify(newsDAO, times(1)).searchNews(new SearchCriteriaObject(), 3);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test(expected = ServiceException.class)
	public void testSearchNewsException() throws ServiceException {
		newsService.searchNews(null);
	}
	
	@Test
	public void testAddAuthor() throws ServiceException {
		newsService.addAuthor(2, 2);
		try {
			verify(newsDAO, times(1)).addAuthor(2, 2);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Test
	public void testRemoveAuthor() throws ServiceException {
		newsService.removeAuthor(2, 2);
		try {
			verify(newsDAO, times(1)).removeAuthor(2, 2);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test
	public void testAddTag() throws ServiceException{
		newsService.addTag(1, 1);
		try {
			verify(newsDAO, times(1)).addTag(1, 1);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Test
	public void testRemoveTag() throws ServiceException{
		newsService.removeTag(1, 1);
		try {
			verify(newsDAO, times(1)).removeTag(1, 1);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test
	public void testRemoveTags() throws ServiceException{
		newsService.removeTags(1);
		try {
			verify(newsDAO, times(1)).removeTags(1);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test
	public void testViewNewsIndexes() throws ServiceException{
		assertEquals(newsService.viewNewsIndexes(1), new ArrayList<Long>());
	}
	
	@Test
	public void testViewNewsIndexesVerification() throws ServiceException{
		newsService.viewNewsIndexes(1);
		try {
			verify(newsDAO, times(1)).readIndexesSorted(1);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test
	public void testCountSearchResult() throws ServiceException{
		assertEquals(newsService.countSearchResult(new SearchCriteriaObject()), 1);
	}
	
	@Test
	public void testCountSearchResultVerification() throws ServiceException {
		newsService.countSearchResult(new SearchCriteriaObject());
		try {
			verify(newsDAO, times(1)).countSearchResult(new SearchCriteriaObject());
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test(expected = ServiceException.class)
	public void testCountSearchResultException() throws ServiceException{
		newsService.countSearchResult(null);
	}
	
	@Test
	public void testViewSearchIndexes() throws ServiceException{
		assertEquals(newsService.viewSearchIndexes(new SearchCriteriaObject(),
				1), new ArrayList<Long>());
	}
	
	@Test
	public void testViewSearchIndexesVerification() throws ServiceException{
		newsService.viewSearchIndexes(new SearchCriteriaObject(), 1);
		try {
			verify(newsDAO, times(1)).readSearchIndexes(new SearchCriteriaObject(), 1);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test(expected = ServiceException.class)
	public void testViewSearchIndexesException() throws ServiceException{
		newsService.viewSearchIndexes(null, 1);
	}
	
	@Test
	public void testFindPageIndex() throws ServiceException{
		assertEquals(newsService.findPageIndex(5), 1);
	}
	
	@Test
	public void testFindPageIndexVerification() throws ServiceException{
		newsService.findPageIndex(5);
		try {
			verify(newsDAO, times(1)).findPageIndex(5);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Test
	public void testFindSearchPageIndex() throws ServiceException{
		assertEquals(newsService.findSearchPageIndex(new SearchCriteriaObject(), 5), 1);
	}
	
	@Test
	public void testFindSearchPageIndexVerification() throws ServiceException{
		newsService.findSearchPageIndex(new SearchCriteriaObject(), 5);
		try {
			verify(newsDAO, times(1)).findSearchPageIndex(new SearchCriteriaObject(), 5);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
