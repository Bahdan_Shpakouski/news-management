package com.epam.newsmanagement.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.entity.Role;
import com.epam.newsmanagement.entity.dataobject.UserTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.FacadeUserService;
import com.epam.newsmanagement.service.impl.RoleService;
import com.epam.newsmanagement.service.impl.UserService;

@RunWith(MockitoJUnitRunner.class)
public class FacadeUserServiceTest {

	@Mock
	private UserService userService;
	@Mock
	private RoleService roleService;
	@InjectMocks
	private FacadeUserService userFacadeService;
	
	@Test
	public void testCreateUser() throws ServiceException{
		UserTO uto = new UserTO();
		uto.setUser(new User());
		ArrayList<Role> roles = new ArrayList<Role>();
		roles.add(new Role());
		roles.add(new Role());
		uto.setRoles(roles);
		userFacadeService.createUser(uto);
		verify(userService, times(1)).createUser(new User());
		verify(roleService, times(2)).addRole(new Role());
	}
	
	@Test(expected = ServiceException.class)
	public void testCreateUserException() throws ServiceException{
		FacadeUserServiceInterface fus = new FacadeUserService();
		fus.createUser(null);
	}

	@Test
	public void testDeleteUser() throws ServiceException {
		userFacadeService.deleteUser(1);
		verify(userService, times(1)).deleteUser(1);
		verify(roleService, times(1)).deleteUserRoles(1);
	}

	@Test
	public void testViewUser() throws ServiceException {
		when(userService.viewUser(anyLong())).thenReturn(new User());
		ArrayList<Role> list = new ArrayList<Role>();
		list.add(new Role());
		list.add(new Role());
		when(roleService.viewUserRoles(anyLong())).thenReturn(list);
		UserTO userTO = userFacadeService.viewUser(1);
		assertEquals(userTO.getUser(), new User());
		assertEquals(userTO.getRoles().size(), 2);
	}
	
	@Test
	public void testViewUserVerification() throws ServiceException {
		when(userService.viewUser(anyLong())).thenReturn(new User());
		ArrayList<Role> list = new ArrayList<Role>();
		list.add(new Role());
		list.add(new Role());
		when(roleService.viewUserRoles(anyLong())).thenReturn(list);
		userFacadeService.viewUser(1);
		verify(userService, times(1)).viewUser(1);
		verify(roleService, times(1)).viewUserRoles(1);
	}
	
	@Test
	public void testViewUsers() throws ServiceException {
		ArrayList<User> list = new ArrayList<User>();
		list.add(new User());
		list.add(new User());
		when(userService.viewUsers()).thenReturn(list);
		ArrayList<Role> list2 = new ArrayList<Role>();
		when(roleService.viewUserRoles(anyLong())).thenReturn(list2);
		assertEquals(userFacadeService.viewUsers().size(), 2);
	}
	
	@Test
	public void testViewUsersVerification() throws ServiceException {
		ArrayList<User> list = new ArrayList<User>();
		list.add(new User());
		list.add(new User());
		when(userService.viewUsers()).thenReturn(list);
		ArrayList<Role> list2 = new ArrayList<Role>();
		when(roleService.viewUserRoles(anyLong())).thenReturn(list2);
		userFacadeService.viewUsers();
		verify(userService, times(1)).viewUsers();
		verify(roleService, times(2)).viewUserRoles(anyLong());
	}

}
