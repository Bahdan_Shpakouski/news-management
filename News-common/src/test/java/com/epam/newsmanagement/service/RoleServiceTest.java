package com.epam.newsmanagement.service;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.jdbc.RoleDAOImpl;
import com.epam.newsmanagement.entity.Role;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.RoleService;

@RunWith(MockitoJUnitRunner.class)
public class RoleServiceTest {

	@Mock
	private RoleDAOImpl roleDAO;
	
	@InjectMocks
	private RoleService roleService;
	
	@Before
	public void setupMocks() throws ServiceException {
		try {
			when(roleDAO.create(any(Role.class))).thenReturn(5l);
			when(roleDAO.readUserRoles(anyLong()))
				.thenReturn(new ArrayList<Role>());
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}
	
	@Test
	public void testAddRole() throws ServiceException {
		assertEquals(roleService.addRole(new Role()), (Long)5l);
	}
	
	@Test
	public void testAddRoleVerification() throws ServiceException {
		roleService.addRole(new Role());
		try {
			verify(roleDAO, times(1)).create(new Role());
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}

	@Test(expected = ServiceException.class)
	public void testAddRoleException() throws ServiceException{
		roleService.addRole(null);
	}
	
	@Test
	public void testRemoveRole() throws ServiceException {
		roleService.removeRole(100);
		try {
			verify(roleDAO, times(1)).delete(100);
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}

	@Test
	public void testUpdateRole() throws ServiceException {
		roleService.updateRole(new Role(), 100);
		try {
			verify(roleDAO, times(1)).update(new Role(), 100);
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}
	
	@Test(expected = ServiceException.class)
	public void testUpdateRoleException() throws ServiceException {
		roleService.updateRole(null, 15);
	}

	@Test
	public void testViewUserRoles() throws ServiceException {
		assertEquals(roleService.viewUserRoles(1), new ArrayList<Role>());
	}
	
	@Test
	public void testViewUserRolesVerification() throws ServiceException {
		roleService.viewUserRoles(100);
		try {
			verify(roleDAO, times(1)).readUserRoles(100);
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}

	@Test
	public void testDeleteUserRoles() throws ServiceException {
		roleService.deleteUserRoles(123);
		try {
			verify(roleDAO, times(1)).deleteUserRoles(123);
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}

}
