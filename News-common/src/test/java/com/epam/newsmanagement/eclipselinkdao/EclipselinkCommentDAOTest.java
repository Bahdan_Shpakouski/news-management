package com.epam.newsmanagement.eclipselinkdao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-config/Test-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:dataset/CommentDataset.xml"},
	type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = { "classpath:dataset/CommentDataset.xml" },
	type = DatabaseOperation.DELETE_ALL)
public class EclipselinkCommentDAOTest {

	@Autowired
	private CommentDAO eclipselinkCommentDAO;
	@Autowired
	private JpaTransactionManager eclipselinkTransactionManager;
	
	public EclipselinkCommentDAOTest() {}

	@Test
	public void testCreate() throws DAOException{
		Comment comment = new Comment();
		comment.setCommentText("Testing");
		comment.setNewsId(3l);
		Long id = null;
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = eclipselinkTransactionManager.getTransaction(def);
		try {
			id = eclipselinkCommentDAO.create(comment);
		}
		catch (DAOException ex) {
			eclipselinkTransactionManager.rollback(status);
		  throw ex;
		}
		eclipselinkTransactionManager.commit(status);
		if(id == null) fail("no id");
		comment.setCommentId(id);
		comment.setCreationDate(eclipselinkCommentDAO.read(id).getCreationDate());
		assertEquals(comment, eclipselinkCommentDAO.read(id));
	}
	
	@Test
	public void testReadAll() throws DAOException{
		ArrayList<Comment> comments = (ArrayList<Comment>) eclipselinkCommentDAO
				.readAll();
		if(comments == null) fail("null pointer");
		assertTrue(comments.size()== 4);
	}
	
	@Test
	public void testRead() throws DAOException {
		Comment comment = eclipselinkCommentDAO.read(1);
		if(comment == null) fail("null pointer");
		assertEquals(comment.getCommentText(), "Cool");
		assertEquals(comment.getCommentId(), (Long)1l);
		assertEquals(comment.getCreationDate().toString(), "2016-02-22 18:45:50.0");
		assertEquals(comment.getNewsId(), (Long)1l);
	}
	
	@Test
	public void testReadNewsComments() throws DAOException {
		ArrayList<Comment> comments = (ArrayList<Comment>) eclipselinkCommentDAO
				.readNewsComments(2);
		if(comments == null) fail("null pointer");
		assertTrue(comments.size()==2);
	}
	
	@Test
	public void testUpdate() throws DAOException{
		Comment comment = new Comment();
		comment.setCommentText("Updated comment");
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = eclipselinkTransactionManager.getTransaction(def);
		try {
			eclipselinkCommentDAO.update(comment, 3);
		}
		catch (DAOException ex) {
			eclipselinkTransactionManager.rollback(status);
		  throw ex;
		}
		eclipselinkTransactionManager.commit(status);
		assertEquals(eclipselinkCommentDAO.read(3).getCommentText(), "Updated comment");
	}
	
	@Test
	public void testDelete() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = eclipselinkTransactionManager.getTransaction(def);
		try {
			eclipselinkCommentDAO.delete(4);
		}
		catch (DAOException ex) {
			eclipselinkTransactionManager.rollback(status);
		  throw ex;
		}
		eclipselinkTransactionManager.commit(status);
		assertEquals(eclipselinkCommentDAO.read(4), null);
	}
	
	@Test
	public void testDeleteNewsComments() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = eclipselinkTransactionManager.getTransaction(def);
		try {
			eclipselinkCommentDAO.deleteNewsComments(2);
		}
		catch (DAOException ex) {
			eclipselinkTransactionManager.rollback(status);
		  throw ex;
		}
		eclipselinkTransactionManager.commit(status);
		assertEquals(eclipselinkCommentDAO.readAll().size(), 2);
	}
}
