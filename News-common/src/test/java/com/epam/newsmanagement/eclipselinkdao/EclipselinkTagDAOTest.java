package com.epam.newsmanagement.eclipselinkdao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-config/Test-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:dataset/TagDataset.xml"},
	type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = { "classpath:dataset/TagDataset.xml" },
	type = DatabaseOperation.DELETE_ALL)
public class EclipselinkTagDAOTest {

	@Autowired
	private TagDAO eclipselinkTagDAO;
	@Autowired
	private JpaTransactionManager eclipselinkTransactionManager;
	
	public EclipselinkTagDAOTest() {}

	@Test
	public void testCreate() throws DAOException{
		Tag tag = new Tag();
		tag.setTagName("Entertainment");
		Long id = null;
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = eclipselinkTransactionManager.getTransaction(def);
		try {
			id = eclipselinkTagDAO.create(tag);
		}
		catch (DAOException ex) {
			eclipselinkTransactionManager.rollback(status);
		  throw ex;
		}
		eclipselinkTransactionManager.commit(status);
		if(id == null) fail("no id");
		tag.setTagId(id);
		assertEquals(tag, eclipselinkTagDAO.read(id));
	}
	
	@Test
	public void testReadAll() throws DAOException{
		ArrayList<Tag> tags = (ArrayList<Tag>) eclipselinkTagDAO.readAll();
		if(tags == null) fail("null pointer");
		assertTrue(tags.size()== 4);
	}
	
	@Test
	public void testRead() throws DAOException {
		Tag tag = eclipselinkTagDAO.read(4);
		if(tag == null) fail("null pointer");
		assertEquals(tag.getTagName(), "Afrika");
		assertEquals(tag.getTagId(), (Long)4l);
	}
	
	@Test
	public void testUpdate() throws DAOException{
		Tag tag = new Tag();
		tag.setTagId(2l);
		tag.setTagName("School");		
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = eclipselinkTransactionManager.getTransaction(def);
		try {
			eclipselinkTagDAO.update(tag, 2);
		}
		catch (DAOException ex) {
			eclipselinkTransactionManager.rollback(status);
		  throw ex;
		}
		eclipselinkTransactionManager.commit(status);
		assertEquals(tag, eclipselinkTagDAO.read(2));
	}
	
	@Test
	public void testDelete() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = eclipselinkTransactionManager.getTransaction(def);
		try {
			eclipselinkTagDAO.delete(2);
		}
		catch (DAOException ex) {
			eclipselinkTransactionManager.rollback(status);
		  throw ex;
		}
		eclipselinkTransactionManager.commit(status);
		assertEquals(eclipselinkTagDAO.read(2), null);
	}
	
	@Test
	public void testReadNewsTags() throws DAOException{
		ArrayList<Tag> tags = (ArrayList<Tag>) eclipselinkTagDAO.readNewsTags(3);
		if(tags == null) fail("null pointer");
		assertTrue(tags.size()== 3);
	}
	
	@Test
	public void testRemoveTagFromAllNews() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = eclipselinkTransactionManager.getTransaction(def);
		try {
			eclipselinkTagDAO.removeTagFromAllNews(1);
		}
		catch (DAOException ex) {
			eclipselinkTransactionManager.rollback(status);
		  throw ex;
		}
		eclipselinkTransactionManager.commit(status);
		assertEquals(eclipselinkTagDAO.readNewsTags(3).size(), 2);
		assertEquals(eclipselinkTagDAO.readNewsTags(2).size(), 1);
	}
}
