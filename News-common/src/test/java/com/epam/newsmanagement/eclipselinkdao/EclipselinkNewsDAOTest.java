package com.epam.newsmanagement.eclipselinkdao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.text.ParseException;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.entity.dataobject.SearchCriteriaObject;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-config/Test-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:dataset/NewsDataset.xml"},
	type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = { "classpath:dataset/NewsDataset.xml" },
	type = DatabaseOperation.DELETE_ALL)
public class EclipselinkNewsDAOTest {

	public EclipselinkNewsDAOTest() {}

	@Autowired
	private NewsDAO eclipselinkNewsDAO;
	@Autowired
	private AuthorDAO eclipselinkAuthorDAO;
	@Autowired
	private TagDAO eclipselinkTagDAO;
	@Autowired
	private JpaTransactionManager eclipselinkTransactionManager;
	
	@Test
	public void testCreate() throws DAOException{
		News news = new News();
		news.setTitle("Title");
		news.setShortText("Short text");
		news.setFullText("Full text");
		Long id = null;
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = eclipselinkTransactionManager.getTransaction(def);
		try {
			id = eclipselinkNewsDAO.create(news);
		}
		catch (DAOException ex) {
			eclipselinkTransactionManager.rollback(status);
		  throw ex;
		}
		eclipselinkTransactionManager.commit(status);
		if(id == null) fail("no id");
		news.setNewsId(id);
		assertEquals(news.getFullText(), eclipselinkNewsDAO.read(id).getFullText());
		assertEquals(news.getShortText(), eclipselinkNewsDAO.read(id).getShortText());
		assertEquals(news.getTitle(), eclipselinkNewsDAO.read(id).getTitle());
		assertEquals(news.getNewsId(), eclipselinkNewsDAO.read(id).getNewsId());
	}
	
	@Test
	public void testReadAll() throws DAOException{
		ArrayList<News> news = (ArrayList<News>) eclipselinkNewsDAO.readAll();
		if(news == null) fail("null pointer");
		assertTrue(news.size()== 6);
	}
	
	@Test
	public void testRead() throws DAOException, ParseException {
		News news = eclipselinkNewsDAO.read(1);
		if(news == null) fail("null pointer");
		assertEquals(news.getTitle(), "Fairytale");
		assertEquals(news.getShortText(), "Once up on a time...");
		assertEquals(news.getFullText(),
				"Once up on a time when dragons lived on Earth...");
		assertEquals(news.getNewsId(), (Long)1l);
		assertEquals(news.getCreationDate().toString(),
				"2016-02-14 12:10:19.056");
		java.text.DateFormat df = java.text.DateFormat.getDateInstance();
		String date = df.format(news.getModificationDate());
		assertEquals(date, "Feb 14, 2016");
	}
	
	@Test
	public void testUpdate() throws DAOException{
		News news = eclipselinkNewsDAO.read(2);
		news.setTitle("Alphabet");
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = eclipselinkTransactionManager.getTransaction(def);
		try {
			eclipselinkNewsDAO.update(news, 2);
		}
		catch (DAOException ex) {
			eclipselinkTransactionManager.rollback(status);
		  throw ex;
		}
		eclipselinkTransactionManager.commit(status);
		assertEquals(news.getTitle(), eclipselinkNewsDAO.read(2).getTitle());
	}
	
	@Test
	public void testDelete() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = eclipselinkTransactionManager.getTransaction(def);
		try {
			eclipselinkNewsDAO.delete(4);
		}
		catch (DAOException ex) {
			eclipselinkTransactionManager.rollback(status);
		  throw ex;
		}
		eclipselinkTransactionManager.commit(status);
		assertEquals(eclipselinkNewsDAO.read(4), null);
	}

	@Test
	public void testAddAuthor() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = eclipselinkTransactionManager.getTransaction(def);
		try {
			eclipselinkNewsDAO.addAuthor(4, 4);
		}
		catch (DAOException ex) {
			eclipselinkTransactionManager.rollback(status);
		  throw ex;
		}
		eclipselinkTransactionManager.commit(status);
		assertEquals(eclipselinkAuthorDAO.readNewsAuthor(4), eclipselinkAuthorDAO.read(4));
	}
	
	@Test
	public void testRemoveAuthor() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = eclipselinkTransactionManager.getTransaction(def);
		try {
			eclipselinkNewsDAO.removeAuthor(3, 2);
		}
		catch (DAOException ex) {
			eclipselinkTransactionManager.rollback(status);
		  throw ex;
		}
		eclipselinkTransactionManager.commit(status);
		assertEquals(eclipselinkAuthorDAO.readNewsAuthor(3), null);
	}
	
	@Test
	public void testAddTag() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = eclipselinkTransactionManager.getTransaction(def);
		try {
			eclipselinkNewsDAO.addTag(4, 2);
		}
		catch (DAOException ex) {
			eclipselinkTransactionManager.rollback(status);
		  throw ex;
		}
		eclipselinkTransactionManager.commit(status);
		ArrayList<Tag> tags = new ArrayList<Tag> ();
		tags.add(eclipselinkTagDAO.read(2));
		assertEquals(eclipselinkTagDAO.readNewsTags(4), tags);
	}
	
	@Test
	public void testRemoveTag() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = eclipselinkTransactionManager.getTransaction(def);
		try {
			eclipselinkNewsDAO.removeTag(1, 3);
		}
		catch (DAOException ex) {
			eclipselinkTransactionManager.rollback(status);
		  throw ex;
		}
		eclipselinkTransactionManager.commit(status);
		assertEquals(eclipselinkTagDAO.readNewsTags(1), new ArrayList<Tag>());
	}
	
	@Test
	public void testCountNews() throws DAOException{
		assertEquals(eclipselinkNewsDAO.countNews(), 6);
	}
	
	@Test
	public void testrReadSorted() throws DAOException{
		ArrayList<News> news = (ArrayList<News>) eclipselinkNewsDAO.readSorted();
		if(news == null) fail("null pointer");
		assertTrue(news.size()== 6);
		assertEquals(news.get(0).getNewsId().longValue(), 5);
	}
	
	@Test
	public void testrReadSorted2() throws DAOException{
		ArrayList<News> news = (ArrayList<News>) eclipselinkNewsDAO.readSorted(1);
		if(news == null) fail("null pointer");
		assertTrue(news.size()== 5);
		assertEquals(news.get(0).getNewsId().longValue(), 5);
	}
	
	@Test
	public void testRemoveAuthors() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = eclipselinkTransactionManager.getTransaction(def);
		try {
			eclipselinkNewsDAO.removeAuthors(1);
		}
		catch (DAOException ex) {
			eclipselinkTransactionManager.rollback(status);
		  throw ex;
		}
		eclipselinkTransactionManager.commit(status);
		assertEquals(eclipselinkAuthorDAO.readNewsAuthor(1), null);
	}
	
	@Test
	public void testRemoveTags() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = eclipselinkTransactionManager.getTransaction(def);
		try {
			eclipselinkNewsDAO.removeTags(2);
		}
		catch (DAOException ex) {
			eclipselinkTransactionManager.rollback(status);
		  throw ex;
		}
		eclipselinkTransactionManager.commit(status);
		assertEquals(eclipselinkTagDAO.readNewsTags(2).size(), 0);
	}
	
	@Test
	public void testSearchNews() throws DAOException{
		SearchCriteriaObject sco = new SearchCriteriaObject();
		ArrayList<Long> tags = new ArrayList<Long> ();
		tags.add(1l);
		tags.add(3l);
		sco.setTags(tags);
		sco.setAuthorId(2l);
		ArrayList<News> news = (ArrayList<News>) eclipselinkNewsDAO.searchNews(sco);
		assertEquals(news.size(), 2);
	}
	
	@Test
	public void testSearchNews2() throws DAOException{
		SearchCriteriaObject sco = new SearchCriteriaObject();
		ArrayList<Long> tags = new ArrayList<Long> ();
		tags.add(1l);
		tags.add(3l);
		sco.setTags(tags);
		sco.setAuthorId(2l);
		assertEquals(eclipselinkNewsDAO.searchNews(sco, 1).size(), 2);
		assertEquals(eclipselinkNewsDAO.searchNews(sco, 2).size(), 0);
	}
	
	@Test
	public void testReadIndexesSorted() throws DAOException{
		ArrayList<Long> indexes = (ArrayList<Long>) eclipselinkNewsDAO
				.readIndexesSorted(1);
		if(indexes == null) fail("null pointer");
		assertTrue(indexes.size()== 5);
		assertEquals(indexes.get(0).longValue(), 5);
	}
	
	@Test
	public void testCountSearchResult() throws DAOException{
		SearchCriteriaObject sco = new SearchCriteriaObject();
		ArrayList<Long> tags = new ArrayList<Long> ();
		tags.add(1l);
		tags.add(3l);
		sco.setTags(tags);
		sco.setAuthorId(2l);
		assertEquals(eclipselinkNewsDAO.countSearchResult(sco), 2);
	}
	
	@Test
	public void testReadSearchIndexes() throws DAOException{
		SearchCriteriaObject sco = new SearchCriteriaObject();
		ArrayList<Long> tags = new ArrayList<Long> ();
		tags.add(1l);
		tags.add(3l);
		sco.setTags(tags);
		sco.setAuthorId(2l);
		ArrayList<Long> indexes = (ArrayList<Long>) eclipselinkNewsDAO
				.readSearchIndexes(sco, 1);
		assertTrue(indexes.size()==2);
		assertEquals(indexes.get(0).longValue(), 3);
		assertEquals(indexes.get(1).longValue(), 2);
	}
	
	@Test
	public void testFindPageIndex() throws DAOException{
		assertTrue(eclipselinkNewsDAO.findPageIndex(1)==2);
		assertTrue(eclipselinkNewsDAO.findPageIndex(4)==1);
	}
	
	@Test
	public void testFindSearchPageIndex() throws DAOException{
		SearchCriteriaObject sco = new SearchCriteriaObject();
		ArrayList<Long> tags = new ArrayList<Long> ();
		tags.add(1l);
		tags.add(3l);
		sco.setTags(tags);
		sco.setAuthorId(2l);
		assertTrue(eclipselinkNewsDAO.findSearchPageIndex(sco, 3)==1);
	}
}
