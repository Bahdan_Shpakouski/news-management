package com.epam.newsmanagement.eclipselinkdao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-config/Test-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:dataset/AuthorDataset.xml"},
	type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = { "classpath:dataset/AuthorDataset.xml" },
	type = DatabaseOperation.DELETE_ALL)
public class EclipselinkAuthorDAOTest {

	public EclipselinkAuthorDAOTest() {	}
	
	@Autowired
	private AuthorDAO eclipselinkAuthorDAO;
	@Autowired
	private JpaTransactionManager eclipselinkTransactionManager;

	@Test
	public void testCreate() throws DAOException{
		Author author = new Author();
		author.setAuthorName("Bob");
		Long id = null;
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = eclipselinkTransactionManager.getTransaction(def);
		try {
			id =  eclipselinkAuthorDAO.create(author);
		}
		catch (DAOException ex) {
			eclipselinkTransactionManager.rollback(status);
		  throw ex;
		}
		eclipselinkTransactionManager.commit(status);
		if(id == null) fail("no id");
		author.setAuthorId(id);
		assertEquals(author, eclipselinkAuthorDAO.read(id));
	}
	
	@Test
	public void testReadAll() throws DAOException{
		ArrayList<Author> authors = (ArrayList<Author>) eclipselinkAuthorDAO.readAll();
		if(authors == null) fail("null pointer");
		assertTrue(authors.size()== 4);
	}
	
	@Test
	public void testReadNotExpired() throws DAOException{
		ArrayList<Author> authors = (ArrayList<Author>) eclipselinkAuthorDAO
				.readNotExpired();
		if(authors == null) fail("null pointer");
		assertTrue(authors.size()== 2);
	}
	
	@Test
	public void testRead() throws DAOException {
		Author author = eclipselinkAuthorDAO.read(1);
		if(author == null) fail("null pointer");
		assertEquals(author.getAuthorName(), "Allan");
		assertEquals(author.getAuthorId(), (Long)1l);
		assertEquals(author.getExpired().toString(), "2016-02-20 18:15:33.0");
	}
	
	@Test
	public void testUpdate() throws DAOException{
		Author author = new Author();
		author.setAuthorName("Johnny");
		author.setAuthorId(3l);
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = eclipselinkTransactionManager.getTransaction(def);
		try {
			eclipselinkAuthorDAO.update(author, 3);
		}
		catch (DAOException ex) {
			eclipselinkTransactionManager.rollback(status);
		  throw ex;
		}
		eclipselinkTransactionManager.commit(status);
		assertEquals(author, eclipselinkAuthorDAO.read(3));
	}
	
	@Test
	public void testDelete() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = eclipselinkTransactionManager.getTransaction(def);
		try {
			eclipselinkAuthorDAO.delete(4);
		}
		catch (DAOException ex) {
			eclipselinkTransactionManager.rollback(status);
		  throw ex;
		}
		eclipselinkTransactionManager.commit(status);
		assertEquals(eclipselinkAuthorDAO.read(4), null);
	}
	
	@Test
	public void testReadNewsAuthor() throws DAOException{
		Author author = eclipselinkAuthorDAO.readNewsAuthor(3);
		if(author == null) fail("null pointer");
		assertTrue(author.getAuthorId()== 2);
	}

}
