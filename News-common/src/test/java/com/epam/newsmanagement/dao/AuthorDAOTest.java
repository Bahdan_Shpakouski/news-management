package com.epam.newsmanagement.dao;


import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.DatabaseOperation;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-config/Test-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:dataset/AuthorDataset.xml"},
	type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = { "classpath:dataset/AuthorDataset.xml" },
	type = DatabaseOperation.DELETE_ALL)
public class AuthorDAOTest {
	
	@Autowired
	private AuthorDAO jdbcAuthorDAO;
	
	public AuthorDAOTest() {
		
	}
	
	@Test
	public void testCreate() throws DAOException{
		Author author = new Author();
		author.setAuthorName("Bob");
		Long id = jdbcAuthorDAO.create(author);
		if(id == null) fail("no id");
		author.setAuthorId(id);
		assertEquals(author, jdbcAuthorDAO.read(id));
	}
	
	@Test
	public void testReadAll() throws DAOException{
		ArrayList<Author> authors = (ArrayList<Author>) jdbcAuthorDAO.readAll();
		if(authors == null) fail("null pointer");
		assertTrue(authors.size()== 4);
	}
	
	@Test
	public void testReadNotExpired() throws DAOException{
		ArrayList<Author> authors = (ArrayList<Author>) jdbcAuthorDAO.readNotExpired();
		if(authors == null) fail("null pointer");
		assertTrue(authors.size()== 2);
	}
	
	@Test
	public void testRead() throws DAOException {
		Author author = jdbcAuthorDAO.read(1);
		if(author == null) fail("null pointer");
		assertEquals(author.getAuthorName(), "Allan");
		assertEquals(author.getAuthorId(), (Long)1l);
		assertEquals(author.getExpired().toString(), "2016-02-20 18:15:33.0");
	}
	
	@Test
	public void testUpdate() throws DAOException{
		Author author = new Author();
		author.setAuthorName("Johnny");
		author.setAuthorId(3l);
		jdbcAuthorDAO.update(author, 3);
		assertEquals(author, jdbcAuthorDAO.read(3));
	}
	
	@Test
	public void testDelete() throws DAOException{
		jdbcAuthorDAO.delete(4);
		assertEquals(jdbcAuthorDAO.read(4), null);
	}
	
	@Test
	public void testReadNewsAuthor() throws DAOException{
		Author author = jdbcAuthorDAO.readNewsAuthor(3);
		if(author == null) fail("null pointer");
		assertTrue(author.getAuthorId()== 2);
	}
}
