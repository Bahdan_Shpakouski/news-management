package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-config/Test-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:dataset/CommentDataset.xml"},
	type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = { "classpath:dataset/CommentDataset.xml" },
	type = DatabaseOperation.DELETE_ALL)
public class CommentDAOTest {
	
	@Autowired
	private CommentDAO jdbcCommentDAO;
	
	public CommentDAOTest() {
		
	}

	@Test
	public void testCreate() throws DAOException{
		Comment comment = new Comment();
		comment.setCommentText("Testing");
		comment.setNewsId(3l);
		Long id = jdbcCommentDAO.create(comment);
		if(id == null) fail("no id");
		comment.setCommentId(id);
		comment.setCreationDate(jdbcCommentDAO.read(id).getCreationDate());
		assertEquals(comment, jdbcCommentDAO.read(id));
	}
	
	@Test
	public void testReadAll() throws DAOException{
		ArrayList<Comment> comments = (ArrayList<Comment>) jdbcCommentDAO.readAll();
		if(comments == null) fail("null pointer");
		assertTrue(comments.size()== 4);
	}
	
	@Test
	public void testRead() throws DAOException {
		Comment comment = jdbcCommentDAO.read(1);
		if(comment == null) fail("null pointer");
		assertEquals(comment.getCommentText(), "Cool");
		assertEquals(comment.getCommentId(), (Long)1l);
		assertEquals(comment.getCreationDate().toString(), "2016-02-22 18:45:50.0");
		assertEquals(comment.getNewsId(), (Long)1l);
	}
	
	@Test
	public void testReadNewsComments() throws DAOException{
		ArrayList<Comment> comments = (ArrayList<Comment>) jdbcCommentDAO.readNewsComments(2);
		if(comments == null) fail("null pointer");
		assertTrue(comments.size()==2);
	}
	
	@Test
	public void testUpdate() throws DAOException{
		Comment comment = new Comment();
		comment.setCommentText("Updated comment");
		jdbcCommentDAO.update(comment, 3);
		assertEquals(jdbcCommentDAO.read(3).getCommentText(), "Updated comment");
	}
	
	@Test
	public void testDelete() throws DAOException{
		jdbcCommentDAO.delete(4);
		assertEquals(jdbcCommentDAO.read(4), null);
	}
	
	@Test
	public void testDeleteNewsComments() throws DAOException{
		jdbcCommentDAO.deleteNewsComments(2);
		assertEquals(jdbcCommentDAO.readAll().size(), 2);
	}
}
