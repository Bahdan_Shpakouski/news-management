package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-config/Test-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:dataset/UserDataset.xml"},
	type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = { "classpath:dataset/UserDataset.xml" },
	type = DatabaseOperation.DELETE_ALL)
public class UserDAOTest {
	
	@Autowired
	private UserDAO jdbcUserDAO;
	
	public UserDAOTest() {
		
	}

	@Test
	public void testCreate() throws DAOException{
		User user = new User();
		user.setUserName("Luda");
		user.setLogin("Cactus");
		user.setPassword("Pass123");
		Long id = jdbcUserDAO.create(user);
		if(id == null) fail("no id");
		user.setUserId(id);
		assertEquals(user, jdbcUserDAO.read(id));
	}
	
	@Test
	public void testReadAll() throws DAOException{
		ArrayList<User> users = (ArrayList<User>) jdbcUserDAO.readAll();
		if(users == null) fail("null pointer");
		assertTrue(users.size()== 6);
	}
	
	@Test
	public void testRead() throws DAOException {
		User user = jdbcUserDAO.read(1);
		if(user == null) fail("null pointer");
		assertEquals(user.getUserName(), "Bogdan");
		assertEquals(user.getLogin(), "ADMIN");
		assertEquals(user.getUserId(), (Long)1l);
		assertEquals(user.getPassword(), "1234");
	}
	
	@Test
	public void testUpdate() throws DAOException{
		User user = new User();
		user.setUserId(4l);
		user.setUserName("Pier");
		user.setLogin("P12");
		user.setPassword("ctaslkj");
		jdbcUserDAO.update(user, 4);
		assertEquals(user, jdbcUserDAO.read(4));
	}
	
	@Test
	public void testDelete() throws DAOException{
		jdbcUserDAO.delete(6);
		assertEquals(jdbcUserDAO.read(6), null);
	}
	
	@Test
	public void testLogin() throws DAOException{
		User user = new User();
		user.setLogin("Raptor");
		user.setPassword("newpass");
		assertEquals(jdbcUserDAO.login(user), (Long)3l);
	}
	
}
