package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import com.epam.newsmanagement.dao.RoleDAO;
import com.epam.newsmanagement.entity.Role;
import com.epam.newsmanagement.exception.DAOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-config/Test-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:dataset/UserDataset.xml"},
	type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = { "classpath:dataset/UserDataset.xml" },
	type = DatabaseOperation.DELETE_ALL)
public class RoleDAOTest {
	
	@Autowired
	private RoleDAO jdbcRoleDAO;
	
	public RoleDAOTest() {
	}

	@Test
	public void testCreate() throws DAOException{
		Role role = new Role();
		role.setRoleName("User");
		role.setUserId(5l);
		Long id = jdbcRoleDAO.create(role);
		if(id == null) fail("no id");
		role.setRoleId(id);
		assertEquals(role, jdbcRoleDAO.read(id));
	}
	
	@Test
	public void testReadAll() throws DAOException{
		ArrayList<Role> roles = (ArrayList<Role>) jdbcRoleDAO.readAll();
		if(roles == null) fail("null pointer");
		assertTrue(roles.size()== 6);
	}
	
	@Test
	public void testRead() throws DAOException {
		Role role = jdbcRoleDAO.read(1);
		if(role == null) fail("null pointer");
		assertEquals(role.getRoleName(), "Admin");
		assertEquals(role.getRoleId(), (Long)1l);
		assertEquals(role.getUserId(), (Long)1l);
	}
	
	@Test
	public void testUpdate() throws DAOException{
		Role role = new Role();
		role.setUserId(4l);
		role.setRoleName("Tester");
		role.setRoleId(5l);
		jdbcRoleDAO.update(role, 5);
		assertEquals(role, jdbcRoleDAO.read(5));
	}
	
	@Test
	public void testDelete() throws DAOException{
		jdbcRoleDAO.delete(6);
		assertEquals(jdbcRoleDAO.read(6), null);
	}
	
	@Test
	public void testDeleteUserRoles() throws DAOException{
		jdbcRoleDAO.deleteUserRoles(4);
		ArrayList<Role> roles = (ArrayList<Role>) jdbcRoleDAO.readUserRoles(4);
		assertTrue(roles.size()== 0);
	}
	
	@Test
	public void testReadUserRoles() throws DAOException{
		ArrayList<Role> roles = (ArrayList<Role>) jdbcRoleDAO.readUserRoles(1);
		if(roles == null) fail("null pointer");
		assertTrue(roles.size()== 2);
	}
}
