package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.entity.dataobject.SearchCriteriaObject;
import com.epam.newsmanagement.exception.DAOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-config/Test-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:dataset/NewsDataset.xml"},
	type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = { "classpath:dataset/NewsDataset.xml" },
	type = DatabaseOperation.DELETE_ALL)
public class NewsDAOTest {

	@Autowired
	private NewsDAO jdbcNewsDAO;
	@Autowired
	private AuthorDAO jdbcAuthorDAO;
	@Autowired
	private TagDAO jdbcTagDAO;
	
	public NewsDAOTest() {
		
	}
	
	@Test
	public void testCreate() throws DAOException{
		News news = new News();
		news.setTitle("Title");
		news.setShortText("Short text");
		news.setFullText("Full text");
		Long id = jdbcNewsDAO.create(news);
		if(id == null) fail("no id");
		news.setNewsId(id);
		assertEquals(news.getFullText(), jdbcNewsDAO.read(id).getFullText());
		assertEquals(news.getShortText(), jdbcNewsDAO.read(id).getShortText());
		assertEquals(news.getTitle(), jdbcNewsDAO.read(id).getTitle());
		assertEquals(news.getNewsId(), jdbcNewsDAO.read(id).getNewsId());
	}
	
	@Test
	public void testReadAll() throws DAOException{
		ArrayList<News> news = (ArrayList<News>) jdbcNewsDAO.readAll();
		if(news == null) fail("null pointer");
		assertTrue(news.size()== 6);
	}
	
	@Test
	public void testRead() throws DAOException {
		News news = jdbcNewsDAO.read(1);
		if(news == null) fail("null pointer");
		assertEquals(news.getTitle(), "Fairytale");
		assertEquals(news.getShortText(), "Once up on a time...");
		assertEquals(news.getFullText(),
				"Once up on a time when dragons lived on Earth...");
		assertEquals(news.getNewsId(), (Long)1l);
		assertEquals(news.getCreationDate().toString(),
				"2016-02-14 12:10:19.056");
		assertEquals(news.getModificationDate().toString(),
				"2016-02-14");
	}
	
	@Test
	public void testUpdate() throws DAOException{
		News news = jdbcNewsDAO.read(2);
		news.setTitle("Alphabet");
		jdbcNewsDAO.update(news, 2);
		assertEquals(news.getTitle(), jdbcNewsDAO.read(2).getTitle());
	}
	
	@Test
	public void testDelete() throws DAOException{
		jdbcNewsDAO.delete(4);
		assertEquals(jdbcNewsDAO.read(4), null);
	}

	@Test
	public void testAddAuthor() throws DAOException{
		jdbcNewsDAO.addAuthor(4, 4);
		assertEquals(jdbcAuthorDAO.readNewsAuthor(4), jdbcAuthorDAO.read(4));
	}
	
	@Test
	public void testRemoveAuthor() throws DAOException{
		jdbcNewsDAO.removeAuthor(3, 2);
		assertEquals(jdbcAuthorDAO.readNewsAuthor(3), null);
	}
	
	@Test
	public void testAddTag() throws DAOException{
		jdbcNewsDAO.addTag(4, 2);
		ArrayList<Tag> tags = new ArrayList<Tag> ();
		tags.add(jdbcTagDAO.read(2));
		assertEquals(jdbcTagDAO.readNewsTags(4), tags);
	}
	
	@Test
	public void testRemoveTag() throws DAOException{
		jdbcNewsDAO.removeTag(1, 3);
		assertEquals(jdbcTagDAO.readNewsTags(1), new ArrayList<Tag>());
	}
	
	@Test
	public void testCountNews() throws DAOException{
		assertEquals(jdbcNewsDAO.countNews(), 6);
	}
	
	@Test
	public void testrReadSorted() throws DAOException{
		ArrayList<News> news = (ArrayList<News>) jdbcNewsDAO.readSorted();
		if(news == null) fail("null pointer");
		assertTrue(news.size()== 6);
		assertEquals(news.get(0).getNewsId().longValue(), 5);
	}
	
	@Test
	public void testrReadSorted2() throws DAOException{
		ArrayList<News> news = (ArrayList<News>) jdbcNewsDAO.readSorted(1);
		if(news == null) fail("null pointer");
		assertTrue(news.size()== 5);
		assertEquals(news.get(0).getNewsId().longValue(), 5);
	}
	
	@Test
	public void testRemoveAuthors() throws DAOException{
		jdbcNewsDAO.removeAuthors(1);
		assertEquals(jdbcAuthorDAO.readNewsAuthor(1), null);
	}
	
	@Test
	public void testRemoveTags() throws DAOException{
		jdbcNewsDAO.removeTags(1);
		assertEquals(jdbcTagDAO.readNewsTags(1).size(), 0);
	}
	
	@Test
	public void testSearchNews() throws DAOException{
		SearchCriteriaObject sco = new SearchCriteriaObject();
		ArrayList<Long> tags = new ArrayList<Long> ();
		tags.add(1l);
		tags.add(3l);
		sco.setTags(tags);
		sco.setAuthorId(2l);
		assertEquals(jdbcNewsDAO.searchNews(sco).size(), 2);
	}
	
	@Test
	public void testSearchNews2() throws DAOException{
		SearchCriteriaObject sco = new SearchCriteriaObject();
		ArrayList<Long> tags = new ArrayList<Long> ();
		tags.add(1l);
		tags.add(3l);
		sco.setTags(tags);
		sco.setAuthorId(2l);
		assertEquals(jdbcNewsDAO.searchNews(sco, 1).size(), 2);
		assertEquals(jdbcNewsDAO.searchNews(sco, 2).size(), 0);
	}
	
	@Test
	public void testReadIndexesSorted() throws DAOException{
		ArrayList<Long> indexes = (ArrayList<Long>) jdbcNewsDAO.readIndexesSorted(1);
		if(indexes == null) fail("null pointer");
		assertTrue(indexes.size()== 5);
		assertEquals(indexes.get(0).longValue(), 5);
	}
	
	@Test
	public void testCountSearchResult() throws DAOException{
		SearchCriteriaObject sco = new SearchCriteriaObject();
		ArrayList<Long> tags = new ArrayList<Long> ();
		tags.add(1l);
		tags.add(3l);
		sco.setTags(tags);
		sco.setAuthorId(2l);
		assertEquals(jdbcNewsDAO.countSearchResult(sco), 2);
	}
	
	@Test
	public void testReadSearchIndexes() throws DAOException{
		SearchCriteriaObject sco = new SearchCriteriaObject();
		ArrayList<Long> tags = new ArrayList<Long> ();
		tags.add(1l);
		tags.add(3l);
		sco.setTags(tags);
		sco.setAuthorId(2l);
		ArrayList<Long> indexes = (ArrayList<Long>) jdbcNewsDAO.readSearchIndexes(sco, 1);
		assertTrue(indexes.size()==2);
		assertEquals(indexes.get(0).longValue(), 3);
		assertEquals(indexes.get(1).longValue(), 2);
	}
	
	@Test
	public void testFindPageIndex() throws DAOException{
		assertTrue(jdbcNewsDAO.findPageIndex(1)==2);
		assertTrue(jdbcNewsDAO.findPageIndex(4)==1);
	}
	
	@Test
	public void testFindSearchPageIndex() throws DAOException{
		SearchCriteriaObject sco = new SearchCriteriaObject();
		ArrayList<Long> tags = new ArrayList<Long> ();
		tags.add(1l);
		tags.add(3l);
		sco.setTags(tags);
		sco.setAuthorId(2l);
		assertTrue(jdbcNewsDAO.findSearchPageIndex(sco, 3)==1);
	}
}
