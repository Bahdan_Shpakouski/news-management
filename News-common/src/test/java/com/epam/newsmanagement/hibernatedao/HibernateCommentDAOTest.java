package com.epam.newsmanagement.hibernatedao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-config/Test-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:dataset/CommentDataset.xml"},
	type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = { "classpath:dataset/CommentDataset.xml" },
	type = DatabaseOperation.DELETE_ALL)
public class HibernateCommentDAOTest {

	static Logger logger = Logger.getLogger(HibernateCommentDAOTest.class);
	
	@Autowired
	private CommentDAO hibernateCommentDAO;
	@Autowired
	private HibernateTransactionManager hibernateTransactionManager;
	
	public HibernateCommentDAOTest() {
		
	}

	@Test
	public void testCreate() throws DAOException{
		Comment comment = new Comment();
		comment.setCommentText("Testing");
		comment.setNewsId(3l);
		Long id = null;
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = hibernateTransactionManager.getTransaction(def);
		try {
			id = hibernateCommentDAO.create(comment);
		}
		catch (DAOException ex) {
			hibernateTransactionManager.rollback(status);
		  throw ex;
		}
		hibernateTransactionManager.commit(status);
		if(id == null) fail("no id");
		comment.setCommentId(id);
		comment.setCreationDate(hibernateCommentDAO.read(id).getCreationDate());
		assertEquals(comment, hibernateCommentDAO.read(id));
	}
	
	@Test
	public void testReadAll() throws DAOException{
		ArrayList<Comment> comments = (ArrayList<Comment>) hibernateCommentDAO.readAll();
		if(comments == null) fail("null pointer");
		assertTrue(comments.size()== 4);
	}
	
	@Test
	public void testRead() throws DAOException {
		Comment comment = hibernateCommentDAO.read(1);
		logger.info(comment);
		if(comment == null) fail("null pointer");
		assertEquals(comment.getCommentText(), "Cool");
		assertEquals(comment.getCommentId(), (Long)1l);
		assertEquals(comment.getCreationDate().toString(), "2016-02-22 18:45:50.0");
		assertEquals(comment.getNewsId(), (Long)1l);
	}
	
	@Test
	public void testReadNewsComments() throws DAOException {
		ArrayList<Comment> comments = (ArrayList<Comment>) hibernateCommentDAO
				.readNewsComments(2);
		if(comments == null) fail("null pointer");
		assertTrue(comments.size()==2);
	}
	
	@Test
	public void testUpdate() throws DAOException{
		Comment comment = new Comment();
		comment.setCommentText("Updated comment");
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = hibernateTransactionManager.getTransaction(def);
		try {
			hibernateCommentDAO.update(comment, 3);
		}
		catch (DAOException ex) {
			hibernateTransactionManager.rollback(status);
		  throw ex;
		}
		hibernateTransactionManager.commit(status);
		assertEquals(hibernateCommentDAO.read(3).getCommentText(), "Updated comment");
	}
	
	@Test
	public void testDelete() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = hibernateTransactionManager.getTransaction(def);
		try {
			hibernateCommentDAO.delete(4);
		}
		catch (DAOException ex) {
			hibernateTransactionManager.rollback(status);
		  throw ex;
		}
		hibernateTransactionManager.commit(status);
		assertEquals(hibernateCommentDAO.read(4), null);
	}
	
	@Test
	public void testDeleteNewsComments() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = hibernateTransactionManager.getTransaction(def);
		try {
			hibernateCommentDAO.deleteNewsComments(2);
		}
		catch (DAOException ex) {
			hibernateTransactionManager.rollback(status);
		  throw ex;
		}
		hibernateTransactionManager.commit(status);
		assertEquals(hibernateCommentDAO.readAll().size(), 2);
	}
}
