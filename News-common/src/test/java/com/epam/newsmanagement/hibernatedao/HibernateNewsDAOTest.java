package com.epam.newsmanagement.hibernatedao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.text.ParseException;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.entity.dataobject.SearchCriteriaObject;
import com.epam.newsmanagement.exception.DAOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-config/Test-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:dataset/NewsDataset.xml"},
	type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = { "classpath:dataset/NewsDataset.xml" },
	type = DatabaseOperation.DELETE_ALL)
public class HibernateNewsDAOTest {

	@Autowired
	private NewsDAO hibernateNewsDAO;
	@Autowired
	private AuthorDAO hibernateAuthorDAO;
	@Autowired
	private TagDAO hibernateTagDAO;
	@Autowired
	private HibernateTransactionManager hibernateTransactionManager;
	
	public HibernateNewsDAOTest() {
		
	}
	
	@Test
	public void testCreate() throws DAOException{
		News news = new News();
		news.setTitle("Title");
		news.setShortText("Short text");
		news.setFullText("Full text");
		Long id = null;
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = hibernateTransactionManager.getTransaction(def);
		try {
			id = hibernateNewsDAO.create(news);
		}
		catch (DAOException ex) {
			hibernateTransactionManager.rollback(status);
		  throw ex;
		}
		hibernateTransactionManager.commit(status);
		if(id == null) fail("no id");
		news.setNewsId(id);
		assertEquals(news.getFullText(), hibernateNewsDAO.read(id).getFullText());
		assertEquals(news.getShortText(), hibernateNewsDAO.read(id).getShortText());
		assertEquals(news.getTitle(), hibernateNewsDAO.read(id).getTitle());
		assertEquals(news.getNewsId(), hibernateNewsDAO.read(id).getNewsId());
	}
	
	@Test
	public void testReadAll() throws DAOException{
		ArrayList<News> news = (ArrayList<News>) hibernateNewsDAO.readAll();
		if(news == null) fail("null pointer");
		assertTrue(news.size()== 6);
	}
	
	@Test
	public void testRead() throws DAOException, ParseException {
		News news = hibernateNewsDAO.read(1);
		if(news == null) fail("null pointer");
		assertEquals(news.getTitle(), "Fairytale");
		assertEquals(news.getShortText(), "Once up on a time...");
		assertEquals(news.getFullText(),
				"Once up on a time when dragons lived on Earth...");
		assertEquals(news.getNewsId(), (Long)1l);
		assertEquals(news.getCreationDate().toString(),
				"2016-02-14 12:10:19.056");
		assertEquals(news.getModificationDate().toString(),
				"2016-02-14 00:00:00.0");
	}
	
	@Test
	public void testUpdate() throws DAOException{
		News news = hibernateNewsDAO.read(2);
		news.setTitle("Alphabet");
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = hibernateTransactionManager.getTransaction(def);
		try {
			hibernateNewsDAO.update(news, 2);
		}
		catch (DAOException ex) {
			hibernateTransactionManager.rollback(status);
		  throw ex;
		}
		hibernateTransactionManager.commit(status);
		assertEquals(news.getTitle(), hibernateNewsDAO.read(2).getTitle());
	}
	
	@Test
	public void testDelete() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = hibernateTransactionManager.getTransaction(def);
		try {
			hibernateNewsDAO.delete(4);
		}
		catch (DAOException ex) {
			hibernateTransactionManager.rollback(status);
		  throw ex;
		}
		hibernateTransactionManager.commit(status);
		assertEquals(hibernateNewsDAO.read(4), null);
	}

	@Test
	public void testAddAuthor() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = hibernateTransactionManager.getTransaction(def);
		try {
			hibernateNewsDAO.addAuthor(4, 4);
		}
		catch (DAOException ex) {
			hibernateTransactionManager.rollback(status);
		  throw ex;
		}
		hibernateTransactionManager.commit(status);
		assertEquals(hibernateAuthorDAO.readNewsAuthor(4), hibernateAuthorDAO.read(4));
	}
	
	@Test
	public void testRemoveAuthor() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = hibernateTransactionManager.getTransaction(def);
		try {
			hibernateNewsDAO.removeAuthor(3, 2);
		}
		catch (DAOException ex) {
			hibernateTransactionManager.rollback(status);
		  throw ex;
		}
		hibernateTransactionManager.commit(status);
		assertEquals(hibernateAuthorDAO.readNewsAuthor(3), null);
	}
	
	@Test
	public void testAddTag() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = hibernateTransactionManager.getTransaction(def);
		try {
			hibernateNewsDAO.addTag(4, 2);
		}
		catch (DAOException ex) {
			hibernateTransactionManager.rollback(status);
		  throw ex;
		}
		hibernateTransactionManager.commit(status);
		ArrayList<Tag> tags = new ArrayList<Tag> ();
		tags.add(hibernateTagDAO.read(2));
		assertEquals(hibernateTagDAO.readNewsTags(4), tags);
	}
	
	@Test
	public void testRemoveTag() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = hibernateTransactionManager.getTransaction(def);
		try {
			hibernateNewsDAO.removeTag(1, 3);
		}
		catch (DAOException ex) {
			hibernateTransactionManager.rollback(status);
		  throw ex;
		}
		hibernateTransactionManager.commit(status);
		assertEquals(hibernateTagDAO.readNewsTags(1), new ArrayList<Tag>());
	}
	
	@Test
	public void testCountNews() throws DAOException{
		assertEquals(hibernateNewsDAO.countNews(), 6);
	}
	
	@Test
	public void testrReadSorted() throws DAOException{
		ArrayList<News> news = (ArrayList<News>) hibernateNewsDAO.readSorted();
		if(news == null) fail("null pointer");
		assertTrue(news.size()== 6);
		assertEquals(news.get(0).getNewsId().longValue(), 5);
	}
	
	@Test
	public void testrReadSorted2() throws DAOException{
		ArrayList<News> news = (ArrayList<News>) hibernateNewsDAO.readSorted(1);
		if(news == null) fail("null pointer");
		assertTrue(news.size()== 5);
		assertEquals(news.get(0).getNewsId().longValue(), 5);
	}
	
	@Test
	public void testRemoveAuthors() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = hibernateTransactionManager.getTransaction(def);
		try {
			hibernateNewsDAO.removeAuthors(1);
		}
		catch (DAOException ex) {
			hibernateTransactionManager.rollback(status);
		  throw ex;
		}
		hibernateTransactionManager.commit(status);
		assertEquals(hibernateAuthorDAO.readNewsAuthor(1), null);
	}
	
	@Test
	public void testRemoveTags() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = hibernateTransactionManager.getTransaction(def);
		try {
			hibernateNewsDAO.removeTags(1);
		}
		catch (DAOException ex) {
			hibernateTransactionManager.rollback(status);
		  throw ex;
		}
		hibernateTransactionManager.commit(status);
		assertEquals(hibernateTagDAO.readNewsTags(1).size(), 0);
	}
	
	@Test
	public void testSearchNews() throws DAOException{
		SearchCriteriaObject sco = new SearchCriteriaObject();
		ArrayList<Long> tags = new ArrayList<Long> ();
		tags.add(1l);
		tags.add(3l);
		sco.setTags(tags);
		sco.setAuthorId(2l);
		ArrayList<News> news = (ArrayList<News>) hibernateNewsDAO.searchNews(sco);
		assertEquals(news.size(), 2);
	}
	
	@Test
	public void testSearchNews2() throws DAOException{
		SearchCriteriaObject sco = new SearchCriteriaObject();
		ArrayList<Long> tags = new ArrayList<Long> ();
		tags.add(1l);
		tags.add(3l);
		sco.setTags(tags);
		sco.setAuthorId(2l);
		assertEquals(hibernateNewsDAO.searchNews(sco, 1).size(), 2);
		assertEquals(hibernateNewsDAO.searchNews(sco, 2).size(), 0);
	}
	
	@Test
	public void testReadIndexesSorted() throws DAOException{
		ArrayList<Long> indexes = (ArrayList<Long>) hibernateNewsDAO
				.readIndexesSorted(1);
		if(indexes == null) fail("null pointer");
		assertTrue(indexes.size()== 5);
		assertEquals(indexes.get(0).longValue(), 5);
	}
	
	@Test
	public void testCountSearchResult() throws DAOException{
		SearchCriteriaObject sco = new SearchCriteriaObject();
		ArrayList<Long> tags = new ArrayList<Long> ();
		tags.add(1l);
		tags.add(3l);
		sco.setTags(tags);
		sco.setAuthorId(2l);
		assertEquals(hibernateNewsDAO.countSearchResult(sco), 2);
	}
	
	@Test
	public void testReadSearchIndexes() throws DAOException{
		SearchCriteriaObject sco = new SearchCriteriaObject();
		ArrayList<Long> tags = new ArrayList<Long> ();
		tags.add(1l);
		tags.add(3l);
		sco.setTags(tags);
		sco.setAuthorId(2l);
		ArrayList<Long> indexes = (ArrayList<Long>) hibernateNewsDAO
				.readSearchIndexes(sco, 1);
		assertTrue(indexes.size()==2);
		assertEquals(indexes.get(0).longValue(), 3);
		assertEquals(indexes.get(1).longValue(), 2);
	}
	
	@Test
	public void testFindPageIndex() throws DAOException{
		assertTrue(hibernateNewsDAO.findPageIndex(1)==2);
		assertTrue(hibernateNewsDAO.findPageIndex(4)==1);
	}
	
	@Test
	public void testFindSearchPageIndex() throws DAOException{
		SearchCriteriaObject sco = new SearchCriteriaObject();
		ArrayList<Long> tags = new ArrayList<Long> ();
		tags.add(1l);
		tags.add(3l);
		sco.setTags(tags);
		sco.setAuthorId(2l);
		assertTrue(hibernateNewsDAO.findSearchPageIndex(sco, 3)==1);
	}

}
