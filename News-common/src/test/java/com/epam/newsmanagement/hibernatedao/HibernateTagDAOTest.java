package com.epam.newsmanagement.hibernatedao;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-config/Test-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:dataset/TagDataset.xml"},
	type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = { "classpath:dataset/TagDataset.xml" },
	type = DatabaseOperation.DELETE_ALL)
public class HibernateTagDAOTest {

	@Autowired
	private TagDAO hibernateTagDAO;
	@Autowired
	private HibernateTransactionManager hibernateTransactionManager;
	
	public HibernateTagDAOTest() {
		
	}
	
	@Test
	public void testCreate() throws DAOException{
		Tag tag = new Tag();
		tag.setTagName("Entertainment");
		Long id = null;
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = hibernateTransactionManager.getTransaction(def);
		try {
			id = hibernateTagDAO.create(tag);
		}
		catch (DAOException ex) {
			hibernateTransactionManager.rollback(status);
		  throw ex;
		}
		hibernateTransactionManager.commit(status);
		if(id == null) fail("no id");
		tag.setTagId(id);
		assertEquals(tag, hibernateTagDAO.read(id));
	}
	
	@Test
	public void testReadAll() throws DAOException{
		ArrayList<Tag> tags = (ArrayList<Tag>) hibernateTagDAO.readAll();
		if(tags == null) fail("null pointer");
		assertTrue(tags.size()== 4);
	}
	
	@Test
	public void testRead() throws DAOException {
		Tag tag = hibernateTagDAO.read(4);
		if(tag == null) fail("null pointer");
		assertEquals(tag.getTagName(), "Afrika");
		assertEquals(tag.getTagId(), (Long)4l);
	}
	
	@Test
	public void testUpdate() throws DAOException{
		Tag tag = new Tag();
		tag.setTagId(2l);
		tag.setTagName("School");
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = hibernateTransactionManager.getTransaction(def);
		try {
			hibernateTagDAO.update(tag, 2);
		}
		catch (DAOException ex) {
			hibernateTransactionManager.rollback(status);
		  throw ex;
		}
		hibernateTransactionManager.commit(status);
		assertEquals(tag, hibernateTagDAO.read(2));
	}
	
	@Test
	public void testDelete() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = hibernateTransactionManager.getTransaction(def);
		try {
			hibernateTagDAO.delete(2);
		}
		catch (DAOException ex) {
			hibernateTransactionManager.rollback(status);
		  throw ex;
		}
		hibernateTransactionManager.commit(status);
		assertEquals(hibernateTagDAO.read(2), null);
	}
	
	@Test
	public void testReadNewsTags() throws DAOException{
		ArrayList<Tag> tags = (ArrayList<Tag>) hibernateTagDAO.readNewsTags(3);
		if(tags == null) fail("null pointer");
		assertTrue(tags.size()== 3);
	}
	
	@Test
	public void testRemoveTagFromAllNews() throws DAOException{
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = hibernateTransactionManager.getTransaction(def);
		try {
			hibernateTagDAO.removeTagFromAllNews(1);
		}
		catch (DAOException ex) {
			hibernateTransactionManager.rollback(status);
		  throw ex;
		}
		hibernateTransactionManager.commit(status);
		assertEquals(hibernateTagDAO.readNewsTags(3).size(), 2);
		assertEquals(hibernateTagDAO.readNewsTags(2).size(), 1);
	}

}
