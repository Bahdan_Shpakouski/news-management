package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="NEWS")
public class News implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -860528232561669686L;
	@Id
	@SequenceGenerator(name="NewsGen", sequenceName = "NEWS_SEQUENCE")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="NewsGen")
	@Column(name="NWS_NEWS_ID")
	private Long newsId;
	@Column(name="NWS_TITLE")
	private String title;
	@Column(name="NWS_SHORT_TEXT")
	private String shortText;
	@Column(name="NWS_FULL_TEXT")
	private String fullText;
	@Column(name="NWS_CREATION_DATE")
	private Timestamp creationDate;
	@Column(name="NWS_MODIFICATION_DATE")
	private Date modificationDate;
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name="NEWS_AUTHORS", 
	joinColumns=@JoinColumn(name="NA_NEWS_ID"),
    inverseJoinColumns=@JoinColumn(name="NA_AUTHOR_ID"))
	private Set<Author> authors;
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name="NEWS_TAGS", 
	joinColumns=@JoinColumn(name="NT_NEWS_ID"),
    inverseJoinColumns=@JoinColumn(name="NT_TAG_ID"))
	private Set<Tag> tags;
	
	public News() {
		newsId = 0l;
		authors = new HashSet<Author>();
		tags = new HashSet<Tag>();
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Set<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
	}

	public Set<Tag> getTags() {
		return tags;
	}

	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}
	
	@Override
	public String toString() {
		return "News [newsId=" + newsId + ", title=" + title + ", shortText=" + shortText + ", fullText=" + fullText
				+ ", creationDate=" + creationDate + ", modificationDate=" + modificationDate + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		result = prime * result + ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

}
