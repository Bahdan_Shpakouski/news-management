package com.epam.newsmanagement.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;

import com.epam.newsmanagement.dao.*;
import com.epam.newsmanagement.dao.eclipselink.*;
import com.epam.newsmanagement.dao.hibernate.*;
import com.epam.newsmanagement.dao.jdbc.*;
import com.epam.newsmanagement.service.*;
import com.epam.newsmanagement.service.impl.*;

@Configuration
@EnableTransactionManagement
public class BeanConfig implements TransactionManagementConfigurer{
	
	@Autowired
	private DataSource dataSource;
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private EntityManagerFactory emf;
	
	static Logger logger = Logger.getLogger(BeanConfig.class);
	private String daoType;
	private static final String HIBERNATE = "Hibernate";
	private static final String ECLIPSELINK = "Eclipselink";
	private static final String JDBC = "JDBC";
	
	public BeanConfig() {
		Locale.setDefault(Locale.ENGLISH);
		Properties daoProperties = new Properties();
		InputStream input = null;
		String propFileName = "config.properties";
		try {
			input = getClass().getClassLoader()
					.getResourceAsStream(propFileName);
			if (input != null) {
				daoProperties.load(input);
				logger.info("selected dao: " + daoProperties
						.getProperty("selectedDao"));
				daoType=daoProperties.getProperty("selectedDao");
				if(daoType==null){
					logger.error("property not found");
					daoType = JDBC;
				}
			} else {
				logger.error("property file '" + propFileName 
						+ "' not found in the classpath");
			}
			input.close();
		} catch(IOException e){
			logger.error("properties loading error");
		}
	}
	
	@Bean
	public AuthorServiceInterface authorService(){
		return new AuthorService();
	}
	
	@Bean
	public CommentServiceInterface commentService(){
		return new CommentService();
	}
	
	@Bean
	public NewsServiceInterface newsService(){
		return new NewsService();
	}
	
	@Bean
	public TagServiceInterface tagService(){
		return new TagService();
	}
	
	@Bean
	public UserServiceInterface userService(){
		return new UserService();
	}
	
	@Bean 
	public RoleServiceInterface roleService(){
		return new RoleService();
	}
	
	@Bean
	public FacadeNewsServiceInterface facadeNewsServiceInterface(){
		return new FacadeNewsService();
	}
	
	@Bean
	public FacadeUserServiceInterface facadeUserServiceInterface(){
		return new FacadeUserService();
	}
	
	@Bean
	public AuthorDAO authorDAO(){
		if(HIBERNATE.equals(daoType)){
			return hibernateAuthorDAO();
		}else if(ECLIPSELINK.equals(daoType)){
			return eclipselinkAuthorDAO();
		}else{
			return jdbcAuthorDAO();
		}
	}
	
	@Bean
	public TagDAO tagDAO(){
		if(HIBERNATE.equals(daoType)){
			return hibernateTagDAO();
		}else if(ECLIPSELINK.equals(daoType)){
			return eclipselinkTagDAO();
		}else{
			return jdbcTagDAO();
		}
	}
	
	@Bean
	public NewsDAO newsDAO(){
		if(HIBERNATE.equals(daoType)){
			return hibernateNewsDAO();
		}else if(ECLIPSELINK.equals(daoType)){
			return eclipselinkNewsDAO();
		}else{
			return jdbcNewsDAO();
		}
	}
	
	@Bean
	public CommentDAO commentDAO(){
		if(HIBERNATE.equals(daoType)){
			return hibernateCommentDAO();
		}else if(ECLIPSELINK.equals(daoType)){
			return eclipselinkCommentDAO();
		}else{
			return jdbcCommentDAO();
		}
	}
	
	@Bean
    public PlatformTransactionManager txManager() {
		return new DataSourceTransactionManager(dataSource);
    }
	
	@Bean 
    public PlatformTransactionManager hibernateTransactionManager() {
			return new HibernateTransactionManager(sessionFactory);
    }
	
	@Bean 
    public PlatformTransactionManager eclipselinkTransactionManager() {
	    JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
	    jpaTransactionManager.setEntityManagerFactory(emf);
	    return jpaTransactionManager;
    }

	@Override
	public PlatformTransactionManager annotationDrivenTransactionManager() {
		if(HIBERNATE.equals(daoType)){
			return hibernateTransactionManager();
		}else if(ECLIPSELINK.equals(daoType)){
			return eclipselinkTransactionManager();
		}else {
			return txManager();
		}
	}
	
	@Bean
	public AuthorDAO jdbcAuthorDAO(){
		return new AuthorDAOImpl();
	}

	@Bean
	public UserDAO jdbcUserDAO(){
		return new UserDAOImpl();
	}
	
	@Bean
	public RoleDAO jdbcRoleDAO(){
		return new RoleDAOImpl();
	}
	
	@Bean
	public TagDAO jdbcTagDAO(){
		return new TagDAOImpl();
	}
	
	@Bean
	public NewsDAO jdbcNewsDAO(){
		return new NewsDAOImpl();
	}
	
	@Bean
	public CommentDAO jdbcCommentDAO(){
		return new CommentDAOImpl();
	}
	
	@Bean
	public AuthorDAO hibernateAuthorDAO(){
		return new HibernateAuthorDAOImpl();
	}
	
	@Bean
	public NewsDAO hibernateNewsDAO(){
		return new HibernateNewsDAOImpl();
	}
	
	@Bean
	public TagDAO hibernateTagDAO(){
		return new HibernateTagDAOImpl();
	}
	
	@Bean
	public CommentDAO hibernateCommentDAO(){
		return new HibernateCommentDAOImpl();
	}
	
	@Bean
	public AuthorDAO eclipselinkAuthorDAO(){
		return new EclipselinkAuthorDAOImpl();
	}
	
	@Bean
	public NewsDAO eclipselinkNewsDAO(){
		return new EclipselinkNewsDAOImpl();
	}
	
	@Bean
	public TagDAO eclipselinkTagDAO(){
		return new EclipselinkTagDAOImpl();
	}
	
	@Bean
	public CommentDAO eclipselinkCommentDAO(){
		return new EclipselinkCommentDAOImpl();
	}
}
