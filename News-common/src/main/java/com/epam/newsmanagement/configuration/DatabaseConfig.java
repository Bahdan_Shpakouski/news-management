package com.epam.newsmanagement.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;
import org.apache.commons.dbcp.BasicDataSource;

@Configuration
public class DatabaseConfig{

	static Logger logger = Logger.getLogger(DatabaseConfig.class);
	
	public DatabaseConfig() {
		Locale.setDefault(Locale.ENGLISH);
	}
	
	@Bean
	public DataSource dataSource(){
		Properties daoProperties = new Properties();
		InputStream input = null;
		String propFileName = "config.properties";
		BasicDataSource dataSource = new BasicDataSource();
		try {
			input = getClass().getClassLoader()
					.getResourceAsStream(propFileName);
			if (input != null) {
				daoProperties.load(input);
				dataSource.setDriverClassName(daoProperties.getProperty("driverClassName"));
				dataSource.setUrl(daoProperties.getProperty("url"));
				dataSource.setUsername(daoProperties.getProperty("username"));
				dataSource.setPassword(daoProperties.getProperty("password"));
				dataSource.setMaxActive(20);
				dataSource.setInitialSize(10);
				dataSource.setRemoveAbandoned(true);
			} else {
				logger.error("property file '" + propFileName 
						+ "' not found in the classpath");
				logger.error("DataSource initialization error");
			}
			input.close();
		} catch(IOException e){
			logger.error("properties loading error");
			logger.error("DataSource initialization error");
		}
		return dataSource;
	}
	
	@Bean
	public LocalSessionFactoryBean sessionFactory(){
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource());
		sessionFactory.setMappingResources("hibernate-config/News.hbm.xml",
				"hibernate-config/Author.hbm.xml", 
				"hibernate-config/Comment.hbm.xml",
				"hibernate-config/Tag.hbm.xml");
		Properties hibernateProperties = new Properties();
		hibernateProperties.put("hibernate.show_sql", true);
		hibernateProperties.put("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
		hibernateProperties.put("hibernate.jdbc.use_get_generated_keys", true);
		sessionFactory.setHibernateProperties(hibernateProperties);
		return sessionFactory;
	}
	
	@Bean
	public EclipseLinkJpaVendorAdapter jpaVendorAdapter(){
		EclipseLinkJpaVendorAdapter jpaVendorAdapter = new EclipseLinkJpaVendorAdapter();
		jpaVendorAdapter.setShowSql(true);
		jpaVendorAdapter.setGenerateDdl(false);
		jpaVendorAdapter.setDatabasePlatform("org.eclipse.persistence.platform.database.OraclePlatform");
		return jpaVendorAdapter;
	}
	
	@Bean
	public LocalContainerEntityManagerFactoryBean emf(){
		LocalContainerEntityManagerFactoryBean emf 
			= new LocalContainerEntityManagerFactoryBean();
		emf.setDataSource(dataSource());
		emf.setPersistenceUnitName("newsPU");
		emf.setJpaVendorAdapter(jpaVendorAdapter());
		emf.setPackagesToScan("com.epam.newsmanagement.entity");
		emf.setJpaDialect(new org.springframework.orm.jpa.vendor.EclipseLinkJpaDialect());
		Properties jpaProperties = new Properties();
		jpaProperties.put("eclipselink.weaving", "false");
		emf.setJpaProperties(jpaProperties);
		return emf;
	}

}
