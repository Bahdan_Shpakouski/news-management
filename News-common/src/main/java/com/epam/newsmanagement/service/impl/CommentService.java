package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentServiceInterface;

public class CommentService implements CommentServiceInterface {

	@Autowired
	private CommentDAO commentDAO;
	
	public CommentService() {
		
	}

	@Override
	public List<Comment> viewComments(long newsId) throws ServiceException {
		try {
			return commentDAO.readNewsComments(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Comment viewComment(long id) throws ServiceException {
		try {
			return commentDAO.read(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public Long createComment(Comment comment) throws ServiceException {
		try {
			if(comment==null) throw new ServiceException("Null parametr");
			return commentDAO.create(comment);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public void updateComment(Comment comment, long id) throws ServiceException {
		try {
			if(comment==null) throw new ServiceException("Null parametr");
			commentDAO.update(comment, id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public void deleteComment(long id) throws ServiceException {
		try {
			commentDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public void deleteNewsComments(long newsId) throws ServiceException {
		try {
			commentDAO.deleteNewsComments(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

}
