package com.epam.newsmanagement.service.impl;

import java.sql.Timestamp;
import java.util.List;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorServiceInterface;

public class AuthorService implements AuthorServiceInterface {

	@Autowired
	private AuthorDAO authorDAO;
	
	public AuthorService() {
		
	}

	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public Long createAuthor(Author author) throws ServiceException {
		try {
			if(author==null) throw new ServiceException("Null parametr");
			return authorDAO.create(author);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public void setExpired(long authorId) throws ServiceException {
		try {
			Author author = authorDAO.read(authorId);
			if(author == null) throw new ServiceException("Error:Author with "
					+ "this index does not exist");
			author.setExpired(new Timestamp(new Date().getTime()));
			authorDAO.update(author, authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public void updateAuthor(Author author, long authorId) throws ServiceException {
		try {
			if(author==null) throw new ServiceException("Null parametr");
			authorDAO.update(author, authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Author> viewAuthors() throws ServiceException {
		try {
			return authorDAO.readAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Author viewAuthor(long authorId) throws ServiceException {
		try {
			return authorDAO.read(authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Author viewNewsAuthor(long newsId) throws ServiceException {
		try {
			return authorDAO.readNewsAuthor(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Author> viewNotExpiredAuthors() throws ServiceException {
		try {
			return authorDAO.readNotExpired();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

}
