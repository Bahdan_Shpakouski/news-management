package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Role;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Service working with roles
 * @author Bahdan_Shpakouski
 *
 */
public interface RoleServiceInterface {
	
	/**
	 * Adds role to the user
	 * @param role bean containing role data
	 * @throws ServiceException
	 */
	Long addRole(Role role) throws ServiceException;
	
	/**
	 * Removes role from the user 
	 * @param roleId id of the role
	 * @throws ServiceException
	 */
	void removeRole(long roleId ) throws ServiceException;
	
	/**
	 * Updates role
	 * @param role bean containing new data
	 * @param roleId id of the role to be updated
	 * @throws ServiceException
	 */
	void updateRole (Role role, long roleId) throws ServiceException;
	
	/**
	 * Returns roles of the user with id passed
	 * @param userId id of the user
	 * @return list of roles
	 * @throws ServiceException
	 */
	List<Role> viewUserRoles(long userId) throws ServiceException;
	
	/**
	 * Deletes all roles of the user
	 * @param userId id of the user
	 * @throws ServiceException
	 */
	void deleteUserRoles(long userId) throws ServiceException;
}
