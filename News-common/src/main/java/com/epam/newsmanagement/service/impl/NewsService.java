package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.dataobject.SearchCriteriaObject;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsServiceInterface;

public class NewsService implements NewsServiceInterface {
	
	@Autowired
	private NewsDAO newsDAO;
	
	public NewsService() {
		
	}
	
	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public Long addNews(News news) throws ServiceException{
		try {
			if(news==null) throw new ServiceException("Null parametr");
			return newsDAO.create(news);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public void editNews(News news, long id) throws ServiceException{
		try {
			if(news==null) throw new ServiceException("Null parametr");
			newsDAO.update(news, id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public void deleteNews(long id) throws ServiceException{
		try {
			newsDAO.removeAuthors(id);
			newsDAO.removeTags(id);
			newsDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public List<News> viewNews() throws ServiceException{
		try {
			return newsDAO.readAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public List<News> viewSortedNews() throws ServiceException{
		try {
			return newsDAO.readSorted();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public List<News> viewSortedNews(int page) throws ServiceException{
		try {
			return newsDAO.readSorted(page);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public News viewMessage(long id) throws ServiceException{
		try {
			return newsDAO.read(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public long countNews() throws ServiceException{
		try {
			return newsDAO.countNews();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<News> searchNews(SearchCriteriaObject sco) throws ServiceException {
		try {
			if(sco==null) throw new ServiceException("Null parametr");
			return newsDAO.searchNews(sco);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public List<News> searchNews(SearchCriteriaObject sco, int page) 
			throws ServiceException {
		try {
			if(sco==null) throw new ServiceException("Null parametr");
			return newsDAO.searchNews(sco, page);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public void addAuthor(long authorId, long newsId) throws ServiceException {
		try {
			newsDAO.addAuthor(newsId, authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public void removeAuthor(long authorId, long newsId) throws ServiceException {
		try {
			newsDAO.removeAuthor(newsId, authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public void addTag(long tagId, long newsId) throws ServiceException {
		try {
			newsDAO.addTag(newsId, tagId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}

	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public void removeTag(long tagId, long newsId) throws ServiceException {
		try {
			newsDAO.removeTag(newsId, tagId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Long> viewNewsIndexes(int page) throws ServiceException {
		try {
			return newsDAO.readIndexesSorted(page);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public long countSearchResult(SearchCriteriaObject sco) throws ServiceException {
		try {
			if(sco==null) throw new ServiceException("Null parametr");
			return newsDAO.countSearchResult(sco);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public List<Long> viewSearchIndexes(SearchCriteriaObject sco, 
			int page) throws ServiceException {
		try {
			if(sco==null) throw new ServiceException("Null parametr");
			return newsDAO.readSearchIndexes(sco, page);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public int findPageIndex(long newsId) throws ServiceException {
		try {
			return newsDAO.findPageIndex(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public void removeTags(long newsId) throws ServiceException {
		try {
			newsDAO.removeTags(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public int findSearchPageIndex(SearchCriteriaObject sco, long newsId) throws ServiceException {
		try {
			return newsDAO.findSearchPageIndex(sco, newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
