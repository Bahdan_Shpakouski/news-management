package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagServiceInterface;

public class TagService implements TagServiceInterface {

	@Autowired
	private TagDAO tagDAO;
	
	public TagService() {
		
	}

	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public Long createTag(Tag tag) throws ServiceException {
		try {
			if(tag==null) throw new ServiceException("Null parametr");
			return tagDAO.create(tag);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Tag> viewTags() throws ServiceException {
		try {
			return tagDAO.readAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Tag viewTag(long tagId) throws ServiceException {
		try {
			return tagDAO.read(tagId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public void updateTag(Tag tag, long tagId) throws ServiceException {
		try {
			if(tag==null) throw new ServiceException("Null parametr");
			tagDAO.update(tag, tagId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Tag> viewNewsTags(long newsId) throws ServiceException {
		try {
			return tagDAO.readNewsTags(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public void deleteTag(long tagId) throws ServiceException {
		try {
			tagDAO.removeTagFromAllNews(tagId);
			tagDAO.delete(tagId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}	
	}

}
