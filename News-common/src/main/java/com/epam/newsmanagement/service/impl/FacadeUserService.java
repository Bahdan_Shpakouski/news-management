package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.entity.Role;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.entity.dataobject.UserTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.FacadeUserServiceInterface;
import com.epam.newsmanagement.service.RoleServiceInterface;
import com.epam.newsmanagement.service.UserServiceInterface;

public class FacadeUserService implements FacadeUserServiceInterface {
	
	@Autowired
	private UserServiceInterface userService;
	@Autowired
	private RoleServiceInterface roleService;
	
	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public void createUser(UserTO userTO) throws ServiceException {
			if(userTO==null) throw new ServiceException("Null parametr");
			long id = userService.createUser(userTO.getUser());
			for(int i=0; i<userTO.getRoles().size(); i++){
				Role role = userTO.getRoles().get(i);
				role.setUserId(id);
				roleService.addRole(role);
			}
	}

	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public void deleteUser(long userId) throws ServiceException {
		roleService.deleteUserRoles(userId);
		userService.deleteUser(userId);
	}

	@Override
	public UserTO viewUser(long userId) throws ServiceException {
		UserTO userTO = new UserTO();
		userTO.setUser(userService.viewUser(userId));
		userTO.setRoles(roleService.viewUserRoles(userId));
		return userTO;
	}

	@Override
	public List<UserTO> viewUsers() throws ServiceException {
		List<UserTO> userList = new ArrayList<UserTO>();
		List<User> users = userService.viewUsers();
		for(User user : users){
			UserTO userTO = new UserTO();
			userTO.setUser(user);
			userTO.setRoles(roleService.viewUserRoles(user.getUserId()));
			userList.add(userTO);
		}
		return userList;
	}
}
