package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Service working with users
 * @author Bahdan_Shpakouski
 *
 */
public interface UserServiceInterface {

	/**
	 * Creates user in the database
	 * @param user bean containing data to be created
	 * @return id of created user or null
	 * @throws ServiceException
	 */
	Long createUser(User user) throws ServiceException;
	
	/**
	 * Updates user
	 * @param user bean containing new data
	 * @param userId id of the user to be updated
	 * @throws ServiceException
	 */
	void updateUser(User user, long userId) throws ServiceException;
	
	/**
	 * Deletes user from the database along with his roles
	 * @param userId id of the user to be deleted
	 * @throws ServiceException
	 */
	void deleteUser(long userId)throws ServiceException;
	
	/**
	 * Returns list of all users
	 * @return list of all users
	 * @throws ServiceException
	 */
	List<User> viewUsers() throws ServiceException;
	
	/**
	 * Returns user with id passed
	 * @param userId id of the user
	 * @return user found or null
	 * @throws ServiceException
	 */
	User viewUser(long userId) throws ServiceException;
	
	/**
	 * Returns id of the user with matching pair of login and password
	 * @param user bean containing login and password
	 * @return id of the user or null
	 * @throws ServiceException
	 */
	Long login(User user) throws ServiceException;

}
