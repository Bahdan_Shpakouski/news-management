package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.entity.dataobject.NewsTO;
import com.epam.newsmanagement.entity.dataobject.SearchCriteriaObject;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorServiceInterface;
import com.epam.newsmanagement.service.CommentServiceInterface;
import com.epam.newsmanagement.service.FacadeNewsServiceInterface;
import com.epam.newsmanagement.service.NewsServiceInterface;
import com.epam.newsmanagement.service.TagServiceInterface;

public class FacadeNewsService implements FacadeNewsServiceInterface {

	@Autowired
	private NewsServiceInterface newsService;
	@Autowired
	private AuthorServiceInterface authorService;
	@Autowired
	private CommentServiceInterface commentService;
	@Autowired
	private TagServiceInterface tagService;
	
	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public Long addNews(NewsTO newsTO) throws ServiceException{
		if(newsTO==null) throw new ServiceException("Null parameter");
		Long newsId = newsService.addNews(newsTO.getNews());
		if(authorService.viewAuthor(newsTO.getAuthor().getAuthorId()) == null){
			long authorId = authorService.createAuthor(newsTO.getAuthor());
			newsService.addAuthor(authorId, newsId);
		}else{
			newsService.addAuthor(newsTO.getAuthor().getAuthorId(), newsId);
		}
		for(int i=0; i<newsTO.getTags().size(); i++){
			newsService.addTag(newsTO.getTags().get(i).getTagId(), newsId);
		}
		return newsId;
	}

	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public void deleteNews(long newsId) throws ServiceException {
		commentService.deleteNewsComments(newsId);
		newsService.deleteNews(newsId);
	}

	@Override
	public List <NewsTO> viewNews() throws ServiceException {
		List <NewsTO> newsList = new ArrayList<NewsTO>();
		List <News> list = newsService.viewNews();
		for(int i=0; i<list.size(); i++){
			newsList.add(new NewsTO());
			newsList.get(i).setNews(list.get(i));
			Long newsId = list.get(i).getNewsId();
			newsList.get(i).setComments(commentService.viewComments(newsId));
			newsList.get(i).setAuthor(authorService.viewNewsAuthor(newsId));
			newsList.get(i).setTags(tagService.viewNewsTags(newsId));
		}
		return newsList;
	}

	@Override
	public List<NewsTO> viewSortedNews() throws ServiceException {
		List<NewsTO> newsList = new ArrayList<NewsTO>();
		List<News> list = newsService.viewSortedNews();
		for(int i=0; i<list.size(); i++){
			newsList.add(new NewsTO());
			newsList.get(i).setNews(list.get(i));
			Long newsId = list.get(i).getNewsId();
			newsList.get(i).setComments(commentService.viewComments(newsId));
			newsList.get(i).setAuthor(authorService.viewNewsAuthor(newsId));
			newsList.get(i).setTags(tagService.viewNewsTags(newsId));
		}
		return newsList;
	}
	
	@Override
	public List<NewsTO> viewSortedNews(int page) throws ServiceException {
		List<NewsTO> newsList = new ArrayList<NewsTO>();
		List<News> list = newsService.viewSortedNews(page);
		for(int i=0; i<list.size(); i++){
			newsList.add(new NewsTO());
			newsList.get(i).setNews(list.get(i));
			Long newsId = list.get(i).getNewsId();
			newsList.get(i).setComments(commentService.viewComments(newsId));
			newsList.get(i).setAuthor(authorService.viewNewsAuthor(newsId));
			newsList.get(i).setTags(tagService.viewNewsTags(newsId));
		}
		return newsList;
	}

	@Override
	public NewsTO viewMessage(long newsId) throws ServiceException {
		NewsTO newsTO = new NewsTO();
		newsTO.setNews(newsService.viewMessage(newsId));
		newsTO.setAuthor(authorService.viewNewsAuthor(newsId));
		newsTO.setComments(commentService.viewComments(newsId));
		newsTO.setTags(tagService.viewNewsTags(newsId));
		return newsTO;
	}

	@Override
	public List<NewsTO> searchNews(SearchCriteriaObject sco) throws ServiceException {
		if(sco==null) throw new ServiceException("Null parametr");
		List<NewsTO> newsList = new ArrayList<NewsTO>();
		List<News> list = newsService.searchNews(sco);
		for(int i=0; i<list.size(); i++){
			newsList.add(new NewsTO());
			newsList.get(i).setNews(list.get(i));
			Long newsId = list.get(i).getNewsId();
			newsList.get(i).setComments(commentService.viewComments(newsId));
			newsList.get(i).setAuthor(authorService.viewNewsAuthor(newsId));
			newsList.get(i).setTags(tagService.viewNewsTags(newsId));
		}
		return newsList;
	}
	
	@Override
	public List<NewsTO> searchNews(SearchCriteriaObject sco, int page) throws ServiceException {
		if(sco==null) throw new ServiceException("Null parametr");
		List<NewsTO> newsList = new ArrayList<NewsTO>();
		List<News> list = newsService.searchNews(sco, page);
		for(int i=0; i<list.size(); i++){
			newsList.add(new NewsTO());
			newsList.get(i).setNews(list.get(i));
			Long newsId = list.get(i).getNewsId();
			newsList.get(i).setComments(commentService.viewComments(newsId));
			newsList.get(i).setAuthor(authorService.viewNewsAuthor(newsId));
			newsList.get(i).setTags(tagService.viewNewsTags(newsId));
		}
		return newsList;
	}

	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public void editMessage(NewsTO newsTO, long newsId) throws ServiceException {
		if(newsTO==null) throw new ServiceException("Null parametr");
		Author previousAuthor = authorService.viewNewsAuthor(newsId);
		List<Tag> previousTags = tagService.viewNewsTags(newsId);
		newsService.editNews(newsTO.getNews(), newsId);
		if(!newsTO.getAuthor().equals(previousAuthor)){
			newsService.removeAuthor(previousAuthor.getAuthorId(), newsId);
			newsService.addAuthor(newsTO.getAuthor().getAuthorId(), newsId);
		}
		if(!newsTO.getTags().equals(previousTags)){
			newsService.removeTags(newsId);
			for(Tag tag : newsTO.getTags()){
				newsService.addTag(tag.getTagId(), newsId);
			}
		}	
	}
}
