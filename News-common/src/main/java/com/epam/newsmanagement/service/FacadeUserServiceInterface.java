package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.dataobject.UserTO;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Facade service working with users and roles
 * @author Bahdan_Shpakouski
 *
 */
public interface FacadeUserServiceInterface {

	/**
	 * Creates user in the database
	 * @param userTO transfer object containing user and his roles
	 * @throws ServiceException
	 */
	void createUser(UserTO userTO) throws ServiceException;
	
	/**
	 * Deletes user from the database along with his roles
	 * @param userId id of the user to be deleted
	 * @throws ServiceException
	 */
	void deleteUser(long userId)throws ServiceException;
	
	/**
	 * Return user with id passed along with his roles
	 * @param userId id of the user
	 * @return transfer object containing user and his roles
	 * @throws ServiceException
	 */
	UserTO viewUser(long userId) throws ServiceException;
	
	/**
	 * Return all users
	 * @return list of all users
	 * @throws ServiceException
	 */
	List<UserTO> viewUsers() throws ServiceException;
}
