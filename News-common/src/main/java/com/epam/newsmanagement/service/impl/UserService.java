package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.UserServiceInterface;

public class UserService implements UserServiceInterface {

	@Autowired
	private UserDAO userDAO;
	
	public UserService() {
		
	}
	
	@Override
	public Long createUser(User user) throws ServiceException {
		try {
			if(user==null) throw new ServiceException("Null parametr");
			return userDAO.create(user);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Long login(User user) throws ServiceException {
		try {
			if(user==null) throw new ServiceException("Null parametr");
			return userDAO.login(user);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void updateUser(User user, long userId) throws ServiceException {
		try {
			if(user==null) throw new ServiceException("Null parametr");
			userDAO.update(user, userId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public void deleteUser(long userId) throws ServiceException {
		try {
			userDAO.delete(userId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		} 
	}

	@Override
	public List<User> viewUsers() throws ServiceException {
		try {
			return userDAO.readAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public User viewUser(long userId) throws ServiceException {
		try {
			return userDAO.read(userId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
