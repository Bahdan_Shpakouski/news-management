package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Service working with comments
 * @author Bahdan_Shpakouski
 * 
 */
public interface CommentServiceInterface {

	/**
	 * Returns list of comments on the news with id passed
	 * @param newsId id of the news
	 * @return list of comments on the news
	 * @throws ServiceException
	 */
	List<Comment> viewComments(long newsId) throws ServiceException;
	
	/**
	 * Returns single comment with id passed
	 * @param id id of the comment
	 * @return comment with id passed
	 * @throws ServiceException
	 */
	Comment viewComment(long id) throws ServiceException;
	
	/**
	 * Creates comment in the database
	 * @param comment bean containing data to be created
	 * @return id of created comment or null
	 * @throws ServiceException
	 */
	Long createComment(Comment comment) throws ServiceException;
	
	/**
	 * Updates comment
	 * @param comment bean containing new data
	 * @param id of the comment
	 * @throws ServiceException
	 */
	void updateComment(Comment comment, long id) throws ServiceException;
	
	/**
	 * Deletes comment from the database
	 * @param id id of the comment
	 * @throws ServiceException
	 */
	void deleteComment(long id) throws ServiceException;
	
	/**
	 * Deletes comments on the news
	 * @param newsId id of the news
	 * @throws ServiceException
	 */
	void deleteNewsComments(long newsId) throws ServiceException;
}
