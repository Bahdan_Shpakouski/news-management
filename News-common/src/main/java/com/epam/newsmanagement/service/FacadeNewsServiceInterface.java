package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.dataobject.NewsTO;
import com.epam.newsmanagement.entity.dataobject.SearchCriteriaObject;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Facade service working with news
 * @author Bahdan_Shpakouski
 *
 */
public interface FacadeNewsServiceInterface {

	/**
	 * Adds news to the database together with it's author and list of tags
	 * @param newsTO transfer object containing author, news, tags 
	 * and comments(will be ignored)
	 * @return id of the created news or null
	 * @throws ServiceException
	 */
	Long addNews(NewsTO newsTO) throws ServiceException;
	
	/**
	 * Deletes news from database along with comments
	 * @param id id of the news
	 * @throws ServiceException
	 */
	void deleteNews(long newsId) throws ServiceException;
	
	/**
	 * Returns list of all news sorted by modification date
	 * @return list of news transfer objects
	 * @throws ServiceException
	 */
	List<NewsTO> viewNews() throws ServiceException;
	
	/**
	 * Returns list of all news sorted by modification date 
	 * and comments in descending order
	 * @return list of news transfer objects 
	 * @throws ServiceException
	 */
	List<NewsTO> viewSortedNews() throws ServiceException;
	
	/**
	 * Returns one page of the list of news sorted by modification date 
	 * and comments in descending order
	 * @param page number of page
	 * @return list of news transfer objects 
	 * @throws ServiceException
	 */
	List<NewsTO> viewSortedNews(int page) throws ServiceException;
	
	/**
	 * Returns single news message together with it's author, tags and comments
	 * @param id id of the news
	 * @return transfer object containing message data, author, lists of tags and comments
	 * @throws ServiceException
	 */
	NewsTO viewMessage(long newsId) throws ServiceException;
	
	/**
	 * Edits message, could change it's author and tags
	 * @param newsTO bean containing new data
	 * @param newsId id of the news
	 * @throws ServiceException
	 */
	void editMessage(NewsTO newsTO, long newsId) throws ServiceException;
	
	/**
	 * Searches news by author and tags and returns list of news found
	 * @param sco search criteria object consisting of author and list of tags
	 * @return list of news transfer objects for news found
	 * @throws ServiceException
	 */
	List<NewsTO> searchNews(SearchCriteriaObject sco) throws ServiceException;
	
	/**
	 * Searches news by author and tags and returns one page 
	 * of the list of news found
	 * @param sco search criteria object consisting of author and list of tags
	 * @param page number of page
	 * @return list of news transfer objects for news found
	 * @throws ServiceException
	 */
	List<NewsTO> searchNews(SearchCriteriaObject sco, int page) throws ServiceException;
}
