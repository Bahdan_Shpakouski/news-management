package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.dataobject.SearchCriteriaObject;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Service working with news
 * @author Bahdan_Shpakouski
 * 
 */
public interface NewsServiceInterface {

	/**
	 * Adds news to the database
	 * @param news bean containing data to be created
	 * @return id of the created news or null
	 * @throws ServiceException
	 */
	Long addNews(News news) throws ServiceException;
	
	/**
	 * Edits news with id passed
	 * @param news bean containing new data
	 * @param id id of the news
	 * @throws ServiceException
	 */
	void editNews(News news, long id) throws ServiceException;
	
	/**
	 * Deletes news from database
	 * @param id id of the news
	 * @throws ServiceException
	 */
	void deleteNews(long id) throws ServiceException;
	
	/**
	 * Returns list of all news sorted by modification date
	 * @return list of all news
	 * @throws ServiceException
	 */
	List<News> viewNews() throws ServiceException;
	
	/**
	 * Returns list of all news sorted by modification date 
	 * and comments in descending order
	 * @return list of all sorted news 
	 * @throws ServiceException
	 */
	List<News> viewSortedNews() throws ServiceException;
	
	/**
	 * Returns one page of the list of news sorted by modification date 
	 * and comments in descending order
	 * @param page number of page
	 * @return list of all sorted news 
	 * @throws ServiceException
	 */
	List<News> viewSortedNews(int page) throws ServiceException;
	
	/**
	 * Returns list of news indexes on page
	 * @param page number of page
	 * @return list of all sorted news 
	 * @throws ServiceException
	 */
	List<Long> viewNewsIndexes(int page) throws ServiceException;
	
	/**
	 * Returns single news message
	 * @param id id of the news
	 * @return news found or null
	 * @throws ServiceException
	 */
	News viewMessage(long id) throws ServiceException;
	
	/**
	 * Counts the number of news in database
	 * @return number representing news count
	 * @throws ServiceException
	 */
	long countNews() throws ServiceException;
	
	/**
	 * Searches news by author and tags and returns list of news found
	 * @param sco search criteria object consisting of author and list of tags
	 * @return list of news found
	 * @throws ServiceException
	 */
	List<News> searchNews(SearchCriteriaObject sco) throws ServiceException;
	
	/**
	 * Searches news by author and tags and returns one page 
	 * of the list of news found
	 * @param sco search criteria object consisting of author and list of tags
	 * @param page number of page
	 * @return list of news found
	 * @throws ServiceException
	 */
	List<News> searchNews(SearchCriteriaObject sco, int page) throws ServiceException;
	
	/**
	 * Searches for news according to search criteria and returns its count
	 * @param sco search criteria object consisting of list of tags and author
	 * @return count of news found
	 * @throws ServiceException
	 */
	long countSearchResult(SearchCriteriaObject sco) throws ServiceException;
	
	/**
	 * Returns news indexes from search result page
	 * @param sco search criteria object consisting of list of tags and author
	 * @param page number of page
	 * @return  list containing news indexes from search result page
	 * @throws ServiceException
	 */
	List<Long> viewSearchIndexes(SearchCriteriaObject sco, int page) throws ServiceException;
	
	/**
	 * Adds author to a news
	 * @param authorId id of the author
	 * @param newsId id of the news
	 * @throws ServiceException
	 */
	void addAuthor(long authorId, long newsId) throws ServiceException;
	
	/**
	 * Removes author from news
	 * @param authorId id of the author
	 * @param newsId id of the news
	 * @throws ServiceException
	 */
	void removeAuthor(long authorId, long newsId) throws ServiceException;
	
	/**
	 * Adds tag to the news
	 * @param tagId id of the tag
	 * @param newsId id of the news
	 * @throws ServiceException
	 */
	void addTag(long tagId, long newsId) throws ServiceException;
	
	/**
	 * Removes tag from the news
	 * @param tagId id of the tag
	 * @param newsId id of the news
	 * @throws ServiceException
	 */
	void removeTag(long tagId, long newsId) throws ServiceException;
	
	/**
	 * Removes all tags from the news
	 * @param newsId id of the news
	 * @throws ServiceException
	 */
	void removeTags(long newsId) throws ServiceException;
	
	/**
	 * Returns index of the page containing news with id passed
	 * @param newsId id of the news
	 * @return index of the page
	 * @throws ServiceException
	 */
	int findPageIndex(long newsId) throws ServiceException;
	

	/**
	 * Returns index of the search page containing news with id passed
	 * @param sco search criteria object consisting of list of tags and author
	 * @param newsId id of the news
	 * @return index of the page
	 * @throws ServiceException
	 */
	int findSearchPageIndex(SearchCriteriaObject sco, long newsId) throws ServiceException;
}
