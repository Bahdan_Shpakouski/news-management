package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Service working with tags
 * @author Bahdan_Shpakouski
 * 
 */
public interface TagServiceInterface {

	/**
	 * Creates tag in the database
	 * @param tag bean containing data to be created
	 * @throws ServiceException
	 */
	Long createTag(Tag tag) throws ServiceException;
	
	/**
	 * Returns list of all tags
	 * @return list of all tags
	 * @throws ServiceException
	 */
	List<Tag> viewTags() throws ServiceException;
	
	/**
	 * Returns list of tags for the news with id passed
	 * @param newsId id of the news
	 * @return list of tags for the news
	 * @throws ServiceException
	 */
	List<Tag> viewNewsTags(long newsId) throws ServiceException;
	
	/**
	 * Returns tag with id passed
	 * @param tagId id of the tag
	 * @return tag with id passed
	 * @throws ServiceException
	 */
	Tag viewTag(long tagId) throws ServiceException;
	
	/**
	 * Updates tag
	 * @param tag bean containing new data
	 * @param tagId id of the tag to be updated
	 * @throws ServiceException
	 */
	void updateTag(Tag tag, long tagId) throws ServiceException;
	
	/**
	 * Deletes tag from the database
	 * @param tagId id of the tag to be deleted
	 * @throws ServiceException
	 */
	void deleteTag(long tagId) throws ServiceException;
}
