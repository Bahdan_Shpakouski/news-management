package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.RoleDAO;
import com.epam.newsmanagement.entity.Role;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.RoleServiceInterface;

public class RoleService implements RoleServiceInterface {

	@Autowired
	private RoleDAO roleDAO;
	
	@Override
	public Long addRole(Role role) throws ServiceException {
		try{
			if(role==null) throw new ServiceException("Null parametr");
			return roleDAO.create(role);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void removeRole(long roleId) throws ServiceException {
		try {
			roleDAO.delete(roleId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public void updateRole(Role role, long roleId) throws ServiceException {
		try {
			if(role==null) throw new ServiceException("Null parametr");
			roleDAO.update(role, roleId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Role> viewUserRoles(long userId) throws ServiceException {
		try{
			return roleDAO.readUserRoles(userId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteUserRoles(long userId) throws ServiceException {
		try{
			roleDAO.deleteUserRoles(userId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
