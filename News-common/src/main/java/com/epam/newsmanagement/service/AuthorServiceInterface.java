package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Service working with authors
 * @author Bahdan_Shpakouski
 * 
 */
public interface AuthorServiceInterface {
	
	/**
	 * Creates author in the database
	 * @param author bean containing data to be created
	 * @return id of created author or null
	 * @throws ServiceException
	 */
	Long createAuthor(Author author) throws ServiceException;
	
	/**
	 * Sets author expired
	 * @param authorId id of the author
	 * @throws ServiceException
	 */
	public void setExpired(long authorId) throws ServiceException;
	
	/**
	 * Updates author
	 * @param author bean containing new data
	 * @param authorId id of the author
	 * @throws ServiceException
	 */
	void updateAuthor(Author author, long authorId) throws ServiceException;
	
	/**
	 * Returns list of all authors
	 * @return list of all authors
	 * @throws ServiceException
	 */
	List<Author> viewAuthors() throws ServiceException;
	
	/**
	 * Returns list of not expired authors
	 * @return list of not expired authors
	 * @throws ServiceException
	 */
	List<Author> viewNotExpiredAuthors() throws ServiceException;
	
	/**
	 * Returns author with id passed
	 * @param authorId id of the author
	 * @return author found or null
	 * @throws ServiceException
	 */
	Author viewAuthor(long authorId) throws ServiceException;
	
	/**
	 * Returns author of the news
	 * @param newsId id of the news
	 * @return author found or null
	 * @throws ServiceException
	 */
	Author viewNewsAuthor(long newsId) throws ServiceException;
}
