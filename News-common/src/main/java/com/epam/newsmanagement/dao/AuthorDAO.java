package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;

/**
 * 
 * Interface for accessing Authors table
 * @author Bahdan_Shpakouski
 */
public interface AuthorDAO extends GenericDAO<Author>{
	
	/**
	 * Reads only not expired authors
	 * @return list of authors found
	 * @throws DAOException
	 */
	List<Author> readNotExpired() throws  DAOException;
	
	/**
	 * Reads author of the news with id passed 
	 * @param newsId id of the news
	 * @return author found or null
	 * @throws DAOException
	 */
	Author readNewsAuthor(long newsId) throws  DAOException;
}
