package com.epam.newsmanagement.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.RoleDAO;
import com.epam.newsmanagement.entity.Role;
import com.epam.newsmanagement.exception.DAOException;

public class RoleDAOImpl implements RoleDAO {

	static Logger logger = Logger.getLogger(RoleDAOImpl.class);
	private static final String SQL_SELECT_ALL = "select RLS_USER_ID,"
			+ " RLS_ROLE_NAME, RLS_ROLE_ID from roles";
	private static final String SQL_SELECT = "select RLS_USER_ID,"
			+ " RLS_ROLE_NAME, RLS_ROLE_ID from roles "
			+ "where rls_role_id=?";
	private static final String SQL_SELECT_USER_ROLES = "select RLS_USER_ID,"
			+ " RLS_ROLE_NAME, RLS_ROLE_ID from roles "
			+ "where rls_user_id=?";
	private static final String SQL_UPDATE = "update roles set rls_role_name = ?"
			+ " where rls_role_id = ?";
	private static final String SQL_INSERT = "insert into roles (rls_user_id,"
			+ " rls_role_name) values (?, ?)";
	private static final String SQL_DELETE = "delete from roles where rls_role_id= ?";
	private static final String SQL_DELETE_USER_ROLES = "delete from roles where"
			+ " rls_user_id= ?";
	private static final String SQL_STATEMENT_ERROR_MESSAGE = "Error occurred "
			+ "while closing statement";
	
	@Autowired
	private DataSource dataSource;
	
	public RoleDAOImpl() {
		
	}

	@Override
	public Long create(Role role) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		Long id = null;
		try{
			String generatedColumns[] = { "rls_role_id" };
			if(dataSource == null) throw new RuntimeException();
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(SQL_INSERT, generatedColumns);
			ps.setLong(1, role.getUserId());
			ps.setString(2, role.getRoleName());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if(rs.next()){
				id = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(SQL_STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}	
		return id;
	}

	@Override
	public List<Role> readAll() throws DAOException {
		List<Role> list = new ArrayList<Role>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_SELECT_ALL);
			rs = ps.executeQuery();
			while(rs.next()){
				Role role = new Role();
				role.setUserId(rs.getLong(1));
				role.setRoleName(rs.getString(2));
				role.setRoleId(rs.getLong(3));
				list.add(role);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(SQL_STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return list;
	}

	@Override
	public Role read(long id) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Role role = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_SELECT);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while(rs.next()){
				role = new Role();
				role.setUserId(rs.getLong(1));
				role.setRoleName(rs.getString(2));
				role.setRoleId(rs.getLong(3));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(SQL_STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return role;
	}

	@Override
	public void update(Role role, long id) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_UPDATE);
			ps.setString(1, role.getRoleName());
			ps.setLong(2, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(SQL_STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
	}

	@Override
	public void delete(long id) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_DELETE);
			ps.setLong(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(SQL_STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
	}

	@Override
	public void deleteUserRoles(long userId) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_DELETE_USER_ROLES);
			ps.setLong(1, userId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(SQL_STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
	}

	@Override
	public List<Role> readUserRoles(long userId) throws DAOException {
		List<Role> list = new ArrayList<Role>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_SELECT_USER_ROLES);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			while(rs.next()){
				Role role = new Role();
				role.setUserId(rs.getLong(1));
				role.setRoleName(rs.getString(2));
				role.setRoleId(rs.getLong(3));
				list.add(role);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(SQL_STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return list;
	}

}
