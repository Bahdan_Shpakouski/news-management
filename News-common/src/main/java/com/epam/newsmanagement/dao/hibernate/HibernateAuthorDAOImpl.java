package com.epam.newsmanagement.dao.hibernate;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

public class HibernateAuthorDAOImpl implements AuthorDAO {

	private static final String HQL_SELECT_ALL = "FROM Author";
	private static final String HQL_SELECT_NOT_EXPIRED = "FROM Author A WHERE A.expired is null";
	private static final String HQL_SELECT_NEWS_AUTHOR = "SELECT N.authors FROM News N "
			+ "WHERE N.newsId=:news_id";
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Long create(Author author) throws DAOException{
		Long authorId = null;
		try{
			Session session = sessionFactory.getCurrentSession();
			authorId = (Long) session.save(author);
		}catch(HibernateException e){
			throw new DAOException(e);
		}
		return authorId;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Author> readAll() throws DAOException{
		List<Author> authors = null;
		Session session = sessionFactory.openSession();
		try{
			Query query = session.createQuery(HQL_SELECT_ALL);
			authors = query.list();
		}catch(HibernateException e){
			throw new DAOException(e);
		}finally{
			session.close();
		}
		return authors;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Author> readNotExpired() throws DAOException{
		List<Author> authors = null;
		Session session = sessionFactory.openSession();
		try{
			Query query = session.createQuery(HQL_SELECT_NOT_EXPIRED);
			authors = query.list();
		}catch(HibernateException e){
			throw new DAOException(e);
		}finally{
			session.close();
		}
		return authors;
	}

	@Override
	public Author read(long authorId) throws DAOException{
		Session session = sessionFactory.openSession();
		Author author = null;
		try{
			author = session.get(Author.class, authorId);
		}catch(HibernateException e){
			throw new DAOException(e);
		}finally{
			session.close();
		}
		return author;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Author readNewsAuthor(long newsId) throws DAOException{
		List<Author> authors = null;
		Author author = null;
		Session session = sessionFactory.openSession();
		try{
			Query query = session.createQuery(HQL_SELECT_NEWS_AUTHOR);
			query.setParameter("news_id", newsId);
			authors = query.list();
			if(!authors.isEmpty()){
				author = authors.get(0);
			}
		}catch(HibernateException e){
			throw new DAOException(e);
		}finally{
			session.close();
		}
		return author;
	}

	@Override
	public void update(Author author, long authorId) throws DAOException{
		try{
			Session session = sessionFactory.getCurrentSession();
			Author oldAuthor = session.get(Author.class, authorId);
			oldAuthor.setAuthorId(authorId);
			oldAuthor.setAuthorName(author.getAuthorName());
			oldAuthor.setExpired(author.getExpired());
			session.save(oldAuthor);
		}catch(HibernateException e){
			throw new DAOException(e);
		}
	}

	@Override
	public void delete(long authorId) throws DAOException{
		Transaction tx = null;
		Session session = sessionFactory.openSession();
		try{
			tx = session.beginTransaction();
			Author author = session.get(Author.class, authorId);
			session.delete(author);
			tx.commit();
		}catch(HibernateException e){
			throw new DAOException(e);
		}finally{
			session.close();
		}
	}

}
