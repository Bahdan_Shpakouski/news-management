package com.epam.newsmanagement.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;

public class UserDAOImpl implements UserDAO {

	static Logger logger = Logger.getLogger(UserDAOImpl.class);
	private static final String SQL_SELECT_ALL = "select USR_USER_ID,"
			+ " USR_USER_NAME, USR_LOGIN, USR_PASSWORD from users";
	private static final String SQL_SELECT = "select USR_USER_ID,"
			+ " USR_USER_NAME, USR_LOGIN, USR_PASSWORD from users "
			+ "where usr_user_id=?";
	private static final String SQL_UPDATE = "update users set usr_user_name = ?,"
			+ " usr_login = ?, usr_password = ? where usr_user_id = ?";
	private static final String SQL_INSERT = "insert into users "
			+ "(usr_user_name, usr_login, usr_password) values (?, ?, ?)";
	private static final String SQL_DELETE = "delete from users "
			+ "where usr_user_id= ?";
	private static final String STATEMENT_ERROR_MESSAGE = "Error occurred "
			+ "while closing statement";
	private static final String SQL_LOGIN = "select usr_user_id from users where "
			+ "usr_login=? and usr_password=?";
	
	public UserDAOImpl() {
		
	}
	
	@Autowired
	private DataSource dataSource;

	@Override
	public Long create(User user) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		Long id = null;
		try{ 
			String generatedColumns[] = { "usr_user_id" };
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_INSERT, generatedColumns);
			ps.setString(1, user.getUserName());
			ps.setString(2, user.getLogin());
			ps.setString(3, user.getPassword());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if(rs.next()){
				id=rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return id;
	}

	@Override
	public List<User> readAll() throws DAOException {
		List<User> list = new ArrayList<User>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_SELECT_ALL);
			rs = ps.executeQuery();
			while(rs.next()){
				User user = new User();
				user.setUserId(rs.getLong(1));
				user.setUserName(rs.getString(2));
				user.setLogin(rs.getString(3));
				user.setPassword(rs.getString(4));
				list.add(user);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return list;
	}
	
	@Override
	public User read(long id) throws DAOException {
		User user = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_SELECT);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while(rs.next()){
				user = new User();
				user.setUserId(rs.getLong(1));
				user.setUserName(rs.getString(2));
				user.setLogin(rs.getString(3));
				user.setPassword(rs.getString(4));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return user;
	}

	@Override
	public void update(User user, long id) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_UPDATE);
			ps.setString(1, user.getUserName());
			ps.setString(2, user.getLogin());
			ps.setString(3, user.getPassword());
			ps.setLong(4, user.getUserId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
	}

	@Override
	public void delete(long id) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_DELETE);
			ps.setLong(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
	}

	@Override
	public Long login(User user) throws DAOException {
		Long id = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_LOGIN);
			ps.setString(1, user.getLogin());
			ps.setString(2, user.getPassword());
			rs = ps.executeQuery();
			while(rs.next()){
				id = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return id;
	}

}
