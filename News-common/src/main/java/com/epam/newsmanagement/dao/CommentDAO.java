package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Interface for accessing Comments table
 * @author Bahdan_Shpakouski
 * 
 */
public interface CommentDAO extends GenericDAO<Comment> {
	
	/**
	 * Reads comments on the news with id passed
	 * @param newsId id of the news
	 * @return list of Comments found for the news
	 * @throws DAOException
	 */
	List<Comment> readNewsComments(long newsId) throws DAOException;
	
	/**
	 * Deletes comments on the news with id passed
	 * @param newsId id of the news
	 * @throws DAOException
	 */
	void deleteNewsComments(long newsId) throws DAOException;
}
