package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Interface for accessing Users table
 * @author Bahdan_Shpakouski
 *
 */
public interface UserDAO extends GenericDAO<User> {
	
	/**
	 * Searches for matching pair of login and password in the Users table
	 * @param user bean containing user data (only login and password fields are required)
	 * @return id of the user found or null;
	 * @throws DAOException
	 */
	Long login(User user) throws DAOException;
}
