package com.epam.newsmanagement.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;

public class HibernateTagDAOImpl implements TagDAO {

	private static final String HQL_SELECT_ALL = "FROM Tag";
	private static final String HQL_SELECT_NEWS_TAGS = "SELECT N.tags FROM News N "
			+ "WHERE N.newsId=:news_id";
	private static final String SQL_REMOVE_TAG_FROM_NEWS = "delete from news_tags "
			+ "where nt_tag_id= ?";
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Long create(Tag tag) throws DAOException {
		Long tagId = null;
		try{
			Session session = sessionFactory.getCurrentSession();
			tagId = (Long) session.save(tag);
		}catch(HibernateException e){
			throw new DAOException(e);
		}
		return tagId;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> readAll() throws DAOException {
		Session session = sessionFactory.openSession();
		List<Tag> tags = null;
		try{
			Query query = session.createQuery(HQL_SELECT_ALL);
			tags = query.list();
		}catch(HibernateException e){
			throw new DAOException(e);
		}finally{
			session.close();
		}
		return tags;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> readNewsTags(long newsId) throws DAOException {
		Session session = sessionFactory.openSession();
		List<Tag> tags = null;
		try{
			Query query = session.createQuery(HQL_SELECT_NEWS_TAGS);
			query.setParameter("news_id", newsId);
			tags = query.list();
		}catch(HibernateException e){
			throw new DAOException(e);
		}finally{
			session.close();
		}
		return tags;
	}

	@Override
	public Tag read(long tagId) throws DAOException {
		Session session = sessionFactory.openSession();
		Tag tag = null;
		try{
			tag = session.get(Tag.class, tagId);
		}catch(HibernateException e){
			throw new DAOException(e);
		}finally{
			session.close();
		}
		return tag;
	}

	@Override
	public void update(Tag tag, long tagId) throws DAOException {
		//Transaction tx = null;
		try{
			//tx = session.beginTransaction();
			Session session = sessionFactory.getCurrentSession();
			Tag oldTag = session.get(Tag.class, tagId);
			oldTag.setTagId(tagId);
			oldTag.setTagName(tag.getTagName());
			session.save(oldTag);
			//tx.commit();
		}catch(HibernateException e){
			throw new DAOException(e);
		}
	}

	@Override
	public void delete(long tagId) throws DAOException {
		//Transaction tx = null;
		try{
			Session session = sessionFactory.getCurrentSession();
			//tx = session.beginTransaction();
			Tag tag = session.get(Tag.class, tagId);
			session.delete(tag);
			//tx.commit();
		}catch(HibernateException e){
			throw new DAOException(e);
		}
	}

	@Override
	public void removeTagFromAllNews(long tagId) throws DAOException {
		//Transaction tx = null;
		try{
			//tx = session.beginTransaction();
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createSQLQuery(SQL_REMOVE_TAG_FROM_NEWS);
			query.setLong(0, tagId);
			query.executeUpdate();
			//tx.commit();
		}catch(HibernateException e){
			throw new DAOException(e);
		}
	}

}
