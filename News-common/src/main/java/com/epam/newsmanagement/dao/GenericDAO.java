package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.exception.DAOException;

/**
 * Generic DAO interface that declares basic CRUD operation
 * @author Bahdan_Shpakouski
 *
 * @param <T> T refers to concrete DAO class working with specific table
 */
public interface GenericDAO<T> {

	/**
	 * Creates row in a table
	 * @param value bean containing record data
	 * @return id of created record
	 * @throws DAOException
	 */
	Long create(T value) throws DAOException;
	
	/**
	 * Reads all rows from a table
	 * @return list of beans containing all data
	 * @throws DAOException
	 */
	List<T> readAll() throws DAOException;
	
	/**
	 * Reads 1 record with id passed from a table
	 * @param id of required record
	 * @return bean containing record data or null
	 * @throws DAOException
	 */
	T read(long id) throws DAOException;
	
	/**
	 * Updates row with id passed in a table 
	 * @param value Bean containing new record data
	 * @param id id of a record to be updated
	 * @throws DAOException
	 */
	void update(T value, long id) throws DAOException;
	
	/**
	 * Deletes row with id passed from a table
	 * @param id id of a record to be deleted
	 * @throws DAOException
	 */
	void delete(long id) throws DAOException;
}
