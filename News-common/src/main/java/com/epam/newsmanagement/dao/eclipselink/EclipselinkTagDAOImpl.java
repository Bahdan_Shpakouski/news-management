package com.epam.newsmanagement.dao.eclipselink;

import java.util.List;
import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;

public class EclipselinkTagDAOImpl implements TagDAO {

	private static final String JPQL_SELECT_ALL = "SELECT T FROM Tag T";
	private static final String JPQL_SELECT_NEWS_TAGS = "SELECT N.tags FROM News N "
			+ "WHERE N.newsId=:news_id";
	private static final String SQL_REMOVE_TAG_FROM_NEWS = "delete from news_tags "
			+ "where nt_tag_id= ?";
	
	@Autowired
	private EntityManagerFactory emf;
	@PersistenceContext
	private EntityManager entityManager;
	
	public EclipselinkTagDAOImpl() {
		
	}

	@Override
	public Long create(Tag value) throws DAOException {
		entityManager.persist(value);
		Long commentId = (Long) emf.getPersistenceUnitUtil().getIdentifier(value);
		return commentId;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> readAll() throws DAOException {
		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery(JPQL_SELECT_ALL);
		query.setHint("eclipselink.result-collection-type", java.util.ArrayList.class);
		List<Tag> list = query.getResultList();
		em.close();
		return list;
	}

	@Override
	public Tag read(long id) throws DAOException {
		EntityManager em = emf.createEntityManager();
		Tag tag = em.find(Tag.class, id);
		em.close();
		return tag;
	}

	
	@Override
	public void update(Tag value, long id) throws DAOException {
		Tag tag = entityManager.find(Tag.class, id);
		tag.setTagName(value.getTagName());
	}

	@Override
	public void delete(long id) throws DAOException {
		Tag tag = entityManager.find(Tag.class, id);
		entityManager.remove(tag);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> readNewsTags(long newsId) throws DAOException {
		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery(JPQL_SELECT_NEWS_TAGS);
		query.setHint("eclipselink.result-collection-type", java.util.ArrayList.class);
		query.setParameter("news_id", newsId);
		List<Tag> list = query.getResultList();
		if(list.size()==1 && list.get(0) == null){
			list = new ArrayList<Tag>();
		}
		em.close();
		return list;
	}

	@Override
	public void removeTagFromAllNews(long tagId) throws DAOException {
		Query query = entityManager.createNativeQuery(SQL_REMOVE_TAG_FROM_NEWS);
		query.setParameter(1, tagId);
		query.executeUpdate();
	}

}
