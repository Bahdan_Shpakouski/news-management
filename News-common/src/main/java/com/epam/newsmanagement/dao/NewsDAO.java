package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.dataobject.SearchCriteriaObject;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Interface for accessing News table
 * @author Bahdan_Shpakouski
 * 
 */
public interface NewsDAO extends GenericDAO<News>{

	/**
	 * Reads all news sorted by modification date
	 * @return list containing all news
	 * @throws DAOException
	 */
	List<News> readAll() throws DAOException;
	
	/**
	 * Reads all news sorted by modification date and comments count in descending order
	 * @return list containing all news
	 * @throws DAOException
	 */
	List<News> readSorted() throws DAOException;
	
	/**
	 * Reads 5 news according to page number from all news
	 *  sorted by modification date and comments count in descending order
	 * @param page number of page
	 * @return list containing all news
	 * @throws DAOException
	 */
	List<News> readSorted(int page) throws DAOException;
	
	/**
	 * Reads news indexes from page
	 * @param page number of page
	 * @return list containing news indexes from page
	 * @throws DAOException
	 */
	List<Long> readIndexesSorted(int page) throws DAOException;
	
	/**
	 * Counts the number of news
	 * @return number representing count of news
	 * @throws DAOException
	 */
	long countNews() throws DAOException;
	
	/**
	 * Creates row in News_authors table establishing relationship
	 *  between News and authors Tables records
	 * @param newsId id of the news
	 * @param authorId id of the author
	 * @throws DAOException
	 */
	void addAuthor(long newsId, long authorId) throws DAOException;
	
	/**
	 * Deletes row from News_authors table removing relationship
	 * between News and authors Tables records
	 * @param newsId id of the news
	 * @param authorId id of the author
	 * @throws DAOException
	 */
	void removeAuthor(long newsId, long authorId) throws DAOException;
	
	/**
	 * Deletes all authors of the news with id passed 
	 * from News_authors table
	 * @param newsId id of the news
	 * @throws DAOException
	 */
	void removeAuthors(long newsId) throws DAOException;
	
	/**
	 * Creates row in News_Tags table establishing 
	 * relationship between News and Tags tables records
	 * @param newsId id of the news
	 * @param tagId id of the tag
	 * @throws DAOException
	 */
	void addTag(long newsId, long tagId) throws DAOException;
	
	/**
	 * Deletes row from News_tags table
	 * removing relationship between News and Tags tables records
	 * @param newsId id of the news
	 * @param tagId  id of the tag
	 * @throws DAOException
	 */
	void removeTag(long newsId, long tagId) throws DAOException;

	/**
	 * Deletes all tags for the news with id passed 
	 * from News_authors table
	 * @param newsId id of the news
	 * @throws DAOException
	 */
	void removeTags(long newsId) throws DAOException;
	
	/**
	 * Searches for news according to search criteria
	 * @param sco search criteria object consisting of list of tags and author
	 * @return list of news found
	 * @throws DAOException
	 */
	List<News> searchNews(SearchCriteriaObject sco) throws DAOException;
	
	/**
	 * Searches for news according to search criteria and page number
	 * @param sco search criteria object consisting of list of tags and author
	 * @param page number of page
	 * @return list of news found
	 * @throws DAOException
	 */
	List<News> searchNews(SearchCriteriaObject sco, int page) throws DAOException;
	
	/**
	 * Searches for news according to search criteria and returns its count
	 * @param sco search criteria object consisting of list of tags and author
	 * @return count of news found
	 * @throws DAOException
	 */
	long countSearchResult(SearchCriteriaObject sco) throws DAOException;
	
	/**
	 * Reads news indexes from search result page
	 * @param sco search criteria object consisting of list of tags and author
	 * @param page number of page
	 * @return  list containing news indexes from search result page
	 * @throws DAOException
	 */
	List<Long> readSearchIndexes(SearchCriteriaObject sco, int page) throws DAOException;
	
	/**
	 * Returns index of the page containing news with id passed
	 * @param newsId id of the news
	 * @return index of the page
	 * @throws DAOException
	 */
	int findPageIndex(long newsId) throws DAOException;
	
	/**
	 * Returns index of the search page containing news with id passed
	 * @param sco search criteria object consisting of list of tags and author
	 * @param newsId id of the news
	 * @return index of the page
	 * @throws DAOException
	 */
	int findSearchPageIndex(SearchCriteriaObject sco, long newsId) throws DAOException;
	
}
