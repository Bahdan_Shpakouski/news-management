package com.epam.newsmanagement.dao.eclipselink;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.entity.dataobject.SearchCriteriaObject;
import com.epam.newsmanagement.exception.DAOException;

public class EclipselinkNewsDAOImpl implements NewsDAO {

	private static final String JPQL_SELECT_ALL = "select N FROM News N order by "
			+ "N.modificationDate desc";
	private static final String JPQL_SELECT_SORTED = "select N, "
			+ "(select count(C) from Comment C where C.news.newsId = N.newsId) as commentCount "
			+ "FROM News N "
			+ "order by N.modificationDate desc, commentCount desc,"
			+ " N.creationDate desc, N.newsId";
	private static final String JPQL_SELECT_INDEXES_SORTED = "select N.newsId,"
			+ " (select count(C) from Comment C where C.news.newsId = N.newsId) as commentCount"
			+ " FROM News N "
			+ "order by N.modificationDate desc, commentCount desc,"
			+ " N.creationDate desc, N.newsId";
	private static final String JPQL_COUNT_NEWS = "select count(N) from News N";
	private static final String JPQL_SEARCH_BEGINNING = "select N, "
			+ "(select count(C) from Comment C where  C.news.newsId = N.newsId) as commentCount"
			+ " from News N "
			+ "inner join N.authors A "
			+ "where A.authorId =:authorId"
			+ " and (select count(T) from N.tags T where ";
	private static final String JPQL_NO_AUTHOR_SEARCH_BEGINNING = "select N, "
			+ "(select count(C) from Comment C where C.news.newsId = N.newsId) as commentCount"
			+ " from News N "
			+ "where "
			+ "(select count(T) from N.tags T where ";
	private static final String JPQL_SEARCH_ENDING = " order by "
			+ "N.modificationDate desc, commentCount desc, "
			+ "N.creationDate desc, N.newsId";
	private static final String JPQL_NO_TAG_SEARCH = "select N, "
			+ "(select count(C) from Comment C where C.news.newsId = N.newsId) as commentCount"
			+ " from News N "
			+ "inner join N.authors A "
			+ "where A.authorId =:authorId"
			+ " order by N.modificationDate desc, commentCount desc,"
			+ " N.creationDate desc, N.newsId";
	private static final String JPQL_SEARCH_COUNT_BEGINNING = "select count(N) "
			+ "from News N "
			+ "inner join N.authors A "
			+ "where A.authorId =:authorId"
			+ " and (select count(T) from N.tags T where ";
	private static final String JPQL_NO_AUTHOR_SEARCH_COUNT_BEGINNING = "select count(N) "
			+ "from News N "
			+ "where (select count(T) from N.tags T where ";
	private static final String JPQL_NO_TAG_SEARCH_COUNT = "select count(N) "
			+ " from News N "
			+ "inner join N.authors A "
			+ "where A.authorId =:authorId";
	private static final String JPQL_SEARCH_BEGINNING_INDEXES = "select N.newsId, "
			+ "(select count(C) from Comment C where  C.news.newsId = N.newsId) as commentCount"
			+ " from News N "
			+ "inner join N.authors A "
			+ "where A.authorId =:authorId"
			+ " and (select count(T) from N.tags T where ";
	private static final String JPQL_NO_AUTHOR_SEARCH_BEGINNING_INDEXES = "select N.newsId, "
			+ "(select count(C) from Comment C where C.news.newsId = N.newsId) as commentCount"
			+ " from News N "
			+ "where "
			+ "(select count(T) from N.tags T where ";
	private static final String JPQL_NO_TAG_SEARCH_INDEXES = "select N.newsId, "
			+ "(select count(C) from Comment C where C.news.newsId = N.newsId) as commentCount"
			+ " from News N "
			+ "inner join N.authors A "
			+ "where A.authorId =:authorId"
			+ " order by N.modificationDate desc, commentCount desc,"
			+ " N.creationDate desc, N.newsId";
	private static final String SQL_FIND_ROW_INDEX = "select r from "
			+ "(select rownum r, nws_news_id "
			+ "from "
			+ "(select nws_news_id, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count"
			+ " from news "
			+ "order by nws_modification_date desc,"
			+ " comment_count desc, nws_creation_date desc, nws_news_id))"
			+ " where nws_news_id=?";
	private static final String SQL_REMOVE_AUTHOR = "delete from news_authors"
			+ " where na_news_id = ? and na_author_id = ?";
	private static final String SQL_REMOVE_AUTHORS = "delete from news_authors"
			+ " where na_news_id = ?";
	private static final String SQL_REMOVE_TAG = "delete from news_tags"
			+ " where nt_news_id = ? and nt_tag_id = ?";
	private static final String SQL_REMOVE_TAGS = "delete from news_tags"
			+ " where nt_news_id = ?";
	private static final String SQL_FIND_SEARCH_ROW_INDEX = "select r from "
			+ "(select rownum r, nws_news_id "
			+ "from "
			+ "(select nws_news_id, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count"
			+ " from news "
			+ "inner join news_authors "
			+ "on na_news_id=nws_news_id "
			+ "where na_author_id=? "
			+ "and (select count(*) from news_tags where nt_news_id=nws_news_id "
			+ "and (";
	private static final String SQL_FIND_NO_AUTHOR_SEARCH_ROW_INDEX = "select r from "
			+ "(select rownum r, nws_news_id "
			+ "from "
			+ "(select nws_news_id, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count"
			+ " from news "
			+ "where "
			+ "(select count(*) from news_tags where nt_news_id=nws_news_id "
			+ "and (";
	private static final String SQL_FIND_NO_TAG_SEARCH_ROW_INDEX = "select r from "
			+ "(select rownum r, nws_news_id "
			+ "from "
			+ "(select nws_news_id, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count"
			+ " from news "
			+ "inner join news_authors "
			+ "on na_news_id=nws_news_id "
			+ "where na_author_id=? "
			+ "order by nws_modification_date desc, comment_count desc,"
			+ " nws_creation_date desc, nws_news_id)) "
			+ "where nws_news_id=?";
	private static final String SQL_FIND_SEARCH_ROW_INDEX_ENDING = "order by "
			+ "nws_modification_date desc, comment_count desc,"
			+ " nws_creation_date desc, nws_news_id)) where nws_news_id=?";

	
	@Autowired
	private EntityManagerFactory emf;
	@PersistenceContext
	private EntityManager entityManager;
	
	public EclipselinkNewsDAOImpl() {
		
	}

	@Override
	public Long create(News value) throws DAOException {
		value.setCreationDate(new Timestamp(new Date().getTime()));
		value.setModificationDate(new Timestamp(new Date().getTime()));
		entityManager.persist(value);
		Long newsId = (Long) emf.getPersistenceUnitUtil().getIdentifier(value);
		return newsId;
	}

	@Override
	public News read(long id) throws DAOException {
		EntityManager em = emf.createEntityManager();
		News news = em.find(News.class, id);
		em.close();
		return news;
	}

	@Override
	public void update(News value, long id) throws DAOException {
		News news = entityManager.find(News.class, id);
		news.setFullText(value.getFullText());
		news.setShortText(value.getShortText());
		news.setTitle(value.getTitle());
		news.setModificationDate(new Timestamp(new Date().getTime()));
	}

	@Override
	public void delete(long id) throws DAOException {
		News news = entityManager.find(News.class, id);
		entityManager.remove(news);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> readAll() throws DAOException {
		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery(JPQL_SELECT_ALL);
		query.setHint("eclipselink.result-collection-type", java.util.ArrayList.class);
		List<News> list = query.getResultList();
		em.close();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> readSorted() throws DAOException {
		EntityManager em = emf.createEntityManager();
		List<News> list = new ArrayList<News>();
		Query query = em.createQuery(JPQL_SELECT_SORTED);
		query.setHint("eclipselink.result-collection-type", java.util.ArrayList.class);
		List<Object[]> results = (ArrayList<Object[]>) query.getResultList();
		for(Object ob[] : results){
			list.add((News)ob[0]);
		}
		em.close();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> readSorted(int page) throws DAOException {
		EntityManager em = emf.createEntityManager();
		List<News> list = new ArrayList<News>();
		Query query = em.createQuery(JPQL_SELECT_SORTED);
		query.setHint("eclipselink.result-collection-type", java.util.ArrayList.class);
		query.setFirstResult((page-1)*5);
		query.setMaxResults(5);
		List<Object[]> results = (ArrayList<Object[]>) query.getResultList();
		for(Object ob[] : results){
			list.add((News)ob[0]);
		}
		em.close();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Long> readIndexesSorted(int page) throws DAOException {
		EntityManager em = emf.createEntityManager();
		List<Long> list = new ArrayList<Long>();
		Query query = em.createQuery(JPQL_SELECT_INDEXES_SORTED);
		query.setHint("eclipselink.result-collection-type", java.util.ArrayList.class);
		query.setFirstResult((page-1)*5);
		query.setMaxResults(5);
		List<Object[]> results = (ArrayList<Object[]>) query.getResultList();
		for(Object ob[] : results){
			list.add((Long)ob[0]);
		}
		em.close();
		return list;
	}

	@Override
	public long countNews() throws DAOException {
		EntityManager em = emf.createEntityManager();
		Long count = (Long) em.createQuery(JPQL_COUNT_NEWS).getSingleResult();
		em.close();
		return count;
	}

	@Override
	public void addAuthor(long newsId, long authorId) throws DAOException {
		News news = entityManager.find(News.class, newsId);
		Author author = entityManager.find(Author.class, authorId);
		news.getAuthors().add(author);
	}

	@Override
	public void removeAuthor(long newsId, long authorId) throws DAOException {
		/*News news = em.find(News.class, newsId);
		Author author = em.find(Author.class, authorId);
		news.getAuthors().remove(author);*/
		Query query = entityManager.createNativeQuery(SQL_REMOVE_AUTHOR);
		query.setParameter(1, newsId);
		query.setParameter(2, authorId);
		query.executeUpdate();
	}

	@Override
	public void removeAuthors(long newsId) throws DAOException {
		/*News news = em.find(News.class, newsId);
		news.getAuthors().clear();*/
		Query query = entityManager.createNativeQuery(SQL_REMOVE_AUTHORS);
		query.setParameter(1, newsId);
		query.executeUpdate();
	}

	@Override
	public void addTag(long newsId, long tagId) throws DAOException {
		News news = entityManager.find(News.class, newsId);
		Tag tag = entityManager.find(Tag.class, tagId);
		news.getTags().add(tag);
	}

	@Override
	public void removeTag(long newsId, long tagId) throws DAOException {
		/*News news = em.find(News.class, newsId);
		Tag tag = em.find(Tag.class, tagId);
		news.getTags().remove(tag);*/
		Query query = entityManager.createNativeQuery(SQL_REMOVE_TAG);
		query.setParameter(1, newsId);
		query.setParameter(2, tagId);
		query.executeUpdate();
	}

	@Override
	public void removeTags(long newsId) throws DAOException {
		/*News news = em.find(News.class, newsId);
		news.getTags().clear();*/
		Query query = entityManager.createNativeQuery(SQL_REMOVE_TAGS);
		query.setParameter(1, newsId);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> searchNews(SearchCriteriaObject sco) throws DAOException {
		List<News> list = new ArrayList<News>();
		boolean addTags = false;
		StringBuilder sb;
		int tagCount = sco.getTags().size();
		if(tagCount == 0 && sco.getAuthorId() == 0){
			return readSorted();
		}else if(tagCount == 0){
			sb  = new StringBuilder(JPQL_NO_TAG_SEARCH);
		}else if(sco.getAuthorId() == 0){
			sb = new StringBuilder(JPQL_NO_AUTHOR_SEARCH_BEGINNING);
			addTags = true;
		}else{
			sb  = new StringBuilder(JPQL_SEARCH_BEGINNING);
			addTags = true;
		}
		if(addTags){
			for(int i=0; i<tagCount; i++){
				if(i>0){
					sb.append("or ");
				}
				sb.append("T.tagId="+sco.getTags().get(i)+" ");
			}
			sb.append(")="+tagCount);
			sb.append(JPQL_SEARCH_ENDING);
		}
		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery(sb.toString());
		query.setHint("eclipselink.result-collection-type", java.util.ArrayList.class);
		if(sco.getAuthorId() != 0){
			query.setParameter("authorId", sco.getAuthorId());
		}
		List<Object[]> results = (ArrayList<Object[]>) query.getResultList();
		for(Object ob[] : results){
			list.add((News)ob[0]);
		}
		em.close();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> searchNews(SearchCriteriaObject sco, int page) throws DAOException {
		List<News> list = new ArrayList<News>();
		boolean addTags = false;
		StringBuilder sb;
		int tagCount = sco.getTags().size();
		if(tagCount == 0 && sco.getAuthorId() == 0){
			return readSorted(page);
		}else if(tagCount == 0){
			sb  = new StringBuilder(JPQL_NO_TAG_SEARCH);
		}else if(sco.getAuthorId() == 0){
			sb = new StringBuilder(JPQL_NO_AUTHOR_SEARCH_BEGINNING);
			addTags = true;
		}else{
			sb  = new StringBuilder(JPQL_SEARCH_BEGINNING);
			addTags = true;
		}
		if(addTags){
			for(int i=0; i<tagCount; i++){
				if(i>0){
					sb.append("or ");
				}
				sb.append("T.tagId="+sco.getTags().get(i)+" ");
			}
			sb.append(")="+tagCount);
			sb.append(JPQL_SEARCH_ENDING);
		}
		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery(sb.toString());
		query.setHint("eclipselink.result-collection-type", java.util.ArrayList.class);
		if(sco.getAuthorId() != 0){
			query.setParameter("authorId", sco.getAuthorId());
		}
		query.setFirstResult((page-1)*5);
		query.setMaxResults(5);
		List<Object[]> results = (ArrayList<Object[]>) query.getResultList();
		for(Object ob[] : results){
			list.add((News)ob[0]);
		}
		em.close();
		return list;
	}

	@Override
	public long countSearchResult(SearchCriteriaObject sco) throws DAOException {
		boolean addTags = false;
		StringBuilder sb;
		int tagCount = sco.getTags().size();
		if(tagCount == 0 && sco.getAuthorId() == 0){
			return countNews();
		}else if(tagCount == 0){
			sb  = new StringBuilder(JPQL_NO_TAG_SEARCH_COUNT);
		}else if(sco.getAuthorId() == 0){
			sb = new StringBuilder(JPQL_NO_AUTHOR_SEARCH_COUNT_BEGINNING);
			addTags = true;
		}else{
			sb  = new StringBuilder(JPQL_SEARCH_COUNT_BEGINNING);
			addTags = true;
		}
		if(addTags){
			for(int i=0; i<tagCount; i++){
				if(i>0){
					sb.append("or ");
				}
				sb.append("T.tagId="+sco.getTags().get(i)+" ");
			}
			sb.append(")="+tagCount);
		}
		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery(sb.toString());
		if(sco.getAuthorId() != 0){
			query.setParameter("authorId", sco.getAuthorId());
		}
		long count = (long) query.getSingleResult();
		em.close();
		return count;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Long> readSearchIndexes(SearchCriteriaObject sco, int page) throws DAOException {
		List<Long> list = new ArrayList<Long>();
		boolean addTags = false;
		StringBuilder sb;
		int tagCount = sco.getTags().size();
		if(tagCount == 0 && sco.getAuthorId() == 0){
			return readIndexesSorted(page);
		}else if(tagCount == 0){
			sb  = new StringBuilder(JPQL_NO_TAG_SEARCH_INDEXES);
		}else if(sco.getAuthorId() == 0){
			sb = new StringBuilder(JPQL_NO_AUTHOR_SEARCH_BEGINNING_INDEXES);
			addTags = true;
		}else{
			sb  = new StringBuilder(JPQL_SEARCH_BEGINNING_INDEXES);
			addTags = true;
		}
		if(addTags){
			for(int i=0; i<tagCount; i++){
				if(i>0){
					sb.append("or ");
				}
				sb.append("T.tagId="+sco.getTags().get(i)+" ");
			}
			sb.append(")="+tagCount);
			sb.append(JPQL_SEARCH_ENDING);
		}
		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery(sb.toString());
		query.setHint("eclipselink.result-collection-type", java.util.ArrayList.class);
		if(sco.getAuthorId() != 0){
			query.setParameter("authorId", sco.getAuthorId());
		}
		query.setFirstResult((page-1)*5);
		query.setMaxResults(5);
		List<Object[]> results = (ArrayList<Object[]>) query.getResultList();
		for(Object ob[] : results){
			list.add((Long)ob[0]);
		}
		em.close();
		return list;
	}

	@Override
	public int findPageIndex(long newsId) throws DAOException {
		EntityManager em = emf.createEntityManager();
		Query query = em.createNativeQuery(SQL_FIND_ROW_INDEX);
		query.setParameter(1, newsId);
		BigDecimal bd = (BigDecimal) query.getSingleResult();
		long row =  bd.longValue();
		int page = (int) (Math.ceil((double)row/5));
		em.close();
		return page;
	}

	@Override
	public int findSearchPageIndex(SearchCriteriaObject sco, long newsId) throws DAOException {
		EntityManager em = emf.createEntityManager();
		StringBuilder builder;
		boolean addTags = false;
		int tagCount = sco.getTags().size();
		if(tagCount == 0 && sco.getAuthorId() == 0){
			return findPageIndex(newsId);
		}else if(tagCount == 0){
			builder  = new StringBuilder(SQL_FIND_NO_TAG_SEARCH_ROW_INDEX);
		}else if(sco.getAuthorId() == 0){
			builder = new StringBuilder(SQL_FIND_NO_AUTHOR_SEARCH_ROW_INDEX);
			addTags = true;
		}else{
			builder  = new StringBuilder(SQL_FIND_SEARCH_ROW_INDEX);
			addTags = true;
		}
		if(addTags){
			for(int i=0; i<tagCount; i++){
				if(i>0){
					builder.append("or ");
				}
				builder.append("nt_tag_id="+sco.getTags().get(i)+" ");
			}
			builder.append("))="+tagCount);
			builder.append(SQL_FIND_SEARCH_ROW_INDEX_ENDING);
		}
		Query query = em.createNativeQuery(builder.toString());
		if(sco.getAuthorId() == 0){
			query.setParameter(1, newsId);
		}else{
			query.setParameter(1, sco.getAuthorId());
			query.setParameter(2, newsId);
		}
		BigDecimal bd = (BigDecimal) query.getSingleResult();
		long row =  bd.longValue();
		int page = (int) (Math.ceil((double)row/5));
		em.close();
		return page;
	}

}
