package com.epam.newsmanagement.dao.eclipselink;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;

public class EclipselinkAuthorDAOImpl implements AuthorDAO {

	private static final String JPQL_SELECT_ALL = "SELECT A FROM Author A";
	private static final String JPQL_SELECT_NOT_EXPIRED = "SELECT A FROM Author A WHERE A.expired is null";
	private static final String JPQL_SELECT_NEWS_AUTHOR = "SELECT N.authors"
			+ " FROM News N WHERE N.newsId=:news_id";
	
	@Autowired
	private EntityManagerFactory emf;
	@PersistenceContext
	private EntityManager entityManager;
	
	public EclipselinkAuthorDAOImpl() {
		
	}

	@Override
	public Long create(Author value) throws DAOException {
		entityManager.persist(value);
		Long authorId = (Long) emf.getPersistenceUnitUtil().getIdentifier(value);
		return authorId;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Author> readAll() throws DAOException {
		EntityManager em = emf.createEntityManager();
		Query query =  em.createQuery(JPQL_SELECT_ALL);
		query.setHint("eclipselink.result-collection-type", java.util.ArrayList.class);
		List<Author> list = query.getResultList();
		em.close();
		return list;
	}

	@Override
	public Author read(long id) throws DAOException {
		EntityManager em = emf.createEntityManager();
		Author author =em.find(Author.class, id);
		em.close();
		return author;
	}

	@Override
	public void update(Author value, long id) throws DAOException {
		Author author =entityManager.find(Author.class, id);
		author.setAuthorName(value.getAuthorName());
		author.setExpired(value.getExpired());
	}

	@Override
	public void delete(long id) throws DAOException {
		Author author =entityManager.find(Author.class, id);
		entityManager.remove(author);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Author> readNotExpired() throws DAOException {
		EntityManager em = emf.createEntityManager();
		Query query =  em.createQuery(JPQL_SELECT_NOT_EXPIRED);
		query.setHint("eclipselink.result-collection-type", java.util.ArrayList.class);
		List<Author> list = query.getResultList();
		em.close();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Author readNewsAuthor(long newsId) throws DAOException {
		EntityManager em = emf.createEntityManager();
		Query query = entityManager.createQuery(JPQL_SELECT_NEWS_AUTHOR);
		query.setParameter("news_id", newsId);
		List<Author> list = query.getResultList();
		Author author = null;
		if(!list.isEmpty()){
			author = list.get(0);
		}
		em.close();
		return author;
	}

}
