package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Interface for accessing Tags table
 * @author Bahdan_Shpakouski
 * 
 */
public interface TagDAO extends GenericDAO<Tag>{
	
	/**
	 * Reads tags for the news with id passed
	 * @param newsId id of the news
	 * @return list of tags found
	 * @throws DAOException
	 */
	List<Tag> readNewsTags(long newsId) throws DAOException;
	
	/**
	 * Removes tag from all news
	 * @param tagId id of the tag
	 * @throws DAOException
	 */
	void removeTagFromAllNews(long tagId) throws DAOException;
}
