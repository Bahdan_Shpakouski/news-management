package com.epam.newsmanagement.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.dataobject.SearchCriteriaObject;
import com.epam.newsmanagement.exception.DAOException;

public class NewsDAOImpl implements NewsDAO {

	static Logger logger = Logger.getLogger(NewsDAOImpl.class);
	private static final String SQL_SELECT_ALL = "select NWS_NEWS_ID, NWS_TITLE,"
			+ " NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE,"
			+ " NWS_MODIFICATION_DATE from news order by NWS_MODIFICATION_DATE desc";
	private static final String SQL_SELECT_SORTED = "select nws_news_id,"
			+ " nws_title, nws_short_text, nws_full_text, nws_creation_date, nws_modification_date, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count "
			+ "from news "
			+ "order by nws_modification_date desc, comment_count desc, "
			+ "nws_creation_date desc, nws_news_id";
	private static final String SQL_SELECT_INDEXES_SORTED = "select * from "
			+ "(select a.*, rownum rnum "
			+ "from "
			+ "(select nws_news_id, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count "
			+ "from news "
			+ "order by nws_modification_date desc, comment_count desc, "
			+ "nws_creation_date desc, nws_news_id)a "
			+ "where rownum<=?) where rnum>=?";
	private static final String SQL_SELECT_PAGE_SORTED = "select * from "
			+ "(select a.*, rownum rnum "
			+ "from "
			+ "(select nws_news_id, nws_title, nws_short_text,	nws_full_text, nws_creation_date,"
			+ " nws_modification_date, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count"
			+ " from news"
			+ " order by nws_modification_date desc, comment_count desc, "
			+ "nws_creation_date desc, nws_news_id)a"
			+ " where rownum<=?) where rnum>=?";
	private static final String SQL_COUNT = "select count(*) from news";
	private static final String SQL_SELECT = "select NWS_NEWS_ID, NWS_TITLE,"
			+ " NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE,"
			+ " NWS_MODIFICATION_DATE from news where nws_news_id=?";
	private static final String SQL_UPDATE = "update news set nws_title = ?,"
			+ "nws_short_text=?, nws_full_text=?,"
			+ " nws_modification_date=? where nws_news_id = ?";
	private static final String SQL_INSERT = "insert into news (nws_title,"
			+ " nws_short_text, nws_full_text, nws_creation_date,"
			+ " nws_modification_date) values (?, ?, ?, ?, ?)";
	private static final String SQL_DELETE = "delete from news where nws_news_id= ?";
	private static final String STATEMENT_ERROR_MESSAGE = "Error occurred "
			+ "while closing statement";
	private static final String SQL_ADD_AUTHOR = "insert into news_authors"
			+ " (na_news_id, na_author_id) values (?, ?)";
	private static final String SQL_REMOVE_AUTHOR = "delete from news_authors"
			+ " where na_news_id = ? and na_author_id = ?";
	private static final String SQL_REMOVE_AUTHORS = "delete from news_authors"
			+ " where na_news_id = ?";
	private static final String SQL_ADD_TAG = "insert into news_tags"
			+ " (nt_news_id, nt_tag_id) values (?, ?)";
	private static final String SQL_REMOVE_TAG = "delete from news_tags"
			+ " where nt_news_id = ? and nt_tag_id = ?";
	private static final String SQL_REMOVE_TAGS = "delete from news_tags"
			+ " where nt_news_id = ?";
	private static final String SQL_SEARCH_BEGINNING = "select nws_news_id,"
			+ " nws_title, nws_short_text, nws_full_text, nws_creation_date, nws_modification_date, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count"
			+ " from news "
			+ "inner join news_authors on na_news_id=nws_news_id"
			+ " where na_author_id=? and "
			+ "(select count(*) from news_tags where nt_news_id=nws_news_id "
			+ "and (";
	private static final String SQL_NO_AUTHOR_SEARCH_BEGINNING = "select nws_news_id,"
			+ " nws_title, nws_short_text, nws_full_text, nws_creation_date, nws_modification_date, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count"
			+ " from news "
			+ " where (select count(*) from news_tags"
			+ " where nt_news_id=nws_news_id "
			+ "and (";
	private static final String SQL_SEARCH_ENDING = " order by "
			+ "nws_modification_date desc, comment_count desc,"
			+ " nws_creation_date desc, nws_news_id";
	private static final String SQL_NO_TAG_SEARCH = "select nws_news_id, nws_title,"
			+ " nws_short_text, nws_full_text, nws_creation_date,"
			+ " nws_modification_date, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count"
			+ " from news "
			+ "inner join news_authors on na_news_id=nws_news_id"
			+ " where na_author_id=? "
			+ "order by nws_modification_date desc, comment_count desc, "
			+ "nws_creation_date desc, nws_news_id";
	private static final String SQL_PAGE_SEARCH_BEGINNING = "select * from "
			+ "(select a.*, rownum rnum "
			+ "from "
			+ "(select nws_news_id, nws_title, nws_short_text, nws_full_text,"
			+ " nws_creation_date, nws_modification_date, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count "
			+ "from news "
			+ "inner join news_authors on na_news_id=nws_news_id "
			+ "where na_author_id=? and "
			+ "(select count(*) from news_tags where nt_news_id=nws_news_id "
			+ "and (";
	private static final String SQL_NO_AUTHOR_PAGE_SEARCH_BEGINNING = "select * from "
			+ "(select a.*, rownum rnum "
			+ "from "
			+ "(select nws_news_id, nws_title, nws_short_text, "
			+ "nws_full_text, nws_creation_date, nws_modification_date, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count "
			+ "from news "
			+ " where "
			+ "(select count(*) from news_tags "
			+ "where nt_news_id=nws_news_id "
			+ "and (";
	private static final String SQL_PAGE_SEARCH_ENDING = " order by"
			+ " nws_modification_date desc, comment_count desc,"
			+ " nws_creation_date desc, nws_news_id)a where rownum<=?) where rnum>=?";
	private static final String SQL_NO_TAG_PAGE_SEARCH = "select * from "
			+ "(select a.*, rownum rnum "
			+ "from "
			+ "(select nws_news_id, nws_title, nws_short_text, nws_full_text,"
			+ " nws_creation_date, nws_modification_date,"
			+ " (select count(*) from comments where cmt_news_id = nws_news_id) as comment_count "
			+ "from news "
			+ "inner join news_authors on na_news_id=nws_news_id "
			+ "where na_author_id=? "
			+ "order by nws_modification_date desc, comment_count desc, "
			+ "nws_creation_date desc, nws_news_id)a "
			+ "where rownum<=?) where rnum>=?";
	private static final String SQL_SEARCH_COUNT_BEGINNING = "select count(*)"
			+ " from news "
			+ "inner join news_authors on na_news_id=nws_news_id "
			+ "where na_author_id=? and "
			+ "(select count(*) from news_tags "
			+ "where nt_news_id=nws_news_id and (";
	private static final String SQL_NO_AUTHOR_SEARCH_COUNT_BEGINNING = "select count(*)"
			+ " from news where (select count(*) from news_tags "
			+ "where nt_news_id=nws_news_id and (";
	private static final String SQL_NO_TAG_SEARCH_COUNT = "select count(*) from "
			+ " news inner join news_authors on na_news_id=nws_news_id where na_author_id=?";
	private static final String SQL_NO_TAG_SEARCH_INDEXES = "select * from "
			+ "(select a.*, rownum rnum "
			+ "from "
			+ "(select nws_news_id, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count "
			+ "from news "
			+ "inner join news_authors on na_news_id=nws_news_id "
			+ "where na_author_id=? "
			+ "order by nws_modification_date desc,"
			+ " comment_count desc, nws_creation_date desc, nws_news_id)a "
			+ "where rownum<=?) where rnum>=?";
	private static final String SQL_SEARCH_BEGINNING_INDEXES = "select * from "
			+ "(select a.*, rownum rnum "
			+ "from "
			+ "(select nws_news_id, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count"
			+ " from news "
			+ "inner join news_authors on na_news_id=nws_news_id "
			+ "where na_author_id=? and "
			+ "(select count(*) from news_tags where nt_news_id=nws_news_id "
			+ "and (";
	private static final String SQL_NO_AUTHOR_SEARCH_BEGINNING_INDEXES = "select * from "
			+ "(select a.*, rownum rnum "
			+ "from "
			+ "(select nws_news_id, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count "
			+ "from news "
			+ "where (select count(*) from news_tags"
			+ " where nt_news_id=nws_news_id "
			+ "and (";
	private static final String SQL_FIND_ROW_INDEX = "select r from "
			+ "(select rownum r, nws_news_id "
			+ "from "
			+ "(select nws_news_id, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count "
			+ "from news "
			+ "order by nws_modification_date desc,"
			+ " comment_count desc, nws_creation_date desc,"
			+ " nws_news_id)) "
			+ "where nws_news_id=?";
	private static final String SQL_FIND_SEARCH_ROW_INDEX = "select r from "
			+ "(select rownum r, nws_news_id "
			+ "from "
			+ "(select nws_news_id, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count "
			+ "from news "
			+ "inner join news_authors on na_news_id=nws_news_id"
			+ " where na_author_id=? "
			+ "and (select count(*) from news_tags where nt_news_id=nws_news_id "
			+ "and (";
	private static final String SQL_FIND_NO_AUTHOR_SEARCH_ROW_INDEX = "select r from "
			+ "(select rownum r, nws_news_id "
			+ "from "
			+ "(select nws_news_id, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count "
			+ "from news "
			+ "where (select count(*) from news_tags "
			+ "where nt_news_id=nws_news_id "
			+ "and (";
	private static final String SQL_FIND_NO_TAG_SEARCH_ROW_INDEX = "select r from "
			+ "(select rownum r, nws_news_id "
			+ "from "
			+ "(select nws_news_id, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count"
			+ " from news "
			+ "inner join news_authors on na_news_id=nws_news_id "
			+ "where na_author_id=? "
			+ "order by nws_modification_date desc, comment_count desc,"
			+ " nws_creation_date desc, nws_news_id)) "
			+ "where nws_news_id=?";
	private static final String SQL_FIND_SEARCH_ROW_INDEX_ENDING = "order by "
			+ "nws_modification_date desc, comment_count desc,"
			+ " nws_creation_date desc, nws_news_id)) where nws_news_id=?";
	
	@Autowired
	private DataSource dataSource;
	
	public NewsDAOImpl() {
		
	}

	@Override
	public Long create(News news) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		Long id = null;
		try{ 
			String generatedColumns[] = { "nws_news_id" };
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_INSERT, generatedColumns);
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setTimestamp(4, new Timestamp(new Date().getTime()));
			ps.setDate(5, new java.sql.Date(new Date().getTime()));
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if(rs.next()){
				id = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return id;
	}

	@Override
	public List<News> readAll() throws DAOException {
		List<News> list = new ArrayList<News>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_SELECT_ALL);
			rs = ps.executeQuery();
			while(rs.next()){
				News news = new News();
				news.setNewsId(rs.getLong(1));
				news.setTitle(rs.getString(2));
				news.setShortText(rs.getString(3));
				news.setFullText(rs.getString(4));
				news.setCreationDate(rs.getTimestamp(5));
				news.setModificationDate(rs.getDate(6));
				list.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return list;
	}

	@Override
	public News read(long id) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		News news = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_SELECT);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while(rs.next()){
				news = new News();
				news.setNewsId(rs.getLong(1));
				news.setTitle(rs.getString(2));
				news.setShortText(rs.getString(3));
				news.setFullText(rs.getString(4));
				news.setCreationDate(rs.getTimestamp(5));
				news.setModificationDate(rs.getDate(6));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return news;
	}

	@Override
	public void update(News news, long id) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_UPDATE);
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setDate(4, new java.sql.Date(new java.util.Date().getTime()));
			ps.setLong(5, news.getNewsId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
	}

	@Override
	public void delete(long id) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_DELETE);
			ps.setLong(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
	}

	@Override
	public void addAuthor(long newsId, long authorId) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_ADD_AUTHOR);
			ps.setLong(1, newsId);
			ps.setLong(2, authorId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
	}

	@Override
	public void removeAuthor(long newsId, long authorId) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_REMOVE_AUTHOR);
			ps.setLong(1, newsId);
			ps.setLong(2, authorId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
	}

	@Override
	public void addTag(long newsId, long tagId) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_ADD_TAG);
			ps.setLong(1, newsId);
			ps.setLong(2, tagId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
	}

	@Override
	public void removeTag(long newsId, long tagId) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_REMOVE_TAG);
			ps.setLong(1, newsId);
			ps.setLong(2, tagId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
	}

	@Override
	public long countNews() throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		long count = 0;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_COUNT);
			rs = ps.executeQuery();
			while(rs.next()){
				count = rs.getInt(1); 
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return count;
	}

	@Override
	public List<News> readSorted() throws DAOException {
		List<News> list = new ArrayList<News>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_SELECT_SORTED);
			rs = ps.executeQuery();
			while(rs.next()){
				News news = new News();
				news.setNewsId(rs.getLong(1));
				news.setTitle(rs.getString(2));
				news.setShortText(rs.getString(3));
				news.setFullText(rs.getString(4));
				news.setCreationDate(rs.getTimestamp(5));
				news.setModificationDate(rs.getDate(6));
				list.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return list;
	}
	
	@Override
	public List<News> readSorted(int page) throws DAOException {
		List<News> list = new ArrayList<News>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_SELECT_PAGE_SORTED);
			ps.setLong(1, page*5);
			ps.setLong(2, (page-1)*5+1);
			rs = ps.executeQuery();
			while(rs.next()){
				News news = new News();
				news.setNewsId(rs.getLong(1));
				news.setTitle(rs.getString(2));
				news.setShortText(rs.getString(3));
				news.setFullText(rs.getString(4));
				news.setCreationDate(rs.getTimestamp(5));
				news.setModificationDate(rs.getDate(6));
				list.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return list;
	}

	@Override
	public void removeAuthors(long newsId) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_REMOVE_AUTHORS);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
	}

	@Override
	public void removeTags(long newsId) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_REMOVE_TAGS);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
	}

	@Override
	public List<News> searchNews(SearchCriteriaObject sco) throws DAOException {
		List<News> list = new ArrayList<News>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean addTags = false;
		StringBuilder builder;
		int tagCount = sco.getTags().size();
		if(tagCount == 0 && sco.getAuthorId() == 0){
			return readSorted();
		}else if(tagCount == 0){
			builder  = new StringBuilder(SQL_NO_TAG_SEARCH);
		}else if(sco.getAuthorId() == 0){
			builder = new StringBuilder(SQL_NO_AUTHOR_SEARCH_BEGINNING);
			addTags = true;
		}else{
			builder  = new StringBuilder(SQL_SEARCH_BEGINNING);
			addTags = true;
		}
		if(addTags){
			for(int i=0; i<tagCount; i++){
				if(i>0){
					builder.append("or ");
				}
				builder.append("nt_tag_id="+sco.getTags().get(i)+" ");
			}
			builder.append("))="+tagCount);
			builder.append(SQL_SEARCH_ENDING);
		}
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(builder.toString());
			if(sco.getAuthorId() != 0){
				ps.setLong(1, sco.getAuthorId());
			}
			rs = ps.executeQuery();
			while(rs.next()){
				News news = new News();
				news.setNewsId(rs.getLong(1));
				news.setTitle(rs.getString(2));
				news.setShortText(rs.getString(3));
				news.setFullText(rs.getString(4));
				news.setCreationDate(rs.getTimestamp(5));
				news.setModificationDate(rs.getDate(6));
				list.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return list;
	}
	
	@Override
	public List<News> searchNews(SearchCriteriaObject sco, int page) throws DAOException {
		List<News> list = new ArrayList<News>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder builder;
		boolean addTags = false;
		int tagCount = sco.getTags().size();
		if(tagCount == 0 && sco.getAuthorId() == 0){
			return readSorted(page);
		}else if(tagCount == 0){
			builder  = new StringBuilder(SQL_NO_TAG_PAGE_SEARCH);
		}else if(sco.getAuthorId() == 0){
			builder = new StringBuilder(SQL_NO_AUTHOR_PAGE_SEARCH_BEGINNING);
			addTags = true;
		}else{
			builder  = new StringBuilder(SQL_PAGE_SEARCH_BEGINNING);
			addTags = true;
		}
		if(addTags){
			for(int i=0; i<tagCount; i++){
				if(i>0){
					builder.append("or ");
				}
				builder.append("nt_tag_id="+sco.getTags().get(i)+" ");
			}
			builder.append("))="+tagCount);
			builder.append(SQL_PAGE_SEARCH_ENDING);
		}
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(builder.toString());
			if(sco.getAuthorId() == 0){
				ps.setLong(1, page*5);
				ps.setLong(2, (page-1)*5+1);
			}else {
				ps.setLong(1, sco.getAuthorId());
				ps.setLong(2, page*5);
				ps.setLong(3, (page-1)*5+1);
			}
			rs = ps.executeQuery();
			while(rs.next()){
				News news = new News();
				news.setNewsId(rs.getLong(1));
				news.setTitle(rs.getString(2));
				news.setShortText(rs.getString(3));
				news.setFullText(rs.getString(4));
				news.setCreationDate(rs.getTimestamp(5));
				news.setModificationDate(rs.getDate(6));
				list.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return list;
	}

	@Override
	public List<Long> readIndexesSorted(int page) throws DAOException {
		List<Long> list = new ArrayList<Long>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_SELECT_INDEXES_SORTED);
			ps.setLong(1, page*5);
			ps.setLong(2, (page-1)*5+1);
			rs = ps.executeQuery();
			while(rs.next()){
				list.add(rs.getLong(1));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return list;
	}

	public long countSearchResult(SearchCriteriaObject sco) throws DAOException{
		long count = 0;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder builder;
		boolean addTags = false;
		int tagCount = sco.getTags().size();
		if(tagCount == 0 && sco.getAuthorId() == 0){
			return countNews();
		}else if(tagCount == 0){
			builder  = new StringBuilder(SQL_NO_TAG_SEARCH_COUNT);
		}else if(sco.getAuthorId() == 0){
			builder = new StringBuilder(SQL_NO_AUTHOR_SEARCH_COUNT_BEGINNING);
			addTags = true;
		}else{
			builder  = new StringBuilder(SQL_SEARCH_COUNT_BEGINNING);
			addTags = true;
		}
		if(addTags){
			for(int i=0; i<tagCount; i++){
				if(i>0){
					builder.append("or ");
				}
				builder.append("nt_tag_id="+sco.getTags().get(i)+" ");
			}
			builder.append("))="+tagCount);
		}
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(builder.toString());
			if(sco.getAuthorId() != 0){
				ps.setLong(1, sco.getAuthorId());
			}
			rs = ps.executeQuery();
			while(rs.next()){
				count = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return count;
	}
	
	public List<Long> readSearchIndexes(SearchCriteriaObject sco,
			int page) throws DAOException{
		List<Long> list = new ArrayList<Long>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder builder;
		boolean addTags = false;
		int tagCount = sco.getTags().size();
		if(tagCount == 0 && sco.getAuthorId() == 0){
			return readIndexesSorted(page);
		}else if(tagCount == 0){
			builder  = new StringBuilder(SQL_NO_TAG_SEARCH_INDEXES);
		}else if(sco.getAuthorId() == 0){
			builder = new StringBuilder(SQL_NO_AUTHOR_SEARCH_BEGINNING_INDEXES);
			addTags = true;
		}else{
			builder  = new StringBuilder(SQL_SEARCH_BEGINNING_INDEXES);
			addTags = true;
		}
		if(addTags){
			for(int i=0; i<tagCount; i++){
				if(i>0){
					builder.append("or ");
				}
				builder.append("nt_tag_id="+sco.getTags().get(i)+" ");
			}
			builder.append("))="+tagCount);
			builder.append(SQL_PAGE_SEARCH_ENDING);
		}
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(builder.toString());
			if(sco.getAuthorId() == 0){
				ps.setLong(1, page*5);
				ps.setLong(2, (page-1)*5+1);
			}else {
				ps.setLong(1, sco.getAuthorId());
				ps.setLong(2, page*5);
				ps.setLong(3, (page-1)*5+1);
			}
			rs = ps.executeQuery();
			while(rs.next()){
				list.add(rs.getLong(1));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return list;
	}
	
	public int findPageIndex(long newsId) throws DAOException{
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int page = 0;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_FIND_ROW_INDEX);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			while(rs.next()){
				long row = rs.getLong(1);
				page = (int) (Math.ceil((double)row/5));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return page;
	}

	@Override
	public int findSearchPageIndex(SearchCriteriaObject sco, long newsId) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int page = 0;
		StringBuilder builder;
		boolean addTags = false;
		int tagCount = sco.getTags().size();
		if(tagCount == 0 && sco.getAuthorId() == 0){
			return findPageIndex(newsId);
		}else if(tagCount == 0){
			builder  = new StringBuilder(SQL_FIND_NO_TAG_SEARCH_ROW_INDEX);
		}else if(sco.getAuthorId() == 0){
			builder = new StringBuilder(SQL_FIND_NO_AUTHOR_SEARCH_ROW_INDEX);
			addTags = true;
		}else{
			builder  = new StringBuilder(SQL_FIND_SEARCH_ROW_INDEX);
			addTags = true;
		}
		if(addTags){
			for(int i=0; i<tagCount; i++){
				if(i>0){
					builder.append("or ");
				}
				builder.append("nt_tag_id="+sco.getTags().get(i)+" ");
			}
			builder.append("))="+tagCount);
			builder.append(SQL_FIND_SEARCH_ROW_INDEX_ENDING);
		}
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(builder.toString());
			if(sco.getAuthorId() == 0){
				ps.setLong(1, newsId);
			}else {
				ps.setLong(1, sco.getAuthorId());
				ps.setLong(2, newsId);
			}
			rs = ps.executeQuery();
			while(rs.next()){
				long row = rs.getLong(1);
				page = (int) (Math.ceil((double)row/5));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return page;
	}
}
