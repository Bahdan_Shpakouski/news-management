package com.epam.newsmanagement.dao.hibernate;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.DAOException;

public class HibernateCommentDAOImpl implements CommentDAO {

	private static final String HQL_SELECT_ALL = "FROM Comment";
	private static final String HQL_SELECT_NEWS_COMMENTS = "FROM Comment C "
			+ "WHERE C.news.newsId = :news_id order by C.creationDate";
	private static final String HQL_DELETE_NEWS_COMMENTS = "DELETE FROM Comment C"
			+ " WHERE C.news.newsId = :news_id";
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Long create(Comment comment) throws DAOException {
		Long commentId = null;
		try{
			Session session = sessionFactory.getCurrentSession();
			comment.setNews(new News());
			comment.getNews().setNewsId(comment.getNewsId());
			comment.setCreationDate(new Timestamp(new Date().getTime()));
			commentId = (Long) session.save(comment);
		}catch(HibernateException e){
			throw new DAOException(e);
		}
		return commentId;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> readAll() throws DAOException {
		List<Comment> comments = null;
		Session session = sessionFactory.openSession();
		try{
			Query query = session.createQuery(HQL_SELECT_ALL);
			comments = query.list();
		}catch(HibernateException e){
			throw new DAOException(e);
		}finally{
			session.close();
		}
		for(Comment comment : comments){
			comment.setNewsId(comment.getNews().getNewsId());
		}
		return comments;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> readNewsComments(long newsId) throws DAOException {
		List<Comment> comments = null;
		Session session = sessionFactory.openSession();
		try{
			Query query = session.createQuery(HQL_SELECT_NEWS_COMMENTS);
			query.setParameter("news_id", newsId);
			comments = query.list();
		}catch(HibernateException e){
			throw new DAOException(e);
		}finally{
			session.close();
		}
		for(Comment comment : comments){
			comment.setNewsId(comment.getNews().getNewsId());
		}
		return comments;
	}

	@Override
	public Comment read(long commentId) throws DAOException {
		Session session = sessionFactory.openSession();
		Comment comment = null;
		try{
			comment = session.get(Comment.class, commentId);
		}catch(HibernateException e){
			throw new DAOException(e);
		}finally{
			session.close();
		}
		if(comment!=null){
			comment.setNewsId(comment.getNews().getNewsId());
		}
		return comment;
	}

	@Override
	public void update(Comment comment, long commentId) throws DAOException {
		try{
			Session session = sessionFactory.getCurrentSession();
			Comment oldComment = session.get(Comment.class, commentId);
			oldComment.setCommentText(comment.getCommentText());
			oldComment.setCreationDate(new Timestamp(new Date().getTime()));
			session.save(oldComment);
		}catch(HibernateException e){
			throw new DAOException(e);
		}
	}
	
	@Override
	public void delete(long commentId) throws DAOException {
		try{
			Session session = sessionFactory.getCurrentSession();
			Comment comment = session.get(Comment.class, commentId);
			session.delete(comment);
		}catch(HibernateException e){
			throw new DAOException(e);
		}
	}

	@Override
	public void deleteNewsComments(long newsId) throws DAOException {
		try{
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery(HQL_DELETE_NEWS_COMMENTS);
			query.setParameter("news_id", newsId);
			query.executeUpdate();
		}catch(HibernateException e){
			throw new DAOException(e);
		}
	}

}
