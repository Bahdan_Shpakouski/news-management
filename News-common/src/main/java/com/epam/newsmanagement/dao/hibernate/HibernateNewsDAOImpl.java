package com.epam.newsmanagement.dao.hibernate;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.entity.dataobject.SearchCriteriaObject;
import com.epam.newsmanagement.exception.DAOException;

public class HibernateNewsDAOImpl implements NewsDAO {

	private static final String HQL_SELECT_ALL = "FROM News N order by "
			+ "N.modificationDate desc";
	private static final String HQL_SELECT_SORTED = "select N, "
			+ "(select count(*) from Comment C where C.news.newsId = N.newsId) as commentCount "
			+ "FROM News N "
			+ "order by N.modificationDate desc, commentCount desc, "
			+ "N.creationDate desc, N.newsId";
	private static final String HQL_SELECT_INDEXES_SORTED = "select N.newsId,"
			+ " (select count(*) from Comment C where C.news.newsId = N.newsId) as commentCount"
			+ " FROM News N "
			+ "order by N.modificationDate desc, commentCount desc, "
			+ "N.creationDate desc, N.newsId";
	private static final String HQL_COUNT_NEWS = "select count(*) from News";
	private static final String HQL_SEARCH_BEGINNING = "select N, "
			+ "(select count(*) from Comment C where C.news.newsId = N.newsId) as commentCount"
			+ " from News N "
			+ "inner join N.authors A "
			+ "where A.authorId =:authorId"
			+ " and (select count(*) from N.tags T where ";
	private static final String HQL_NO_AUTHOR_SEARCH_BEGINNING = "select N, "
			+ "(select count(*) from Comment C where C.news.newsId = N.newsId) as commentCount"
			+ " from News N "
			+ "where"
			+ " (select count(*) from N.tags T where ";
	private static final String HQL_SEARCH_ENDING = " order by "
			+ "N.modificationDate desc, commentCount desc,"
			+ " N.creationDate desc, N.newsId";
	private static final String HQL_NO_TAG_SEARCH = "select N, "
			+ "(select count(*) from Comment C where C.news.newsId = N.newsId) as commentCount"
			+ " from News N "
			+ "inner join N.authors A "
			+ "where A.authorId =:authorId"
			+ " order by N.modificationDate desc, commentCount desc,"
			+ " N.creationDate desc, N.newsId";
	private static final String HQL_SEARCH_COUNT_BEGINNING = "select count(*)"
			+ "from News N "
			+ "inner join N.authors A "
			+ "where A.authorId =:authorId"
			+ " and (select count(*) from N.tags T where ";
	private static final String HQL_NO_AUTHOR_SEARCH_COUNT_BEGINNING = "select count(*)"
			+ "from News N where (select count(*) from N.tags T where ";
	private static final String HQL_NO_TAG_SEARCH_COUNT = "select count(*) "
			+ " from News N inner join N.authors A "
			+ "where A.authorId =:authorId";
	private static final String HQL_SEARCH_BEGINNING_INDEXES = "select N.newsId, "
			+ "(select count(*) from Comment C where C.news.newsId = N.newsId) as commentCount"
			+ " from News N "
			+ "inner join N.authors A "
			+ "where A.authorId =:authorId"
			+ " and (select count(*) from N.tags T where ";
	private static final String HQL_NO_AUTHOR_SEARCH_BEGINNING_INDEXES = "select N.newsId, "
			+ "(select count(*) from Comment C where C.news.newsId = N.newsId) as commentCount"
			+ " from News N "
			+ "where "
			+ "(select count(*) from N.tags T where ";
	private static final String HQL_NO_TAG_SEARCH_INDEXES = "select N.newsId, "
			+ "(select count(*) from Comment C where C.news.newsId = N.newsId) as commentCount"
			+ " from News N "
			+ "inner join N.authors A "
			+ "where A.authorId =:authorId"
			+ " order by N.modificationDate desc, commentCount desc, "
			+ "N.creationDate desc, N.newsId";
	private static final String SQL_FIND_ROW_INDEX = "select r from "
			+ "(select rownum r, nws_news_id "
			+ "from "
			+ "(select nws_news_id, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count "
			+ "from news "
			+ "order by nws_modification_date desc,"
			+ " comment_count desc, nws_creation_date desc, nws_news_id)) "
			+ "where nws_news_id=?";
	private static final String SQL_FIND_SEARCH_ROW_INDEX = "select r from "
			+ "(select rownum r, nws_news_id "
			+ "from "
			+ "(select nws_news_id, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count "
			+ "from news "
			+ "inner join news_authors "
			+ "on na_news_id=nws_news_id "
			+ "where na_author_id=? "
			+ "and (select count(*) from news_tags where nt_news_id=nws_news_id "
			+ "and (";
	private static final String SQL_FIND_NO_AUTHOR_SEARCH_ROW_INDEX = "select r from "
			+ "(select rownum r, nws_news_id "
			+ "from "
			+ "(select nws_news_id, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count "
			+ "from news "
			+ "where "
			+ "(select count(*) from news_tags "
			+ "where nt_news_id=nws_news_id "
			+ "and (";
	private static final String SQL_FIND_NO_TAG_SEARCH_ROW_INDEX = "select r from "
			+ "(select rownum r, nws_news_id "
			+ "from "
			+ "(select nws_news_id, "
			+ "(select count(*) from comments where cmt_news_id = nws_news_id) as comment_count "
			+ "from news "
			+ "inner join news_authors "
			+ "on na_news_id=nws_news_id "
			+ "where na_author_id=? "
			+ "order by nws_modification_date desc, comment_count desc,"
			+ " nws_creation_date desc, nws_news_id)) "
			+ "where nws_news_id=?";
	private static final String SQL_FIND_SEARCH_ROW_INDEX_ENDING = "order by "
			+ "nws_modification_date desc, comment_count desc,"
			+ " nws_creation_date desc, nws_news_id)) where nws_news_id=?";
			
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Long create(News news) throws DAOException {
		Long newsId = null;
		try{
			Session session = sessionFactory.getCurrentSession();
			news.setCreationDate(new Timestamp(new Date().getTime()));
			news.setModificationDate(new Timestamp(new Date().getTime()));
			newsId = (Long) session.save(news);
		}catch(HibernateException e){
			throw new DAOException(e);
		}
		return newsId;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> readAll() throws DAOException {
		Session session = sessionFactory.openSession();
		Query query = session.createQuery(HQL_SELECT_ALL);
		List<News> news = null;
		try{
			news = query.list();
		}catch(HibernateException e){
			throw new DAOException(e);
		}finally{
			session.close();
		}
		return news;
	}

	@Override
	public News read(long newsId) throws DAOException {
		Session session = sessionFactory.openSession();
		News news = null;
		try{
			news = session.get(News.class, newsId);
		}catch(HibernateException e){
			throw new DAOException(e);
		}finally{
			session.close();
		}
		return news;
	}

	@Override
	public void update(News news, long newsId) throws DAOException {
		try{
			Session session = sessionFactory.getCurrentSession();
			News oldNews = session.get(News.class, newsId);
			oldNews.setNewsId(newsId);
			oldNews.setTitle(news.getTitle());
			oldNews.setShortText(news.getShortText());
			oldNews.setFullText(news.getFullText());
			oldNews.setModificationDate(new Timestamp(new Date().getTime()));
			session.save(oldNews);
		}catch(HibernateException e){
			throw new DAOException(e);
		}
	}

	@Override
	public void delete(long newsId) throws DAOException {
		try{
			Session session = sessionFactory.getCurrentSession();
			News news = session.get(News.class, newsId);
			session.delete(news);
		}catch(HibernateException e){
			throw new DAOException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> readSorted() throws DAOException {
		Session session = sessionFactory.openSession();
		Query query = session.createQuery(HQL_SELECT_SORTED);
		List<News> news = new ArrayList<News>();
		try{
			List<Object[]> results = query.list();
			for(Object ob[] : results){
				news.add((News)ob[0]);
			}
		}catch(HibernateException e){
			throw new DAOException(e);
		}finally{
			session.close();
		}
		return news;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> readSorted(int page) throws DAOException {
		Session session = sessionFactory.openSession();
		Query query = session.createQuery(HQL_SELECT_SORTED);
		query.setFirstResult((page-1)*5);
		query.setMaxResults(5);
		List<News> news = new ArrayList<News>();
		try{
			List<Object[]> results = query.list();
			for(Object ob[] : results){
				news.add((News)ob[0]);
			}
		}catch(HibernateException e){
			throw new DAOException(e);
		}finally{
			session.close();
		}
		return news;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Long> readIndexesSorted(int page) throws DAOException {
		Session session = sessionFactory.openSession();
		Query query = session.createQuery(HQL_SELECT_INDEXES_SORTED);
		query.setFirstResult((page-1)*5);
		query.setMaxResults(5);
		List<Long> indexes = new ArrayList<Long>();
		try{
			List<Object[]> results = query.list();
			for(Object ob[] : results){
				indexes.add((Long)ob[0]);
			}
		}catch(HibernateException e){
			throw new DAOException(e);
		}finally{
			session.close();
		}
		return indexes;
	}

	@Override
	public long countNews() throws DAOException {
		Session session = sessionFactory.openSession();
		long count = 0l;
		try{
			count = (long) session.createQuery(HQL_COUNT_NEWS).uniqueResult();
		}catch(HibernateException e){
			throw new DAOException(e);
		}finally{
			session.close();
		}
		return count;
	}

	@Override
	public void addAuthor(long newsId, long authorId) throws DAOException {
		try{
			Session session = sessionFactory.getCurrentSession();
			News oldNews = session.get(News.class, newsId);
			Author author = session.get(Author.class, authorId);
			oldNews.getAuthors().add(author);
			session.update(oldNews);
		}catch(HibernateException e){
			throw new DAOException(e);
		}
	}

	@Override
	public void removeAuthor(long newsId, long authorId) throws DAOException {
		try{
			Session session = sessionFactory.getCurrentSession();
			News oldNews = session.get(News.class, newsId);
			Author author = session.get(Author.class, authorId);
			oldNews.getAuthors().remove(author);
			session.update(oldNews);
		}catch(HibernateException e){
			throw new DAOException(e);
		}
	}

	@Override
	public void removeAuthors(long newsId) throws DAOException {
		try{
			Session session = sessionFactory.getCurrentSession();
			News oldNews = session.get(News.class, newsId);
			oldNews.getAuthors().clear();
			session.update(oldNews);
		}catch(HibernateException e){
			throw new DAOException(e);
		}
	}

	@Override
	public void addTag(long newsId, long tagId) throws DAOException {
		try{
			Session session = sessionFactory.getCurrentSession();
			News oldNews = session.get(News.class, newsId);
			Tag tag = session.get(Tag.class, tagId);
			oldNews.getTags().add(tag);
			session.update(oldNews);
		}catch(HibernateException e){
			throw new DAOException(e);
		}
	}

	@Override
	public void removeTag(long newsId, long tagId) throws DAOException {
		try{
			Session session = sessionFactory.getCurrentSession();
			News oldNews = session.get(News.class, newsId);
			Tag tag = session.get(Tag.class, tagId);
			oldNews.getTags().remove(tag);
			session.update(oldNews);
		}catch(HibernateException e){
			throw new DAOException(e);
		}
	}

	@Override
	public void removeTags(long newsId) throws DAOException {
		try{
			Session session = sessionFactory.getCurrentSession();
			News oldNews = session.get(News.class, newsId);
			oldNews.getTags().clear();
			session.update(oldNews);
		}catch(HibernateException e){
			throw new DAOException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> searchNews(SearchCriteriaObject sco) throws DAOException {
		List<News> list = new ArrayList<News>();
		boolean addTags = false;
		StringBuilder builder;
		int tagCount = sco.getTags().size();
		if(tagCount == 0 && sco.getAuthorId() == 0){
			return readSorted();
		}else if(tagCount == 0){
			builder  = new StringBuilder(HQL_NO_TAG_SEARCH);
		}else if(sco.getAuthorId() == 0){
			builder = new StringBuilder(HQL_NO_AUTHOR_SEARCH_BEGINNING);
			addTags = true;
		}else{
			builder  = new StringBuilder(HQL_SEARCH_BEGINNING);
			addTags = true;
		}
		if(addTags){
			for(int i=0; i<tagCount; i++){
				if(i>0){
					builder.append("or ");
				}
				builder.append("T.tagId="+sco.getTags().get(i)+" ");
			}
			builder.append(")="+tagCount);
			builder.append(HQL_SEARCH_ENDING);
		}
		Session session = sessionFactory.openSession();
		try{
			Query query = session.createQuery(builder.toString());
			if(sco.getAuthorId() !=0){
				query.setParameter("authorId", sco.getAuthorId());
			}
			List<Object[]> results = query.list();
			for(Object ob[] : results){
				list.add((News)ob[0]);
			}
		} catch (HibernateException e) {
			throw new DAOException(e);
		}finally{
				session.close();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> searchNews(SearchCriteriaObject sco, int page) throws DAOException {
		List<News> list = new ArrayList<News>();
		boolean addTags = false;
		StringBuilder builder;
		int tagCount = sco.getTags().size();
		if(tagCount == 0 && sco.getAuthorId() == 0){
			return readSorted(page);
		}else if(tagCount == 0){
			builder  = new StringBuilder(HQL_NO_TAG_SEARCH);
		}else if(sco.getAuthorId() == 0){
			builder = new StringBuilder(HQL_NO_AUTHOR_SEARCH_BEGINNING);
			addTags = true;
		}else{
			builder  = new StringBuilder(HQL_SEARCH_BEGINNING);
			addTags = true;
		}
		if(addTags){
			for(int i=0; i<tagCount; i++){
				if(i>0){
					builder.append("or ");
				}
				builder.append("T.tagId="+sco.getTags().get(i)+" ");
			}
			builder.append(")="+tagCount);
			builder.append(HQL_SEARCH_ENDING);
		}
		Session session = sessionFactory.openSession();
		try{ 
			Query query = session.createQuery(builder.toString());
			if(sco.getAuthorId() != 0){
				query.setParameter("authorId", sco.getAuthorId());
			}
			query.setFirstResult((page-1)*5);
			query.setMaxResults(5);
			List<Object[]> results = query.list();
			for(Object ob[] : results){
				list.add((News)ob[0]);
			}
		} catch (HibernateException e) {
			throw new DAOException(e);
		}finally{
				session.close();
		}
		return list;
	}

	@Override
	public long countSearchResult(SearchCriteriaObject sco) throws DAOException {
		long count = 0l;
		boolean addTags = false;
		StringBuilder builder;
		int tagCount = sco.getTags().size();
		if(tagCount == 0 && sco.getAuthorId() == 0){
			return countNews();
		}else if(tagCount == 0){
			builder  = new StringBuilder(HQL_NO_TAG_SEARCH_COUNT);
		}else if(sco.getAuthorId() == 0){
			builder = new StringBuilder(HQL_NO_AUTHOR_SEARCH_COUNT_BEGINNING);
			addTags = true;
		}else{
			builder  = new StringBuilder(HQL_SEARCH_COUNT_BEGINNING);
			addTags = true;
		}
		if(addTags){
			for(int i=0; i<tagCount; i++){
				if(i>0){
					builder.append("or ");
				}
				builder.append("T.tagId="+sco.getTags().get(i)+" ");
			}
			builder.append(")="+tagCount +")");
		}
		Session session = sessionFactory.openSession();
		try{
			Query query = session.createQuery(builder.toString());
			if(sco.getAuthorId() != 0){
				query.setParameter("authorId", sco.getAuthorId());
			}
			count = (long) query.uniqueResult();
		}catch(HibernateException e){
			throw new DAOException(e);
		}finally{
			session.close();
		}
		return count;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Long> readSearchIndexes(SearchCriteriaObject sco, int page) throws DAOException {
		List<Long> list = new ArrayList<Long>();
		boolean addTags = false;
		StringBuilder builder;
		int tagCount = sco.getTags().size();
		if(tagCount == 0 && sco.getAuthorId() == 0){
			return readIndexesSorted(page);
		}else if(tagCount == 0){
			builder  = new StringBuilder(HQL_NO_TAG_SEARCH_INDEXES);
		}else if(sco.getAuthorId() == 0){
			builder = new StringBuilder(HQL_NO_AUTHOR_SEARCH_BEGINNING_INDEXES);
			addTags = true;
		}else{
			builder  = new StringBuilder(HQL_SEARCH_BEGINNING_INDEXES);
			addTags = true;
		}
		if(addTags){
			for(int i=0; i<tagCount; i++){
				if(i>0){
					builder.append("or ");
				}
				builder.append("T.tagId="+sco.getTags().get(i)+" ");
			}
			builder.append(")="+tagCount);
			builder.append(HQL_SEARCH_ENDING);
		}
		Session session = sessionFactory.openSession();
		try{ 
			Query query = session.createQuery(builder.toString());
			if(sco.getAuthorId() != 0){
				query.setParameter("authorId", sco.getAuthorId());
			}
			query.setFirstResult((page-1)*5);
			query.setMaxResults(5);
			List<Object[]> results = query.list();
			for(Object ob[] : results){
				list.add((Long)ob[0]);
			}
		} catch (HibernateException e) {
			throw new DAOException(e);
		}finally{
				session.close();
		}
		return list;
	}

	@Override
	public int findPageIndex(long newsId) throws DAOException {
		Transaction tx = null;
		Session session = sessionFactory.openSession();
		long page = 0;
		Query query = session.createSQLQuery(SQL_FIND_ROW_INDEX);
		query.setLong(0, newsId);
		try{
			tx = session.beginTransaction();
			BigDecimal bd = (BigDecimal) query.uniqueResult();
			long row =  bd.longValue();
			page = (int) (Math.ceil((double)row/5));
			tx.commit();
		}catch(HibernateException e){
			throw new DAOException(e);
		}finally{
			session.close();
		}
		return (int)page;
	}

	@Override
	public int findSearchPageIndex(SearchCriteriaObject sco, long newsId) throws DAOException {
		Session session = sessionFactory.openSession();
		long page = 0;
		StringBuilder builder;
		boolean addTags = false;
		int tagCount = sco.getTags().size();
		if(tagCount == 0 && sco.getAuthorId() == 0){
			return findPageIndex(newsId);
		}else if(tagCount == 0){
			builder  = new StringBuilder(SQL_FIND_NO_TAG_SEARCH_ROW_INDEX);
		}else if(sco.getAuthorId() == 0){
			builder = new StringBuilder(SQL_FIND_NO_AUTHOR_SEARCH_ROW_INDEX);
			addTags = true;
		}else{
			builder  = new StringBuilder(SQL_FIND_SEARCH_ROW_INDEX);
			addTags = true;
		}
		if(addTags){
			for(int i=0; i<tagCount; i++){
				if(i>0){
					builder.append("or ");
				}
				builder.append("nt_tag_id="+sco.getTags().get(i)+" ");
			}
			builder.append("))="+tagCount);
			builder.append(SQL_FIND_SEARCH_ROW_INDEX_ENDING);
		}
		Query query = session.createSQLQuery(builder.toString());
		if(sco.getAuthorId() == 0){
			query.setLong(0, newsId);
		}else{
			query.setLong(0, sco.getAuthorId());
			query.setLong(1, newsId);
		}
		try{
			BigDecimal bd = (BigDecimal) query.uniqueResult();
			long row =  bd.longValue();
			page = (int) (Math.ceil((double)row/5));
		}catch(HibernateException e){
			throw new DAOException(e);
		}finally{
			session.close();
		}
		return (int)page;
	}
	
}
