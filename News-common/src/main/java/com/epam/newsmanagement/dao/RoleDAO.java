package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Role;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Interface for accessing Roles table
 * @author Bahdan_Shpakouski
 * 
 */
public interface RoleDAO extends GenericDAO<Role> {
	
	/**
	 * Deletes roles of the user with id passed
	 * @param userId id of the user
	 * @throws DAOException
	 */
	void deleteUserRoles(long userId) throws DAOException;
	
	/**
	 * Reads roles of the user with id passed
	 * @param userId
	 * @return list of roles found
	 * @throws DAOException
	 */
	List<Role> readUserRoles(long userId) throws DAOException;
}
