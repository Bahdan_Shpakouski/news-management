package com.epam.newsmanagement.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;

public class AuthorDAOImpl implements AuthorDAO {

	static Logger logger = Logger.getLogger(AuthorDAOImpl.class);
	private static final String SQL_SELECT_ALL = "select atr_author_id,"
			+ " atr_author_name, atr_expired from authors ";
	private static final String SQL_SELECT_NOT_EXPIRED = "select atr_author_id,"
			+ " atr_author_name, atr_expired from authors "
			+ "where atr_expired is null";
	private static final String SQL_SELECT = "select atr_author_id,"
			+ " atr_author_name, atr_expired from authors "
			+ "where atr_author_id=?";
	private static final String SQL_SELECT_NEWS_AUTHOR = "select atr_author_id,"
			+ " atr_author_name, atr_expired from authors "
			+ "inner join news_authors on atr_author_id=na_author_id where"
			+ " na_news_id=?";
	private static final String SQL_UPDATE = "update authors set atr_author_name = ?,"
			+ " atr_expired = ? where atr_author_id = ?";
	private static final String SQL_INSERT = "insert into authors "
			+ "(atr_author_name, atr_expired) values (?, ?)";
	private static final String SQL_DELETE = "delete from authors "
			+ "where atr_author_id= ?";
	private static final String STATEMENT_ERROR_MESSAGE = "Error occurred "
			+ "while closing statement";
	
	@Autowired
	private DataSource dataSource;
	
	public AuthorDAOImpl() {
		
	}

	@Override
	public Long create(Author author) throws DAOException {
		PreparedStatement ps = null;
		Connection conn = null;
		Long id = null;
		try{ 
			String generatedColumns[] = { "atr_author_id" };
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_INSERT, generatedColumns);
			ps.setString(1, author.getAuthorName());
			ps.setTimestamp(2, author.getExpired());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if(rs.next()){
				id = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return id;
	}

	@Override
	public List<Author> readAll() throws DAOException {
		List<Author> list = new ArrayList<Author>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_SELECT_ALL);
			rs = ps.executeQuery();
			while(rs.next()){
				Author author = new Author();
				author.setAuthorId(rs.getLong(1));
				author.setAuthorName(rs.getString(2));
				author.setExpired(rs.getTimestamp(3));
				list.add(author);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return list;
	}

	@Override
	public Author read(long id) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Author author = null;
		try { 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_SELECT);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while(rs.next()){
				author= new Author();
				author.setAuthorId(rs.getLong(1));
				author.setAuthorName(rs.getString(2));
				author.setExpired(rs.getTimestamp(3));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return author;
	}

	@Override
	public void update(Author author, long id) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_UPDATE);
			ps.setString(1, author.getAuthorName());
			ps.setTimestamp(2, author.getExpired());
			ps.setLong(3, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
	}

	@Override
	public void delete(long id) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_DELETE);
			ps.setLong(1, id);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
	}

	@Override
	public Author readNewsAuthor(long newsId) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Author author = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_SELECT_NEWS_AUTHOR);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			while(rs.next()){
				author = new Author();
				author.setAuthorId(rs.getLong(1));
				author.setAuthorName(rs.getString(2));
				author.setExpired(rs.getTimestamp(3));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return author;
	}

	@Override
	public List<Author> readNotExpired() throws DAOException {
		List<Author> list = new ArrayList<Author>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_SELECT_NOT_EXPIRED);
			rs = ps.executeQuery();
			while(rs.next()){
				Author author = new Author();
				author.setAuthorId(rs.getLong(1));
				author.setAuthorName(rs.getString(2));
				author.setExpired(rs.getTimestamp(3));
				list.add(author);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return list;
	}

}
