package com.epam.newsmanagement.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;

public class CommentDAOImpl implements CommentDAO{

	static Logger logger = Logger.getLogger(CommentDAOImpl.class);
	private static final String SQL_SELECT_ALL = "select CMT_COMMENT_ID,"
			+ " CMT_NEWS_ID, CMT_COMMENT_TEXT, CMT_CREATION_DATE from comments";
	private static final String SQL_SELECT_NEWS_COMMENTS = "select CMT_COMMENT_ID,"
			+ " CMT_NEWS_ID, CMT_COMMENT_TEXT, CMT_CREATION_DATE from comments"
			+ " where cmt_news_id=? order by CMT_CREATION_DATE";
	private static final String SQL_SELECT = "select CMT_COMMENT_ID, CMT_NEWS_ID,"
			+ " CMT_COMMENT_TEXT, CMT_CREATION_DATE from comments "
			+ "where cmt_comment_id=?";
	private static final String SQL_UPDATE = "update comments set "
			+ "cmt_comment_text = ?, cmt_creation_date=? "
			+ "where cmt_comment_id = ?";
	private static final String SQL_INSERT = "insert into comments (cmt_news_id,"
			+ " cmt_comment_text, cmt_creation_date) values (?, ?, ?)";
	private static final String SQL_DELETE = "delete from comments "
			+ "where cmt_comment_id= ?";
	private static final String SQL_DELETE_NEWS_COMMENTS = "delete from comments "
			+ "where cmt_news_id= ?";
	private static final String STATEMENT_ERROR_MESSAGE = "Error occurred "
			+ "while closing statement";
	
	@Autowired
	private DataSource dataSource;
	
	public CommentDAOImpl() {
		
	}

	@Override
	public Long create(Comment comment) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		Long id = null;
		try{ 
			String generatedColumns[] = { "cmt_comment_id" };
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_INSERT, generatedColumns);
			ps.setLong(1, comment.getNewsId());
			ps.setString(2, comment.getCommentText());
			ps.setTimestamp(3, new Timestamp(new Date().getTime()));
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if(rs.next()){
				id = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}	
		return id;
	}

	@Override
	public List<Comment> readAll() throws DAOException {
		List<Comment> list = new ArrayList<Comment>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_SELECT_ALL);
			rs = ps.executeQuery();
			while(rs.next()){
				Comment comment = new Comment();
				comment.setCommentId(rs.getLong(1));
				comment.setNewsId(rs.getLong(2));
				comment.setCommentText(rs.getString(3));
				comment.setCreationDate(rs.getTimestamp(4));
				list.add(comment);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return list;
	}

	@Override
	public Comment read(long id) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Comment comment = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_SELECT);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while(rs.next()){
				comment =  new Comment();
				comment.setCommentId(rs.getLong(1));
				comment.setNewsId(rs.getLong(2));
				comment.setCommentText(rs.getString(3));
				comment.setCreationDate(rs.getTimestamp(4));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return comment;
	}

	@Override
	public void update(Comment comment, long id) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_UPDATE);
			ps.setString(1, comment.getCommentText());
			ps.setTimestamp(2, new Timestamp(new Date().getTime()));
			ps.setLong(3, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
	}

	@Override
	public void delete(long id) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_DELETE);
			ps.setLong(1, id);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
	}

	@Override
	public List<Comment> readNewsComments(long newsId) throws DAOException {
		List<Comment> list = new ArrayList<Comment>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_SELECT_NEWS_COMMENTS);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			while(rs.next()){
				Comment comment =  new Comment();
				comment.setCommentId(rs.getLong(1));
				comment.setNewsId(rs.getLong(2));
				comment.setCommentText(rs.getString(3));
				comment.setCreationDate(rs.getTimestamp(4));
				list.add(comment);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return list;
	}

	@Override
	public void deleteNewsComments(long newsId) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_DELETE_NEWS_COMMENTS);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
	}

}
