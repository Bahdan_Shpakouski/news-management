package com.epam.newsmanagement.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;

public class TagDAOImpl implements TagDAO {

	static Logger logger = Logger.getLogger(TagDAOImpl.class);
	private static final String SQL_SELECT_ALL = "select TG_TAG_ID,"
			+ " TG_TAG_NAME from tags";
	private static final String SQL_SELECT = "select TG_TAG_ID, TG_TAG_NAME"
			+ " from tags where tg_tag_id=?";
	private static final String SQL_SELECT_NEWS_TAGS = "select TG_TAG_ID,"
			+ " TG_TAG_NAME from tags inner join news_tags"
			+ " on tg_tag_id=nt_tag_id where nt_news_id=?";
	private static final String SQL_UPDATE = "update tags set tg_tag_name = ?"
			+ " where tg_tag_id = ?";
	private static final String SQL_INSERT = "insert into tags (tg_tag_name) "
			+ "values (?)";
	private static final String SQL_DELETE = "delete from tags "
			+ "where tg_tag_id= ?";
	private static final String SQL_REMOVE_TAG_FROM_NEWS = "delete from news_tags "
			+ "where nt_tag_id= ?";
	private static final String STATEMENT_ERROR_MESSAGE = "Error occurred "
			+ "while closing statement";
	
	@Autowired
	private DataSource dataSource;
	
	public TagDAOImpl() {
		
	}

	@Override
	public Long create(Tag tag) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		Long id = null;
		try{
			String generatedColumns[] = { "tg_tag_id" };
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_INSERT, generatedColumns);
			ps.setString(1, tag.getTagName());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if(rs.next()){
				id = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return id;
	}

	@Override
	public List<Tag> readAll() throws DAOException {
		List<Tag> list = new ArrayList<Tag>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_SELECT_ALL);
			rs = ps.executeQuery();
			while(rs.next()){
				Tag tag = new Tag();
				tag.setTagId(rs.getLong(1));
				tag.setTagName(rs.getString(2));
				list.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return list;
	}

	@Override
	public Tag read(long id) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Tag tag = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_SELECT);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while(rs.next()){
				tag = new Tag();
				tag.setTagId(rs.getLong(1));
				tag.setTagName(rs.getString(2));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return tag;
	}

	@Override
	public void update(Tag tag, long id) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_UPDATE);
			ps.setString(1, tag.getTagName());
			ps.setLong(2, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
	}

	@Override
	public void delete(long id) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_DELETE);
			ps.setLong(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
	}

	@Override
	public List<Tag> readNewsTags(long newsId) throws DAOException {
		List<Tag> list = new ArrayList<Tag>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_SELECT_NEWS_TAGS);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			while(rs.next()){
				Tag tag = new Tag();
				tag.setTagId(rs.getLong(1));
				tag.setTagName(rs.getString(2));
				list.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
		return list;
	}

	@Override
	public void removeTagFromAllNews(long tagId) throws DAOException {
		Connection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = DataSourceUtils.getConnection(dataSource);
			ps = conn.prepareStatement(SQL_REMOVE_TAG_FROM_NEWS);
			ps.setLong(1, tagId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				if(ps!=null){
					ps.close();
				}
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			DataSourceUtils.releaseConnection(conn, dataSource);
		}
	}

}
