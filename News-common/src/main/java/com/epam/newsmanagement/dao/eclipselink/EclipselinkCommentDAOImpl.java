package com.epam.newsmanagement.dao.eclipselink;

import java.sql.Timestamp;
import java.util.List;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.DAOException;

public class EclipselinkCommentDAOImpl implements CommentDAO {

	private static final String JPQL_SELECT_ALL = "SELECT c FROM Comment c";
	private static final String JPQL_SELECT_NEWS_COMMENTS = "SELECT C FROM Comment C "
			+ "WHERE C.news.newsId = :news_id ORDER BY C.creationDate";
	private static final String JPQL_DELETE_NEWS_COMMENTS = "DELETE FROM Comment C"
			+ " WHERE C.news.newsId = :news_id";
	
	@Autowired
	private EntityManagerFactory emf;
	@PersistenceContext
	private EntityManager entityManager;
	
	public EclipselinkCommentDAOImpl() {
		
	}

	@Override
	public Long create(Comment value) throws DAOException {
		value.setCreationDate(new Timestamp(new Date().getTime()));
		value.setNews(new News());
		value.getNews().setNewsId(value.getNewsId());
		entityManager.persist(value);
		Long commentId = (Long) emf.getPersistenceUnitUtil().getIdentifier(value);
		return commentId;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> readAll() throws DAOException {
		EntityManager em = emf.createEntityManager();
		Query query =  em.createQuery(JPQL_SELECT_ALL);
		query.setHint("eclipselink.result-collection-type", java.util.ArrayList.class);
		List<Comment> list = query.getResultList();
		em.close();
		for(Comment comment : list){
			comment.setNewsId(comment.getNews().getNewsId());
		}
		return list;
	}

	@Override
	public Comment read(long id) throws DAOException {
		EntityManager em = emf.createEntityManager();
		Comment comment =em.find(Comment.class, id);
		em.close();
		if(comment != null) {
			comment.setNewsId(comment.getNews().getNewsId());
		}
		return comment;
	}

	@Override
	public void update(Comment value, long id) throws DAOException {
		Comment comment =entityManager.find(Comment.class, id);
		comment.setCommentText(value.getCommentText());
		comment.setCreationDate(new Timestamp(new Date().getTime()));
	}

	@Override
	public void delete(long id) throws DAOException {
		Comment comment = entityManager.find(Comment.class, id);
		entityManager.remove(comment);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> readNewsComments(long newsId) throws DAOException {
		EntityManager em = emf.createEntityManager();
		Query query =  em.createQuery(JPQL_SELECT_NEWS_COMMENTS);
		query.setHint("eclipselink.result-collection-type", java.util.ArrayList.class);
		query.setParameter("news_id", newsId);
		List<Comment> list = query.getResultList();
		em.close();
		for(Comment comment : list){
			comment.setNewsId(comment.getNews().getNewsId());
		}
		return list;
	}

	@Override
	public void deleteNewsComments(long newsId) throws DAOException {
		Query query =  entityManager.createQuery(JPQL_DELETE_NEWS_COMMENTS);
		query.setParameter("news_id", newsId);
		query.executeUpdate();
		
	}

}
